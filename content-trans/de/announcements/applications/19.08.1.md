---
aliases:
- ../announce-applications-19.08.1
changelog: true
date: 2019-09-05
description: KDE veröffentlicht die Anwendungen 19.08.1
layout: application
major_version: '19.08'
release: applications-19.08.1
title: KDE veröffentlicht die Anwendungen 19.08.1
version: 19.08.1
---
{{% i18n_date %}}

Heute veröffentlicht KDE die erste Aktualisierung der <a href='../19.08.0'>KDE-Anwendungen 19.08</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

More than twenty recorded bugfixes include improvements to Kontact, Dolphin, Kdenlive, Konsole, Step, among others.

Verbesserungen umfassen:

- Several regressions in Konsole's tab handling have been fixed
- Dolphin again starts correctly when in split-view mode
- Deleting a soft body in the Step physics simulator no longer causes a crash
