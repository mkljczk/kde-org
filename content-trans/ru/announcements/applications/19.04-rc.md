---
aliases:
- ../announce-applications-19.04-rc
date: 2019-04-05
description: KDE Ships Applications 19.04 Release Candidate.
layout: application
release: applications-19.03.90
title: KDE выпускает первую версию-кандидат к выпуску KDE Applications 19.04
version_number: 19.03.90
version_text: 19.04 Release Candidate
---
April 5, 2019. Today KDE released the Release Candidate of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

Check the <a href='https://community.kde.org/Applications/19.04_Release_Notes'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release.

Выпуски KDE Applications 19.04 требуют тщательного тестирования для обеспечения должного уровня качества и удовлетворения пользователей. Очень важную роль в этом играют те, кто используют нашу систему повседневно, ведь сами разработчики просто не могут протестировать все возможные конфигурации. Мы рассчитываем на вашу помощь в поиске ошибок на этом этапе, чтобы мы могли исправить их до выхода финальной версии. По возможности включайтесь в команду — установите эту версию-кандидат в выпуски и <a href='https://bugs.kde.org/'>сообщайте нам</a> о найденных ошибках.
