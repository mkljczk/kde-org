---
aliases:
- ../announce-applications-15.08.0
changelog: true
date: 2015-08-19
description: KDE publie la version 15.08.0 des applications de KDE.
layout: application
release: applications-15.08.0
title: KDE publie les applications de KDE 15.08.0
version: 15.08.0
---
{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/dolphin15.08_0.png" alt=`Dolphin dans sa nouvelle apparence - maintenant développé avec l'environnement de développement version 5 de KDE` class="text-center" width="600px" caption=`Dolphin dans sa nouvelle apparence - maintenant développé avec l'environnement de développement version 5 de KDE`>}}

19 Août 2015. Aujourd'hui, KDE publie les applications KDE 15.08.

Avec cette mise à jour, un ensemble de 107 applications ont été portées sous <a href='https://dot.kde.org/2013/09/25/frameworks-5'>les environnements de développement de KDE en version 5</a>. L'équipe s'efforce d'apporter la meilleure qualité à votre bureau et à ses applications. Aussi, votre retour d'expérience est important pour l'équipe de KDE.

Avec cette version, il y a plusieurs nouveaux ajouts à la liste des applications reposant sur l'environnement de développement KDE version 5, incluant <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, la <a href='https://www.kde.org/applications/office/kontact/'>suite Kontact</a>, <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, <a href='https://games.kde.org/game.php?game=picmi'>Picmi</a>, etc.

### Aperçu technique de la suite Kontact

Durant les quelques derniers mois, l'équipe de KDE PIM a fait des efforts importants pour le portage de Kontact vers « Qt 5 »et l'environnement de développement de KDE 5. De plus, les performances d'accès aux données ont été très largement améliorées grâce à une couche de communication optimisée.
L'équipe de KDE PIM travaille activement pour un polissage supplémentaire de la suite Kontact et est en attente de retours des utilisateurs.
Pour des informations complémentaires et détaillées concernant les changements dans KDE PIM, veuillez consulter le blog de <a href='http://www.aegiap.eu/kdeblog/'>Laurent Montels</a>.

### Kdenlive et Okular

Cette mise à jour de Kdenlive intègre un grand nombre de corrections dans l'assistant de DVD, ainsi qu'un grand nombre de corrections et d'autres fonctionnalités dont l'intégration de quelques importants remaniements. Pour plus d'informations concernant les modifications dans Kdenlive, veuillez consulter sa <a href='https://kdenlive.org/discover/15.08.0'>note complète de modifications</a>. Enfin, Okular prend maintenant en charge les transitions en atténuation dans le mode de présentation.

{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/ksudoku_small_0.png" alt=`KSudoku avec un puzzle avec des caractères` class="text-center" width="600px" caption=`KSudoku avec un puzzle avec des caractères`>}}

### Dolphin, Éducation et Jeux

Dolphin a été aussi porté dans l'environnement de développement de KDE 5. Marble a reçu un support amélioré de <a href='https://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system'>UTM</a>, ainsi qu'une meilleure prise en charge des annotations, de la modification et des recouvrements « KML ».

Ark a reçu un nombre incroyable de contributions, y compris de nombreuses petites corrections de bogues. KStars a reçu un large nombre de contributions, y compris l'algorithme « Flat ADU » et la vérifications des limites, l'enregistrement des changements de fuseaux horaires, la déviation de guidage, la limite d'auto-focus « HFR » dans le fichier de séquence ainsi que l'ajout de la prise en charge d'un taux de telescope et la mise en sécurité. KSudoku fonctionne bien mieux. Les contributions intègrent : l'ajout d'une interface graphique et un un moteur pour utiliser des puzzles dans « Mathdoku » et « Killer Sudoku », l'ajout d'un nouveau résolveur reposant sur l'algorithme « DLX » (Donald Knuth's Dancing Links)

### Autres mises à jour

A coté de cette mise à jour, Plasma 4 sera publié dans sa version « LTS », en version 4.11.22 à ce jour.
