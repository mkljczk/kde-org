---
aliases:
- ../4.11
custom_about: true
custom_contact: true
date: 2013-08-14
description: KDE publie la version 4.11.1 des environnements de développement, des
  applications et de la plate-forme de développement de Plasma.
title: Compilation des logiciels de KDE 4.11
---
{{<figure src="/announcements/4/4.11.0/screenshots/plasma-4.11.png" class="text-center" caption=`Les environnements de bureaux Plasma de KDE 4.11` >}} <br />

14 Août 2013. La communauté KDE est fière d'annoncer les mises à jour majeures de la dernière version des environnements de bureaux, des applications et de la plate-forme de développement Plasma. Celles-ci apportent de nouvelles fonctionnalités et de nouvelles corrections de bogues, rendant la plate-forme prête pour des évolutions ultérieures. Les espaces de travail de Plasma 4.11 bénéficieront d'une période de maintenance étendue puisque l'équipe de développement aura pour objectif la transition technique vers l'environnement de développement en version 5. Cette étape présente la dernière publication conjointe des environnements de bureaux, des applications et de la plate-forme de développement Plasma sous le même numéro de version.<br/>

Cette publication rend hommage à <a href='http://en.wikipedia.org/wiki/Atul_Chitnis'>Atul « toolz » Chitnis</a>, un grand champion du Logiciel Libre en Inde. Atul a mis en place depuis 2001 les conférence LINUX de Bangalore et FOSS.IN, qui ont été toutes les deux des évènements marquants dans la communauté du Logiciel Libre en Inde. KDE Inde a été créé lors de la première conférence FOSS.IN en Décembre 2005. De nombreux contributeurs indiens de KDE ont démarré à ces évènements. Si la journée du projet KDE a toujours été un immense succès, cela n'a été possible que grâce à l'investissement de Atul. Atul nous a quitté le 03 Juin, après avoir mené bataille contre le cancer. Puisse son âme être en paix. Nous sommes tous reconnaissants pour ses contributions à un monde meilleur.

Ces version sont toutes traduites en 54 langues. Plus de langues sont attendues avec les versions correctives mineures à publier mensuellement par KDE. L'équipe de documentation a mis à jour les manuels de 91 applications pour cette version.

## <a href="../plasma"><img src="/announcements/announce-4.11/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Les espaces de travail de Plasma 4.11 continuent à améliorer l'expérience utilisateur</a>.

Engagé dans une maintenance à long terme, les environnements de bureaux Plasma apportent de nombreuses améliorations aux fonctionnalités de base, avec une barre de tâches plus facile, un composant graphique de batterie plus intelligent et un mélangeur de sons amélioré. L'introduction de KScreen apporte une gestion intelligente du mode multi-écran à l'environnement de bureaux et des améliorations très importantes de performances, conjugué avec de petits réglages d'ergonomie nécessaires à un confort général agréable.

## <a href="./applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> Les applications de KDE 4.11 apportent un important pas en avant concernant la gestion des informations personnelles et des améliorations sur toutes les fonctionnalités.</a>

Cette version intègre de très nombreuses améliorations dans la pile de KDE PIM, apportant de bien meilleures performances et plusieurs nouvelles fonctionnalités. Kate améliore la productivité pour les développeurs Python et Javascript avec de nouveaux modules externes. Dolphin est devenu plus rapide et les applications pour l'éducation bénéficient de nombreuses fonctionnalités variées.

## <a href="./platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> La plate-forme 4.11 de KDE fournit de meilleures performances</a>.

Cette version de la plate-forme KDE 4.11 continue à se focaliser sur la stabilité. De nouvelles fonctionnalités ont été implémentées pour la version 5.0 du futur environnement de développement de KDE. Mais, il a été prévu de limiter les optimisations pour l'environnement Nepomuk dans les versions stables.

<br /> 
Lors de la mise à jour, veuillez consulter les <a href='http://community.kde.org/KDE_SC/4.11_Release_Notes'>notes de publication</a>.<br /> 

## Diffusez l'information et observez ce qui se passe : mettez une balise &quot;KDE&quot;

KDE encourage les personnes à diffuser l'information sur les réseaux sociaux. Proposez des messages sur les sites de « news », utilisez les canaux comme « Delicious », « Digg », « Reddit », « Twitter », « Identi.ca ». Téléchargez des copies d'écran vers des services comme « Facebook », « Flickr », « Ipernity » et « Picasa ». Postez les sur les groupes appropriés. Créez des vidéos d'écran. Chargez les sur « YouTube », « Blip.tv » et « Vimeo ». Veuillez marquer vos messages et les éléments téléchargés avec la balise &quot;KDE&quot;. Cela les rendra plus facile à trouver et donnera à l'équipe de promotion de KDE, une façon pour analyser la couverture associée aux publications de versions 4.11 pour les logiciels de KDE.

## Évènements de publication

Comme d'habitude, les membres de la communauté KDE organisent tout autour du monde, des évènements pour les nouvelles versions. Plusieurs ont déjà été programmés et d'autres viendront plus tard. Vous trouverez <a href='http://community.kde.org/Promo/Events/Release_Parties/4.11'>une liste de ces évènements ici</a>. Tout le monde est le bienvenu pour y participer ! Il y aura un mélange de personnes concernées et de conversations motivées, tout autant que des boissons et de la nourriture. C'est une opportunité pour apprendre plus de choses sur la suite du développement de KDE, de se sentir impliqué ou juste de rencontrer d'autres utilisateurs ou contributeurs.

Toute personne est invitée à organiser son propre évènement. Il est amusant d'en organiser un et il est ouvert à tout le monde ! Regardez les <a href='http://community.kde.org/Promo/Events/Release_Parties'>conseils sur « Comment organiser un évènement »</a>.

## A propos de ces annonces de publication

Ces annonces de publication ont été rédigées par Jos Poortvliet, Sebastian Kügler, Markus Slopianka, Burkhard Lück, Valorie Zimmerman, Maarten De Meyer, Frank Reininghaus, Michael Pyne, Martin Gräßlin, d'autres membres de l'équipe de promotion de KDE et plus largement de toute la communauté de KDE. Elles mettent en évidence les nombreuses modifications faites dans les logiciels KDE durant les six derniers mois écoulés.

#### Soutenir KDE

<a href="http://jointhegame.kde.org/"> <img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3" alt="Join the Game"/> </a>

Le nouveau <a href='http://jointhegame.kde.org/'>Programme de membres actifs</a> de KDE e.V est maintenant ouvert. Pour 25 &euro; par trimestre, vous pouvez contribuer à l'accroissement de la communauté internationale de KDE pour le développement de logiciels libres de portée mondiale.
