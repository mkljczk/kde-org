---
aliases:
- ../../kde-frameworks-5.52.0
date: 2018-11-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Correcção da compilação com o BUILD_QCH=TRUE
- Uso de facto dos 'fileNameTerms' e 'xAttrTerms'
- [Balooshow] Evitar acessos fora dos limites ao aceder a dados corrompidos da BD
- [Extracção] Não verificar o QFile::exists para um URL vazio
- [Agendamento] Uso de opção para registar quando um módulo de execução fica inactivo
- [Extracção] Tratamento adequado dos dados onde o tipo MIME não deverá ser indexado
- [Agendamento] Correcção de utilização inválida da data/hora do QFileInfo::created() (erro 397549)
- [Extracção] Tornar a extracção mais resistente a estoiros (erro 375131)
- Passagem do FileIndexerConfig como constante para as indexações individuais
- [Configuração] Remoção do suporte da configuração do KDE4, parar de escrever em ficheiros de configuração arbitrários
- [Extracção] Melhoria na depuração da linha de comandos, direccionamento do 'stderr'
- [Agendamento] Reutilização do 'fileinfo' do FilteredDirIterator
- [Agendamento] Reutilização do tipo MIME no UnindexedFileIterator na indexação
- [Agendamento] Remoção da variável supérflua 'm_extractorIdle'
- Aplicação de verificações de ficheiros não indexados e de elementos órfãos do índice no arranque
- [balooctl] Impressão do estado actual &amp; do ficheiro em indexação no início do monitor (erro 364858)
- [balooctl] Vigiar também as mudanças de estado
- [balooctl] Correcção do comando "index" com um ficheiro já indexado mas movido (erro 397242)

### BluezQt

- Adição da geração de ficheiros de inclusão da API do Media e MediaEndpoint

### Ícones Breeze

- Mudança dos ícones do gestor de pacotes para emblemas
- Reintrodução de ícones de ligações monocromáticos como acções
- Melhoria no contraste, legibilidade e consistência dos emblemas (erro 399968)
- Suporte de um "novo" tipo MIME para os ficheiros .deb (erro 399421)

### Módulos extra do CMake

- ECMAddQch: ajuda ao 'doxygen', predefinindo mais macros Q*DECL**
- Interfaces: Suporte ao uso das pastas do 'sys' para a pasta de instalação do Python
- Interfaces: Remoção do INSTALL_DIR_SUFFIX do 'ecm_generate_python_binding'
- Adição do suporte para a sanitização difusa

### KCMUtils

- suporte para KCM's com várias páginas

### KConfig

- Adição de mecanismo para notificar outros clientes da mudança de configurações por DBus
- Exposição de método de leitura para o KConfig::addConfigSources

### KConfigWidgets

- Possibilidade de o KHelpCenter abrir as páginas correctas da ajuda do KDE quando o KHelpClient é invocado com uma âncora

### KCrash

- KCrash: correcção de estoiro (irónico :-)) quando usado numa aplicação sem uma QCoreApplication

### KDeclarative

- tornar o 'push/pop' parte da API do ConfigModule

### KDED

- Remoção da mensagem inútil "No X-KDE-DBus-ServiceName found" (Nenhum X-KDE-DBus-ServiceName encontrado)

### KDesignerPlugin

- Referência ao produto "KF5" nos meta-dados do elemento, em vez de "KDE"

### KDocTools

- Documentação da API: adição de documentação mínima para o espaço de nomes KDocTools, para o 'doxygen' o cubra
- Criação opcional de um ficheiro QCH com a documentação da API com o ECMAddQCH
- Espera pela compilação do 'docbookl10nhelper' antes de compilar as nossas próprias páginas do 'man'
- Uso do interpretador de Perl indicado em vez de se basear no PATH

### KFileMetaData

- [ExtractorCollection] Uso apenas do melhor 'plugin' de extracção correspondente
- [KFileMetaData] Adição de módulo de extracção para o XML e SVG genéricos
- [KFileMetaData] Adição de utilitário para os meta-dados Dublin Core codificados em XML
- implementação do suporte para a leitura de marcas ID3 dos ficheiros .aiff e .wav
- implementação de mais marcas para os meta-dados ASF
- extracção das marcas do APE nos ficheiros APE e Wavpack
- fornecimento de uma lista de tipos MIME suportados para o 'embeddedimagedata'
- comparação com o QLatin1String e harmonização do tratamento de todos os tipos
- Não estoirar no caso de dados de EXIV2 inválidos (erro 375131)
- epubextractor: Adição da propriedade ReleaseYear
- remodelação do 'taglibextractor' para funções específicas pelo tipo dos meta-dados
- adição das marcas ASF/ficheiros WMA como tipo MIME suportado
- uso de uma extracção própria para testar o 'taglibwriter'
- adição de um sufixo de texto para testar os dados e para testes com Unicode do 'taglibwriter'
- remoção de verificação da versão da Taglib durante a compilação
- extensão da cobertura dos testes para todos os tipos MIME do 'taglibextractor'
- Uso de variável com o texto já obtido em vez de obter de novo

### KGlobalAccel

- Correcção das notificações de mudança da disposição do teclado (erro 269403)

### KHolidays

- Adição do Ficheiro de Feriados do Bahrain
- Possibilidade de o KHolidays funcionar também como biblioteca estática

### KIconThemes

- Adição de um QIconEnginePlugin para permitir a descodificação do QIcon (erro 399989)
- [KIconLoader] Substituição de um QImageReader com alocação de dados por um com alocação na pilha
- [KIconLoader] Ajuste do contorno do emblema, dependendo do tamanho dos ícones
- Centrar os ícones de forma adequada, caso o tamanho não seja adequado (erro 396990)

### KIO

- Não tentar usar por contingências protocolos SSL "menos seguros"
- [KSambaShare] Eliminação de '/' final na localização da partilha
- [kdirlistertest] Esperar um pouco mais pela finalização da listagem
- Mostrar uma mensagem de desculpas caso o ficheiro não seja local
- kio_help: Correcção de estoiro no QCoreApplication ao aceder ao 'help://' (erro 399709)
- Evitar a espera pelas acções do utilizador quando a prevenção de captura do foco do KWin for alta ou extremamente alta
- [KNewFileMenu] Não abrir um QFile vazio
- Adição de ícones em falta no código do Painel de Locais do KIO
- Eliminação dos ponteiros para KFileItem em bruto no KCoreDirListerCache
- Adição da opção 'Montar' ao menu de contexto de um dispositivo não montado nos Locais
- Adição de um elemento 'Propriedades' no menu de contexto do painel de Locais
- Correcção do aviso "macro expansion producing 'defined' has undefined behavior" (a expansão da macro que produz 'defined' tem um comportamento indefinido)

### Kirigami

- Correcção dos itens em falta nas compilações estáticas
- suporte básico para as páginas escondidas
- carregamento de ícones dos temas de ícones adequados
- (muitas outras correcções)

### KNewStuff

- Mensagens de erro mais úteis

### KNotification

- Correcção de um estoiro provocado por uma má gestão do tempo de vida da notificação de áudio baseada no Canberra (erro 398695)

### KParts

- Correcção do 'Cancelar' não tratado no método desactualizado BrowserRun::askEmbedOrSave
- Migração para uma variante não-obsoleta do KRun::runUrl
- Migração do KIO::Job::ui() -&gt; KJob::uiDelegate()

### KWayland

- Adição do protocolo de ecrãs virtuais do KWayland
- Protecção contra a remoção da fonte de dados antes de processar o evento de recepção do 'dataoffer' (erro 400311)
- [servidor] Respeito da região de entrada das sub-superfícies no foco da superfície do cursor
- [xdgshell] Adição dos operadores das opções de ajuste de restrições da posição

### KWidgetsAddons

- Documentação da API: correcção da nota "Desde" do KPageWidgetItem::isHeaderVisible
- Adição de uma nova propriedade 'headerVisible'

### KWindowSystem

- Não comparar os iteradores devolvidos por duas cópias devolvidas em separado

### KXMLGUI

- Suporte para KMainWindows 1..n no kRestoreMainWindows

### NetworkManagerQt

- Adição das opções de configuração do IPV4 em falta
- Adição da configuração 'vxlan'

### Plasma Framework

- reversão da mudança de escala dos ícones em dispositivos móveis
- Suporte de legendas mnemónicas
- Remoção da opção PLASMA_NO_KIO
- Pesquisa adequada por temas de contingência

### Purpose

- Activação da opção Dialog no JobDialog

### Solid

- [solid-hardware5] Ícone da lista nos detalhes do dispositivo
- [UDisks2] Desactivação da energia da unidade ao remover, caso seja suportado (erro 270808)

### Sonnet

- Correcção de problemas na adivinha da linguagem

### Sindicância

- Adição do ficheiro README.md em falta (necessário em vários programas)

### Realce de sintaxe

- Realce de sintaxe dos ficheiros CLIST do z/OS
- Criação de novos ficheiros de realce de sintaxe para a JCL (Job Control Language)
- Remoção do modo de abertura das versões do Qt demasiado recentes
- incremento da versão + correcção da versão do Kate necessária para a versão actual da plataforma
- regra de palavra-chave: Suporte para a inclusão de palavras-chave de outra linguagem/ficheiro
- Sem verificação ortográfica para o Metamath, excepto nos comentários
- CMake: Adição de variáveis e propriedades relacionadas com o XCode introduzidas no 3.13
- CMake: Introdução de novas funcionalidades na próxima versão 3.13

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
