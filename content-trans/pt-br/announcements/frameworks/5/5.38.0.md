---
aliases:
- ../../kde-frameworks-5.38.0
date: 2017-09-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Faltou no anúncio do último mês: o KF5 inclui uma Nova plataforma: Kirigami, um conjunto de 'plugins' em QtQuick para construir interfaces de utilizador baseadas nas linhas-mestras de usabilidade do KDE.

### Baloo

- Correcção da pesquisa baseada em pastas

### Módulos extra do CMake

- Configuração do CMAKE_*_OUTPUT_5.38 para executar os testes sem instalar
- Inclusão de um módulo para descobrir as importações de QML como dependências de execução

### Integração do Framework

- Reposição de um ícone de limpeza dos campos em alta resolução
- Correcção da aceitação de janelas com Ctrl+Return quando os botões mudaram de nome

### KActivitiesStats

- Remodelação da pesquisa que combina os recursos ligados e usados
- Recarregamento do modelo quando o recurso fica dissociado
- Correcção da pesquisa que reúne os recursos ligados e usados

### KConfig

- Correcção das legendas das acções DeleteFile/RenameFile (erro 382450)
- kconfigini: Eliminação dos espaços iniciais ao ler os valores dos elementos (erro 310674)

### KConfigWidgets

- Descontinuação do KStandardAction::Help e do KStandardAction::SaveOptions
- Correcção das legendas das acções DeleteFile/RenameFile (erro 382450)
- Uso do "document-close" como ícone para o KStandardAction::close

### KCoreAddons

- DesktopFileParser: adição de pesquisa de contingência no ":/kservicetypes5/*"
- Adição do suporte para 'plugins' desinstalados no kcoreaddons_add_plugin
- desktopfileparser: Correcção do processamento de chaves/valores fora do padrão (erro 310674)

### KDED

- suporte para o X-KDE-OnlyShowOnQtPlatforms

### KDocTools

- CMake: Correcção da redução do nome do destino quando a pasta de compilação tem caracteres especiais (erro 377573)
- Adição do CC BY-SA 4.0 Internacional e uso por omissão

### KGlobalAccel

- KGlobalAccel: migração para o novo método port 'symXModXToKeyQt' do KKeyServer, para corrigir as teclas do teclado numérico (erro 183458)

### KInit

- klauncher: correcção da correspondência do 'appId' para as aplicações Flatpak

### KIO

- Migração do KCM de atalhos Web do KServiceTypeTrader para o KPluginLoader::findPlugins
- [KFilePropsPlugin] Formatação regional do 'totalSize' durante o cálculo
- KIO: correcção de uma fuga de memória duradoura ao sair
- Adição da capacidade de filtragem de tipos MIME no KUrlCompletion
- KIO: migração dos 'plugins' de filtragem de URI's do KServiceTypeTrader para o json+KPluginMetaData
- [KUrlNavigator] Emissão de 'tabRequested' quando o local no menu é carregado com o botão do meio (erro 304589)
- [KUrlNavigator] Emissão de 'tabRequested' quando o selector de locais é carregado com o botão do meio (erro 304589)
- [KACLEditWidget] Permitir o duplo-click para editar um elemento
- [kiocore] Correcção do erro de lógica na versão anterior
- [kiocore] Verificação se o 'klauncher' está em execução ou não
- Limitar a sério a taxa de mensagens INF_PROCESSED_SIZE (erro 383843)
- Não limpar o armazém de certificados AC de SSL do Qt
- [KDesktopPropsPlugin] Criar a pasta de destino se não existir
- ['KIO slave' de ficheiros] Correcção da aplicação de atributos especiais dos ficheiros (erro 365795)
- Remoção da verificação de ocupação em ciclo no TransferJobPrivate::slotDataReqFromDevice
- mudança do 'kiod5' para um "agente" no Mac
- Correcção do KCM do 'proxy' que não carrega correctamente os 'proxies' manuais

### Kirigami

- esconder as barras de deslocamento se forem inúteis
- Adição de um exemplo básico para ajustar a largura das colunas com uma pega arrastável
- Camadas mais largas no posicionamento das pegas
- correcção na colocação das pegas quando se sobrepõe à última página
- não mostrar uma pega falsa na última coluna
- não guardar coisas nos métodos delegados (erro 383741)
- como já foi definido o keyNavigationEnabled, definir também as envolvências
- melhor alinhamento à esquerda para o botão 'recuar' (erro 383751)
- não ter em conta 2 vezes o cabeçalho no deslocamento (erro 383725)
- nunca envolver as legendas do cabeçalho
- correcção do FIXME: remoção do 'resetTimer' (erro 383772)
- não deslocar demasiado o 'applicationheader' se não for um dispositivo móvel
- Adição de uma propriedade para esconder o separador do PageRow correspondente ao AbstractListItem
- correcção do deslocamento com o 'originY' e o fluxo 'baixo-para-cima'
- Eliminação de aviso sobre definir tamanhos tanto em pixels como em pontos
- não despoletar o modo acessível nas vistas invertidas
- ter o rodapé da página em conta
- adição de um exemplo ligeiramente mais complexo de uma aplicação de conversação
- pesquisa mais tolerante a falhas do rodapé correcto
- Verificação da validade de um item antes de o usar
- Respeitar a posição da camada no 'isCurrentPage'
- uso de uma animação em vez de um animador (erro 383761)
- deixar o espaço necessário para o rodapé da página, se possível
- melhor escurecimento para as gavetas do 'applicationitem'
- escurecimento do fundo do 'applicationitem'
- correcção das margens no botão para recuar
- margens adequadas no botão para recuar
- menos avisos no ApplicationHeader
- não usar a escala do Plasma para os tamanhos dos ícones
- novo visual para as pegas

### KItemViews

### KJobWidgets

- Inicialização do estado do botão "Pausa" no seguimento dos elementos gráficos

### KNotification

- Não bloquear o início do serviço de notificações (erro 382444)

### Plataforma KPackage

- remodelação do 'kpackagetool' em relação às opções do 'stringy'

### KRunner

- Limpar as acções anteriores ao actualizar
- Adição de execuções remotas sobre DBus

### KTextEditor

- Migração da API de programação do Document/View para uma solução baseada no QJSValue
- Mostrar os ícones no menu de contexto do contorno de ícones
- Substituição do KStandardAction::PasteText pelo KStandardAction::Paste
- Suporte de escalas fraccionárias na geração da antevisão da barra lateral
- Migração do QtScript para o QtQml

### KWayland

- Tratar os 'buffers' RGB de entrada como pré-multiplicados
- Actualização das saídas do SurfaceInterface quando uma saída global é destruída
- destruição da saída de seguimento do KWayland::Client::Surface
- Evitar o envio de ofertas de dados de uma fonte inválida (erro 383054)

### KWidgetsAddons

- simplificação do 'setContents' ao deixar o Qt deixar fazer a maior parte do trabalho
- KSqueezedTextLabel: Adição do isSqueezed() por conveniência
- KSqueezedTextLabel: Pequenas melhorias na documentação da API
- [KPasswordLineEdit] Definição do 'proxy' de foco para o campo de edição (erro 383653)
- [KPasswordDialog] Reposição da propriedade 'geometry'

### KWindowSystem

- KKeyServer: correcção do tratamento do KeypadModifier (erro 183458)

### KXMLGUI

- Poupança de uma grande quantidade de chamadas stat() no início da aplicação
- Correcção da posição do KHelpMenu no Wayland (erro 384193)
- Eliminação do tratamento problemático do botão direito do rato (erro 383162)
- KUndoActions: uso do 'actionCollection' para definir o atalho

### Plasma Framework

- [ConfigModel] Protecção contra a adição de uma ConfigCategory nula
- [ConfigModel] Permitir, de forma programática, adicionar e remover o ConfigCategory (erro 372090)
- [EventPluginsManager] Exposição do 'pluginPath' no modelo
- [Item de Ícone] Não limpar sem necessidade o 'imagePath'
- [FrameSvg] Uso do QPixmap::mask() em vez do método obsoleto por convolução, através do alphaChannel()
- [FrameSvgItem] Criação do objecto 'margins/fixedMargins' a pedido
- correcção do estado assinalado para os itens do menu
- Forçar o estilo do Plasma para o QQC2 nas 'applets'
- Instalação da pasta 'PlasmaComponents.3/private'
- Eliminação dos restantes temas de 'cores baixas'
- [Theme] Uso do SimpleConfig no KConfig
- Evitar algumas pesquisas desnecessárias do conteúdo do tema
- eliminação dos eventos inúteis de dimensionamento para tamanhos vazios (erro 382340)

### Realce de sintaxe

- Adição da definição de sintaxe para as listas de filtros do Adblock Plus
- Remodelação da definição de sintaxe do Sieve
- Adição do realce de sintaxe dos ficheiros de configuração do QDoc
- Adição da definição de realce de sintaxe para o Tiger
- Escape do hífen nas expressões regulares do rest.xml (erro 383632)
- correcção: o texto simples aparece realçado como PowerShell
- Adição do realce de sintaxe para o Metamath
- Reaproveitamento do realce de sintaxe do LESS no SCSS (erro 369277)
- Adição do realce de Pony
- Remodelação da definição de sintaxe do e-mail

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
