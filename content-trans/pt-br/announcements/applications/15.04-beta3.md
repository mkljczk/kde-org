---
aliases:
- ../announce-applications-15.04-beta3
date: '2015-03-19'
description: O KDE Lança as Aplicações do KDE 15.0 Beta 3.
layout: application
title: O KDE disponibiliza a terceira versão Beta do KDE Applications 15.04
---
19 de março de 2015. Hoje o KDE disponibilizou o terceiro beta da nova versão do KDE Applications. Com as dependências e funcionalidades estabilizadas, o foco das equipes do KDE agora é a correção de erros e pequenos ajustes.

Com diversos aplicativos sendo preparados para o KDE Frameworks 5, as versões do KDE Applications 15.04 precisam de testes aprofundados para manter e melhorar a qualidade e a experiência do usuário. Precisamos dos usuários atuais para manter a alta qualidade do KDE, porque os desenvolvedores simplesmente não conseguem testar todas as configurações possíveis. Contamos com você para nos ajudar a encontrar erros antecipadamente, para que possam ser corrigidos antes da versão final. Por favor, considere juntar-se à equipe, instalando a versão beta e <a href='https://bugs.kde.org/'>comunicando todos os erros encontrados</a>.
