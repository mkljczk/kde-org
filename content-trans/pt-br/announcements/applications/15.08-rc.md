---
aliases:
- ../announce-applications-15.08-rc
date: 2015-08-06
description: O KDE Lança as Aplicações 15.08 Pré-Lançamento.
layout: application
release: applications-15.07.90
title: O KDE disponibiliza a versão Release Candidate do KDE Applications 15.08
---
6 de agosto de 2015. Hoje o KDE disponibilizou o Release Candidate da nova versão do KDE Applications. Com as dependências e funcionalidades estabilizadas, o foco das equipes do KDE agora é a correção de erros e pequenos ajustes.

Com diversos aplicativos sendo preparados para o KDE Frameworks 5, as versões do KDE Applications 15.08 precisam de testes aprofundados para manter e melhorar a qualidade e a experiência do usuário. Precisamos dos usuários atuais para manter a alta qualidade do KDE, porque os desenvolvedores simplesmente não conseguem testar todas as configurações possíveis. Contamos com você para nos ajudar a encontrar erros antecipadamente, para que possam ser corrigidos antes da versão final. Por favor, considere juntar-se à equipe, instalando esta versão e <a href='https://bugs.kde.org/'>comunicando todos os erros encontrados</a>.
