---
aliases:
- ../announce-applications-17.12-beta
custom_spread_install: true
date: 2017-11-17
description: O KDE Lança as Aplicações do KDE 17.12 Beta.
layout: application
release: applications-17.11.80
title: O KDE disponibiliza a versão Beta do KDE Applications 17.12
---
17 de novembro de 2017. Hoje o KDE disponibilizou o beta da nova versão do KDE Applications. Com as dependências e funcionalidades estabilizadas, o foco das equipes do KDE agora é a correção de erros e pequenos ajustes.

Veja mais informações nas <a href='https://community.kde.org/Applications/17.12_Release_Notes'>notas de lançamento da comunidade</a> sobre novos pacotes, pacotes que sejam agora baseados no KF5 e problemas conhecidos. Será disponibilizado um anúncio mais completo para a versão final

As versões das Aplicações do KDE 17.12 precisam de testes aprofundados para manter e melhorar a qualidade e a experiência do utilizador. Os utilizadores actuais são críticos para manter a alta qualidade do KDE, dado que os programadores não podem simplesmente testar todas as configurações possíveis. Contamos consigo para nos ajudar a encontrar erros antecipadamente, para que possam ser rectificados antes da versão final. Por favor, pense em juntar-se à equipa, instalando a versão beta e <a href='https://bugs.kde.org/'>comunicando todos os erros encontrados</a>.

#### Instalando os pacotes binários do KDE Applications 17.12 Beta

<em>Pacotes</em>. Alguns distribuidores de SO's Linux/UNIX forneceram simpaticamente alguns pacotes binários das Aplicações do KDE 17.12 Beta (internamente 17.11.80) para algumas versões das suas distribuições e, em alguns casos, outros voluntários da comunidade também o fizeram. Os pacotes binários adicionais, assim como as actualizações dos pacotes agora disponíveis, poderão aparecer nas próximas semanas.

<em>Localizações dos Pacotes</em>. Para uma lista actualizada dos pacotes binários disponíveis, dos quais o Projecto do KDE foi informado, visite por favor o <a href='http://community.kde.org/Binary_Packages'>Wiki da Comunidade</a>.

#### Compilando o KDE Applications 17.12 Beta

Poderá <a href='http://download.kde.org/unstable/applications/17.11.80/src/'>transferir à vontade</a> o código-fonte completo do 17.12 Beta. As instruções de compilação e instalação estão disponíveis na <a href='/info/applications/applications-17.11.80'>Página de Informações das Aplicações do KDE 17.12 Beta</a>.
