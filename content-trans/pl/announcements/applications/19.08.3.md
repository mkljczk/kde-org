---
aliases:
- ../announce-applications-19.08.3
changelog: true
date: 2019-11-07
description: KDE wydało Aplikacje 19.08.
layout: application
major_version: '19.08'
release: applications-19.08.3
title: KDE wydało Aplikacje 19.08.3
version: 19.08.3
---
{{% i18n_date %}}

Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../19.08.0'> Aplikacji KDE 19.08</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

More than a dozen recorded bugfixes include improvements to Kontact, Ark, Cantor, K3b, Kdenlive, Konsole, Okular, Spectacle, Umbrello, among others.

Wśród ulepszeń znajdują się:

- In the video-editor Kdenlive, compositions no longer disappear when reopening a project with locked tracks
- Okular's annotation view now shows creation times in local time zone instead of UTC
- Keyboard control has been improved in the Spectacle screenshot utility
