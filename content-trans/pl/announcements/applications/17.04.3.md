---
aliases:
- ../announce-applications-17.04.3
changelog: true
date: 2017-07-13
description: KDE wydało Aplikacje KDE 17.04.3
layout: application
title: KDE wydało Aplikacje KDE 17.04.3
version: 17.04.3
---
13 lipca 2017. Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../17.04.0'>Aplikacji KDE 17.04</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

More than 25 recorded bugfixes include improvements to kdepim, dolphin, dragonplayer, kdenlive, umbrello, among others.

To wydanie zawiera także długoterminowo wspieraną wersję Platformy Programistycznej KDE 4.14.34.
