---
aliases:
- ../announce-applications-17.12.1
changelog: true
date: 2018-01-11
description: KDE wydało Aplikacje KDE 17.12.1
layout: application
title: KDE wydało Aplikacje KDE 17.12.1
version: 17.12.1
---
11 stycznia 2018. Dzisiaj KDE wydało pierwsze uaktualnienie stabilizujące <a href='../17.12.0'>Aplikacji KDE 17.12</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

About 20 recorded bugfixes include improvements to Kontact, Dolphin, Filelight, Gwenview, KGet, Okteta, Umbrello, among others.

Wśród ulepszeń znajdują się:

- Sending mails in Kontact has been fixed for certain SMTP servers
- Gwenview's timeline and tag searches have been improved
- JAVA import has been fixed in Umbrello UML diagram tool
