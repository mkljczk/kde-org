---
aliases:
- ../announce-applications-15.08.3
changelog: true
date: 2015-11-10
description: KDE wydało Aplikacje KDE 15.08.3
layout: application
title: KDE wydało Aplikacje KDE 15.08.3
version: 15.08.3
---
10 listopada 2015. Dzisiaj KDE wydało pierwsze uaktualnienie stabilizujące <a href='../15.08.0'>Aplikacji KDE 15.08</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 20 zarejestrowanych poprawek błędów uwzględnia ulepszenia do ark, dolphin, kdenlive, kdepim, kig, lokalize oraz umbrello.

To wydanie zawiera także długoterminowo wspieraną wersję Platformy Programistycznej KDE 4.14.14.
