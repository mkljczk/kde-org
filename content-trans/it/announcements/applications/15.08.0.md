---
aliases:
- ../announce-applications-15.08.0
changelog: true
date: 2015-08-19
description: KDE rilascia Applications 15.08.0.
layout: application
release: applications-15.08.0
title: KDE rilascia KDE Applications 15.08.0
version: 15.08.0
---
{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/dolphin15.08_0.png" alt=`Dolphin nel nuovo aspetto - ora basato su KDE Frameworks 5` class="text-center" width="600px" caption=`Dolphin nel nuovo aspetto - ora basato su KDE Frameworks 5`>}}

19 agosto 2015. Oggi KDE rilascia KDE Applications 15.08.

Con questa versione un totale di 107 applicazioni sono state portate su <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>. La squadra si sta impegnando per portare la migliore qualità sul tuo desktop e queste applicazioni. Quindi contiamo su di te per inviare le tue segnalazioni.

Questo rilascio include le prime versioni basate su KDE Frameworks 5 di <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, <a href='https://www.kde.org/applications/office/kontact/'>la suite Kontact</a>, <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, <a href='https://games.kde.org/game.php?game=picmi'>Picmi</a>, ecc.

### Anteprima tecnica della suite Kontact

Negli ultimi mesi la squadra di KDE PIM ha fatto molti sforzi nella conversione di Kontact a Qt 5 e KDE Frameworks 5. Inoltre, le prestazioni di accesso ai dati sono state notevolmente migliorate da un livello di comunicazione ottimizzato. La squadra di KDE PIM sta lavorando duramente per lucidare ulteriormente la suite Kontact e non vede l'ora di ricevere il tuo riscontro. Per ulteriori e dettagliate informazioni su ciò che è cambiato in KDE PIM, vedi il <a href='http://www.aegiap.eu/kdeblog/'>blog di Laurent Montels</a>.

### Kdenlive e Okular

Questa versione di Kdenlive include molte correzioni nella procedura guidata DVD, insieme a un gran numero di correzioni di bug e altre funzionalità che includono l'integrazione di alcuni rifacimenti più grandi. Ulteriori informazioni sui cambiamenti di Kdenlive possono essere viste nel suo <a href='https://kdenlive.org/discover/15.08.0'>elenco dettagliato delle novità</a>. E Okular ora supporta la transizione di dissolvenza nella modalità di presentazione.

{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/ksudoku_small_0.png" alt=`KSudoku con un puzzle di personaggi` class="text-center" width="600px" caption=`KSudoku con un puzzle di personaggi`>}}

### Dolphin, didattica e giochi

Anche Dolphin è stato portato su KDE Frameworks 5. Marble ha ricevuto un supporto <a href='https://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system'>UTM</a> migliorato e un migliore supporto per le annotazioni, la modifica le sovrapposizioni KML.

Ark ha ricevuto un numero sorprendente di commit tra cui molte piccole correzioni. Kstars ha ricevuto un gran numero di commit, tra cui il miglioramento dell'algoritmo ADU e il controllo dei valori limiti, il salvataggio del ribaltamento del meridiano, la deviazione guida e il limite HFR autofocus nel file di sequenza e l'aggiunta del rate di telescopio e il supporto unpark. KSudoku è migliorato. I commit includono: aggiunge GUI e motore per entrare nei puzzle di Mathdoku e Killer Sudoku e aggiunge un nuovo risolutore basato sull'algoritmo DLX (Donal Knuth Dancing Links).

### Altri rilasci

Insieme a questa versione Plasma 4 sarà pubblicato per l'ultima volta nella sua versione LTS come versione 4.11.22.
