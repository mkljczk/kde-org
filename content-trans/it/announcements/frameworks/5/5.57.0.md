---
aliases:
- ../../kde-frameworks-5.57.0
date: 2019-04-13
layout: framework
libCount: 70
---
### Attica

- Accept any HTTP status between 100 and 199 as benign

### Baloo

- [DocumentIdDB] Silence non-error debug message, warn on errors
- [baloosearch] Allow specifying a time when using e.g. mtime
- [indexcleaner] Avoid removing included folders below excluded ones
- [MTimeDB] Fix lookup for the LessEqual range
- [MTimeDB] Fix lookup when time range should return empty set
- Correct asserts/error handling in MTimeDB
- Protect against invalid parents in the IdTreeDB
- Remove document from MTimeDB/DocumentTimeDB even when timestamp is 0
- Be more precise with mimetype detection (bug 403902)
- [timeline] Canonicalize Url
- [timeline] Fix missing/misplaced SlaveBase::finished() calls
- [balooshow] Several extensions basic file information output
- [timeline] Fix warning, add missing UDS entry for "."
- [balooctl] Reduce nesting level for addOption arguments, cleanup
- React to config updates inside indexer (bug 373430)
- Fix regression when opening DB in read-write mode (bug 405317)
- [balooctl] Cleanup trailing whitespace
- [engine] Unbreak code, revert renaming of Transaction::abort()
- Harmonize handling of underscore in query parser
- Baloo engine: treat every non-success code as a failure (bug 403720)

### BluezQt

- Move Media interface into Adapter
- Manager: Don't require Media1 interface for initialization (bug 405478)
- Device: Check object path in interfaces removed slot (bug 403289)

### Icone Brezza

- Add "notifications" and "notifications-disabled" icons (bug 406121)
- make start-here-kde also available start-here-kde-plasma
- Sublime Merge Icon
- Give applications-games and input-gaming more contrast with Breeze Dark
- Make 24px go-up actually 24px
- Add preferences-desktop-theme-applications and preferences-desktop-theme-windowdecorations icons
- Add symlinks from "preferences-desktop-theme" to "preferences-desktop-theme-applications"
- Remove preferences-desktop-theme in preparation to making it a symlink
- Add collapse/expand-all, window-shade/unshade (bug 404344)
- Improve consistency of window-* and add more
- Make go-bottom/first/last/top look more like media-skip*
- Change go-up/down-search symlinks target to go-up/down
- Improve pixel grid alignment of go-up/down/next/previous/jump
- Change media-skip* and media-seek* style
- Enforce new muted icon style in all action icons

### Moduli CMake aggiuntivi

- Re-enable the setting of QT_PLUGIN_PATH
- ecm_add_wayland_client_protocol: Improve error messages
- ECMGeneratePkgConfigFile: make all vars dependent on ${prefix}
- Add UDev find module
- ECMGeneratePkgConfigFile: add variables used by pkg_check_modules
- Restore FindFontconfig backward compatibility for plasma-desktop
- Add Fontconfig find module

### Integrazione della struttura

- use more appropriate plasma-specific icon for plasma category
- use plasma icon as icon for plasma notification category

### KDE Doxygen Tools

- Update URLs to use https

### KArchive

- Fix crash in KArchive::findOrCreate with broken files
- Fix uninitialized memory read in KZip
- Add Q_OBJECT to KFilterDev

### KCMUtils

- [KCModuleLoader] Pass args to created KQuickAddons::ConfigModule
- Pass focus to child searchbar when KPluginSelector is focused (bug 399516)
- Improve the KCM error message
- Add runtime guard that pages are KCMs in KCMultiDialog (bug 405440)

### KCompletion

- Don't set a null completer on a non-editable combobox

### KConfig

- Add Notify capability to revertToDefault
- point readme to the wiki page
- kconfig_compiler: new kcfgc args HeaderExtension &amp; SourceExtension
- [kconf_update] move from custom logging tech to qCDebug
- Remove reference from const KConfigIniBackend::BufferFragment &amp;
- KCONFIG_ADD_KCFG_FILES macro: ensure a change of File= in kcfg is picked up

### KCoreAddons

- Fix "* foo *" we don't want to bold this string
- Fix Bug 401996 - clicking contact web url =&gt; uncomplete url is selected (bug 401996)
- Print strerror when inotify fails (typical reason: "too many open files")

### KDBusAddons

- Convert two old-style connects to new-style

### KDeclarative

- [GridViewKCM] Fix implicit width calculation
- move the gridview in a separate file
- Avoid fractionals in GridDelegate sizes and alignments

### Supporto KDELibs 4

- Remove find modules provided by ECM

### KDocTools

- Update Ukrainian translation
- Catalan updates
- it entities: update URLs to use https
- Update URLs to use https
- Use Indonesian translation
- Update design to look more similar to kde.org
- Add necessary files to use native Indonesian language for all Indonesian docs

### KFileMetaData

- Implement support for writing rating information for taglib writer
- Implement more tags for taglib writer
- Rewrite taglib writer to use property interface
- Test ffmpeg extractor using mime type helper (bug 399650)
- Propose Stefan Bruns as KFileMetaData maintainer
- Declare PropertyInfo as QMetaType
- Safeguard against invalid files
- [TagLibExtractor] Use the correct mimetype in case of inheritance
- Add a helper to determine actual supported parent mime type
- [taglibextractor] Test extraction of properties with multiple values
- Generate header for new MimeUtils
- Use Qt function for string list formatting
- Fix number localization for properties
- Verify mimetypes for all existing sample files, add some more
- Add helper function to determine mime type based on content and extension (bug 403902)
- Add support for extracting data from ogg and ts files (bug 399650)
- [ffmpegextractor] Add Matroska Video test case (bug 403902)
- Rewrite the taglib extractor to use the generic PropertyMap interface (bug 403902)
- [ExtractorCollection] Load extractor plugins lazily
- Fix extraction of aspect ratio property
- Increase precision of frame rate property

### KHolidays

- Sort the polish holidays categories

### KI18n

- Report human-readable error if Qt5Widgets is required but is not found

### KIconThemes

- Fix padding icon that doesn't exactly match the requested size (bug 396990)

### KImageFormats

- ora:kra: qstrcmp -&gt; memcmp
- Fix RGBHandler::canRead
- xcf: Don't crash with files with unsupported layer modes

### KIO

- Replace currentDateTimeUtc().toTime_t() with currentSecsSinceEpoch()
- Replace QDateTime::to_Time_t/from_Time_t with to/fromSecsSinceEpoch
- Improve executable dialog buttons' icons (bug 406090)
- [KDirOperator] Show Detailed Tree View by default
- KFileItem: call stat() on demand, add SkipMimeTypeDetermination option
- KIOExec: fix error when the remote URL has no filename
- KFileWidget In saving single file mode an enter/return press on the KDirOperator triggers slotOk (bug 385189)
- [KDynamicJobTracker] Use generated DBus interface
- [KFileWidget] When saving, highlight filename after clicking existing file also when using double-click
- Don't create thumbnails for encrypted Vaults (bug 404750)
- Fix WebDAV directory renaming if KeepAlive is off
- Show list of tags in PlacesView (bug 182367)
- Delete/Trash confirmation dialogue: Fix misleading title
- Display the correct file/path in "too bit for fat32" error message (bug 405360)
- Phrase error message with GiB, not GB (bug 405445)
- openwithdialog: use recursive flag in proxy filter
- Remove URLs being fetched when listing job is completed (bug 383534)
- [CopyJob] Treat URL as dirty when renaming file as conflict resolution
- Pass local file path to KFileSystemType::fileSystemType()
- Fix upper/lower case rename on case insensitive fs
- Fix "Invalid URL: QUrl("some.txt")" warnings in Save dialog (bug 373119)
- Fix crash while moving files
- Fix NTFS hidden check for symlinks to NTFS mountpoints (bug 402738)
- Make file overwrite a bit safer (bug 125102)

### Kirigami

- fix listItems implicitWidth
- shannon entropy to guess monochrome icon
- Prevent context drawer from disappearing
- remove actionmenuitembase
- don't try to get the version on static builds
- [Mnemonic Handling] Replace only first occurrence
- sync when any model property updates
- use icon.name in back/forward
- fix toolbars for layers
- Fix errors in kirigami example files
- Add a SearchField and PasswordField component
- fix handle icons (bug 404714)
- [InlineMessage] Do not draw shadows around the message
- immediately layout on order changed
- fix breadcrumb layout
- never show toolbar when the current item asks not to
- manage back/forward in the filter as well
- support back/forward mouse buttons
- Add lazy instantiation for submenus
- fix toolbars for layers
- kirigami_package_breeze_icons: Search among size 16 icons as well
- Fix Qmake based build
- get the attached property of the proper item
- fix logic when to show the toolbar
- possible to disable toolbar for layer's pages
- always show global toolbar on global modes
- signal Page.contextualActionsAboutToShow
- a bit of space to the right of the title
- relayout when visibility changes
- ActionTextField: Properly place actions
- topPadding and BottomPadding
- text on images always need to be white (bug 394960)
- clip overlaysheet (bug 402280)
- avoid parenting OverlaySheet to ColumnView
- use a qpointer for the theme instance (bug 404505)
- hide breadcrumb on pages that don't want a toolbar (bug 404481)
- don't try to override the enabled property (bug 404114)
- Possibility for custom header and footer in ContextDrawer (bug 404978)

### KJobWidgets

- [KUiServerJobTracker] Update destUrl before finishing the job

### KNewStuff

- Switch URLs to https
- Update link to fsearch project
- Handle unsupported OCS commands, and don't over-vote (bug 391111)
- New location for KNSRC files
- [knewstuff] Remove qt5.13 deprecated method

### KNotification

- [KStatusNotifierItem] Send desktop-entry hint
- Allow to set custom hints for notifications

### KNotifyConfig

- Allow selecting only supported audio files (bug 405470)

### KPackage Framework

- Fix finding the host tools targets file in the Android docker environment
- Add cross-compilation support for kpackagetool5

### KService

- Add X-GNOME-UsesNotifications as recognized key
- Add bison minimum version of 2.4.1 due to %code

### KTextEditor

- Fix: apply correctly the text colors of the chosen scheme (bug 398758)
- DocumentPrivate: Add option "Auto Reload Document" to View menu (bug 384384)
- DocumentPrivate: Support to set dictionary on block selection
- Fix Words &amp; Chars String on katestatusbar
- Fix Minimap with QtCurve style
- KateStatusBar: Show lock icon on modified label when in read-only mode
- DocumentPrivate: Skip auto quotes when these looks already balanced (bug 382960)
- Add Variable interface to KTextEditor::Editor
- relax code to only assert in debug build, work in release build
- ensure compatibility with old configs
- more use of generic config interface
- simplify QString KateDocumentConfig::eolString()
- transfer sonnet setting to KTextEditor setting
- ensure now gaps in config keys
- convert more things to generic config interface
- more use of the generic config interface
- generic config interface
- Don't crash on malformed syntax highlighting files
- IconBorder: Accept drag&amp;drop events (bug 405280)
- ViewPrivate: Make deselection by arrow keys more handy (bug 296500)
- Fix for showing argument hint tree on non-primary screen
- Port some deprecated method
- Restore the search wrapped message to its former type and position (bug 398731)
- ViewPrivate: Make 'Apply Word Wrap' more comfortable (bug 381985)
- ModeBase::goToPos: Ensure jump target is valid (bug 377200)
- ViInputMode: Remove unsupported text attributes from the status bar
- KateStatusBar: Add dictionary button
- add example for line height issue

### KWidgetsAddons

- Make KFontRequester consistent
- Update kcharselect-data to Unicode 12.0

### KWindowSystem

- Send blur/background contrast in device pixels (bug 404923)

### NetworkManagerQt

- WireGuard: make marshalling/demarshalling of secrets from map to work
- Add missing support for WireGuard into base setting class
- Wireguard: handle private key as secrets
- Wireguard: peers property should be NMVariantMapList
- Add Wireguard connection type support
- ActiveConnecton: add stateChangedReason signal where we can see the reason of state change

### Plasma Framework

- [AppletInterface] Check for corona before accessing it
- [Dialog] Don't forward hover event when there is nowhere to forward it to
- [Menu] Fix triggered signal
- Reduce the importance of some debug information so actual warnings can be seen
- [PlasmaComponents3 ComboBox] Fix textColor
- FrameSvgItem: catch margin changes of FrameSvg also outside own methods
- Add Theme::blurBehindEnabled()
- FrameSvgItem: fix textureRect for tiled subitems to not shrink to 0
- Fix breeze dialog background with Qt 5.12.2 (bug 405548)
- Remove crash in plasmashell
- [Icon Item] Also clear image icon when using Plasma Svg (bug 405298)
- textfield height based only on clear text (bug 399155)
- bind alternateBackgroundColor

### Scopo

- Add KDE Connect SMS plugin

### QQC2StyleBridge

- the plasma desktop style supports icon coloring
- [SpinBox] Improve mouse wheel behavior
- add a bit of padding in ToolBars
- fix RoundButton icons
- scrollbar based padding on all delegates
- look for a scrollview to take its scrollbar for margins

### Solid

- Allow building without UDev on Linux
- Only get clearTextPath when used

### Evidenziazione della sintassi

- Add syntax definition for Elm language to syntax-highlighting
- AppArmor &amp; SELinux: remove one indentation in XML files
- Doxygen: don't use black color in tags
- Allow line end context switches in empty lines (bug 405903)
- Fix endRegion folding in rules with beginRegion+endRegion (use length=0) (bug 405585)
- Add extensions to groovy highlighting (bug 403072)
- Add Smali syntax highlighting file
- Add "." as weakDeliminator in Octave syntax file
- Logcat: fix dsError color with underline="0"
- fix highlighter crash for broken hl file
- guard target link libraries for older CMake version (bug 404835)

### Informazioni di sicurezza

Il codice rilasciato è stato firmato con GPG utilizzando la chiave seguente: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impronta della chiave primaria: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
