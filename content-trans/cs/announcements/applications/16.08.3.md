---
aliases:
- ../announce-applications-16.08.3
changelog: true
date: 2016-11-10
description: KDE vydává Aplikace KDE 16.08.3
layout: application
title: KDE vydává Aplikace KDE 16.08.3
version: 16.08.3
---
November 10, 2016. Today KDE released the third stability update for <a href='../16.08.0'>KDE Applications 16.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 20 recorded bugfixes include improvements to kdepim, ark, okteta, umbrello, kmines, among others.

This release also includes Long Term Support version of KDE Development Platform 4.14.26.
