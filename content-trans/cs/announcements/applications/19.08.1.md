---
aliases:
- ../announce-applications-19.08.1
changelog: true
date: 2019-09-05
description: KDE vydává Aplikace KDE 19.08.1.
layout: application
major_version: '19.08'
release: applications-19.08.1
title: KDE vydává Aplikace KDE 19.08.1
version: 19.08.1
---
{{% i18n_date %}}

Today KDE released the first stability update for <a href='../19.08.0'>KDE Applications 19.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than twenty recorded bugfixes include improvements to Kontact, Dolphin, Kdenlive, Konsole, Step, among others.

Improvements include:

- Several regressions in Konsole's tab handling have been fixed
- Dolphin again starts correctly when in split-view mode
- Deleting a soft body in the Step physics simulator no longer causes a crash
