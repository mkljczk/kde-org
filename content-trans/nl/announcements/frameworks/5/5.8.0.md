---
aliases:
- ../../kde-frameworks-5.8.0
date: '2015-03-13'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Nieuwe frameworks:

- KPeople, biedt toegang tot alle contacten en de bijbehorende mensen
- KXmlRpcClient, interactie met XMLRPC-services

### Algemeen

- Een aantal reparaties aan het bouwen voor compileren met het komende Qt 5.5

### KActivities

- Hulpbronnen voor het scoren van services is nu gereed

### KArchive

- Stop mislukken op ZIP-bestanden met meerdere data-descriptors

### KCMUtils

- Herstel KCModule::setAuthAction

### KCoreAddons

- KPluginMetadata: voeg ondersteuning toe voor verborgen sleutel

### KDeclarative

- Geef voorkeur aan het blootstellen van lijsten aan QML met QJsonArray
- Behandel niet-standaard devicePixelRatios in afbeeldingen
- Stel hasUrls bloot in DeclarativeMimeData
- Sta gebruikers toe om in te stellen hoeveel horizontale lijnen getekend worden

### KDocTools

- Het bouwen op MacOSX repareren bij gebruik van Homebrew
- Betere styling van media-objecten (afbeeldingen, ...) in documentatie
- Codeer ongeldige tekens in paden gebruikt in XML DTD's, waarmee fouten worden voorkomen

### KGlobalAccel

- Tijd van activeren ingesteld als dynamische eigenschap bij geactiveerde QAction.

### KIconThemes

- QIcon::fromTheme(xxx, someFallback) repareren zou geen fallback teruggeven

### KImageFormats

- Maak PSD-imagelezer onafhankelijk van bytevolgorde van gegevens.

### KIO

- Maak UDSEntry::listFields verouderd en voeg de methode UDSEntry::fields toe die een QVector teruggeeft zonder kostbare conversie.
- De bookmarkmanager alleen synchroniseren als de wijziging door dit proces is gedaan (bug 343735)
- Opstarten van kssld5-dbus-service repareren
- Implementeer quota-used-bytes en quota-available-bytes uit RFC 4331 om informatie over vrije ruimte in http-ioslave in te schakelen.

### KNotifications

- De initialisatie van audio vertragen totdat deze echt nodig is
- Configuratie voor notificatie repareren die niet direct wordt toegepast
- Audio notificaties die stoppen nadat het eerste bestand is afgespeeld repareren

### KNotifyConfig

- Optionele afhankelijkheid van QtSpeech toevoegen om uitgesproken meldingen opnieuw in te schakelen.

### KService

- KPluginInfo: ondersteun lijsten met tekenreeksen als eigenschappen

### KTextEditor

- Statistiek van aantal woorden in de statusbalk toevoegen
- vimode: crash repareren bij verwijderen van de laatste regel in "Visual Line mode"

### KWidgetsAddons

- Laat KRatingWidget werken met devicePixelRatio

### KWindowSystem

- KSelectionWatcher en KSelectionOwner kunnen gebruikt worden zonder af te hangen van QX11Info.
- KXMessages kan gebruikt worden zonder af te hangen van QX11Info

### NetworkManagerQt

- Nieuwe eigenschappen en methoden voor NetworkManager 1.0.0 toegevoegd

#### Plasma framework

- plasmapkg2 voor vertaalde systemen gerepareerd
- Opmaak van tekstballonnen verbeterd
- Maak het mogelijk om plasmoids scripts buiten het plasma-pakket te laden ...

### Wijzigingen aan het bouwsysteem (extra-cmake-modules)

- Breid ecm_generate_headers macro uit om ook CamelCase.h-headers te ondersteunen

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
