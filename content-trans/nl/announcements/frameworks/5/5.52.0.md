---
aliases:
- ../../kde-frameworks-5.52.0
date: 2018-11-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Unbreak gebouwd met BUILD_QCH=TRUE
- Echt fileNameTerms en xAttrTerms gebruiken
- [Balooshow] Toegang buiten grenzen vermijden bij toegang tot corrupte db-gegevens
- [Extractor] Doe geen controle QFile::exists voor een lege url
- [Scheduler] Vlag gebruiken om te volgen wanneer een draaiend iets niets meer doet
- [Extractor] Documenten juist behandelen waar mimetype niet geïndexeerd moet worden
- [Scheduler] Verkeerd gebruik van verouderde QFileInfo::created() tijdstempel repareren (bug 397549)
- [Extractor] Crash van extractor robust maken (bug 375131)
- De FileIndexerConfig als const doorgeven aan de individuele indexeerders
- [Config] Ondersteuning voor KDE4 configuratie verwijderen, stop het schrijven naar willekeurige configuratiebestanden
- [Extractor] Debuggen van opdrachtregel verbeteren, stderr doorsturen
- [Scheduler] Bestandsinformatie uit FilteredDirIterator opnieuw gebruiken
- [Scheduler] Mimetype uit UnindexedFileIterator in indexeren opnieuw gebruiken
- [Scheduler] Overbodige m_extractorIdle variabele verwijderen
- Controles uitvoeren op niet geïndexeerde bestanden en naar niets verwijzende indexitems bij opstarten
- [balooctl] Huidige status- &amp; indexeringsbestand afdrukken wanneer de monitor start (bug 364858)
- [balooctl] Ook op wijzigingen in status monitoren
- [balooctl] "index" opdracht met al geïndexeerde, maar verplaatste bestand repareren (bug 397242)

### BluezQt

- Generatie van API-header van Media en MediaEndpoint toevoegen

### Breeze pictogrammen

- Pictogrammen van pakketbeheerder wijzigen naar emblemen
- Monochrome pictogrammen voor koppelingen als actie opnieuw toevoegen
- Contrast, leesbaarheid en consistentie van embleem verbeteren (bug 399968)
- "Nieuw" mimetype voor .deb bestanden ondersteunen (bug 399421)

### Extra CMake-modules

- ECMAddQch: hulp voor doxygen met vooraf definiëren van meer Q*DECL** macro's
- Bindings: Gebruik van systeempaden voor python installatiemap ondersteunen
- Bindings: INSTALL_DIR_SUFFIX verwijderen uit ecm_generate_python_binding
- Ondersteuning toegevoegd voor de fuzzer sanitizer

### KCMUtils

- ondersteuning voor meerdere pagina's kcms

### KConfig

- Mechanisme toevoegen aan meld andere clients over configuratiewijzigingen via DBus
- Gettermethode voor KConfig::addConfigSources zichtbaar maken

### KConfigWidgets

- KHelpCenter toestaan de juiste pagina's van KDE-help te openen wanneer KHelpClient is opgeroepen met een anker

### KCrash

- KCrash: crash repareren (ironic heh) bij gebruik in een app zonder QCoreApplication

### KDeclarative

- push/pop onderdeel van ConfigModule API maken

### KDED

- Onnodige "No X-KDE-DBus-ServiceName gevonden" bericht verwijderen

### KDesignerPlugin

- Naar product "KF5" in widgetmetagegevens verwijzen, in plaats van "KDE"

### KDocTools

- API dox: minimale docs toevoegen aan KDocTools naamruimte, zodat doxygen het dekt
- Een QCH bestand aanmaken met de API-dox, optioneel, met ECMAddQCH
- Op docbookl10nhelper wachten om te worden gebouwd voor het bouwen van onze eigen manpagina's
- Gespecificeerde Perl interpretator gebruiken in plaats van te vertrouwen op PATH

### KFileMetaData

- [ExtractorCollection] Alleen de beste overeenkomst van extractor-plug-in gebruiken
- [KFileMetaData] Extractie toevoegen voor generieke XML en SVG
- [KFileMetaData] Helper toevoegen voor XML gecodeerde Dublin Core metagegevens
- ondersteuning implementeren voor lezen van ID3-tags uit aiff- en wav-bestanden
- meer tags implementeren voor asf-metagegevens
- ape-tags uit ape- en wavpack-bestanden extraheren
- een lijst leveren met ondersteunde mimetypes voor embeddedimagedata
- met QLatin1String vergelijken en behandeling van alle typen harmoniseren
- Niet crashen bij ongeldige exiv2 gegevens (bug 375131)
- epubextractor: Eigenschap ReleaseYear toevoegen
- taglibextractor: opnieuw maken naar functies specifiek voor type metagegevens
- wma-bestanden/asf-tags toevoegen als ondersteund mimetype
- eigen extractor gebruiken voor testen van de taglibwriter
- een tekenreeksachtervoegsel toevoegen om gegevens te testen en te gebruiken voor testen van unicode van taglibwriter
- controle op tijd bij compileren voor taglib-versie verwijderen
- dekking van testen uitbreiden naar alle ondersteunde mimetypes voor taglibextractor
- Variabele gebruiken met al opgehaalde tekst in plaats van het opnieuw ophalen

### KGlobalAccel

- Melding van wijziging van toetsenbordindeling repareren (bug 269403)

### KHolidays

- Vakantiebestand voor Bahrain toevoegen
- Laat KHolidays ook werken als statische bibliotheek

### KIconThemes

- Een QIconEnginePlugin toevoegen om QIcon deserialisatie toe te staan (bug 399989)
- [KIconLoader] Heap-allocated QImageReader vervangen door een stack-allocated
- [KIconLoader] Embleemrand aanpassen afhankelijk van pictogramgrootte
- Pictogrammen juist centreren als grootte niet past (bug 396990)

### KIO

- Probeer niet terug te vallen naar "minder veilige" SSL-protocollen
- [KSambaShare] Achterliggende / uit pad van share weghalen
- [kdirlistertest] Nog even langer wachten op de luisteraar om te beëindigen
- Sorry-bericht tonen als bestand niet lokaal is
- kio_help: crash in QCoreApplication repareren bij toegang naar help:// (bug 399709)
- Wachten vermijden voor gebruikersacties wanneer voorkomen van stelen van focus in kwin hoog of extreem is
- [KNewFileMenu] Geen lege QFile openen
- Ontbrekende pictogrammen toevoegen aan Places Panel code uit KIO
- De ruwe KFileItem pointers in KCoreDirListerCache weggooien
- Optie 'Aankoppelen' toevoegen aan contextmenu van niet aangekoppelde apparaten in Plaatsen
- Een item 'Eigenschap' toevoegen in het contextmenu van het plaatspaneel
- Waarschuwing "macro expansion producing 'defined' has undefined behavior" repareren

### Kirigami

- Ontbrekende items in statisch bouwen repareren
- basis ondersteuning voor verborgen pagina's
- pictogrammen laden uit de juiste pictogramthema's
- (vele andere reparaties)

### KNewStuff

- Meer nuttige foutmeldingen

### KNotification

- Een crash veroorzaakt door slecht beheer van leeftijd van op canberra gebaseerde geluidsmelding gerepareerd (bug 398695)

### KParts

- Annuleren die niet afgehandeld wordt in verouderde BrowserRun::askEmbedOrSave repareren
- Overzetten naar niet-verouderde variant van KRun::runUrl
- Overgezet KIO::Job::ui() -&gt; KJob::uiDelegate()

### KWayland

- Wayland virtueel bureaubladprotocol toevoegen
- Gegevensbron die verwijderd wordt vóór afwerken van dataoffer receive event bewaken (bug 400311)
- [server] Invoerregio respecteren van sub-surfaces op aanwijzer surfacefocus
- [xdgshell] Positioneringsbeperking van vlagbewerkers voor aanpassen toevoegen

### KWidgetsAddons

- API dox: "Since" notitie van KPageWidgetItem::isHeaderVisible repareren
- Een nieuwe eigenschap headerVisible toevoegen

### KWindowSystem

- Geen iteratoren vergelijken teruggegeven uit twee gescheiden teruggegeven kopieën

### KXMLGUI

- Neem 1..n KMainWindows in kRestoreMainWindows

### NetworkManagerQt

- Ontbrekende opties voor ipv4-instellingen toevoegen
- Vxlan instelling toevoegen

### Plasma Framework

- schalen van pictogrammen op mobiel terugzetten
- Mnemonische labels ondersteunen
- Optie PLASMA_NO_KIO verwijderen
- Zoek op de juiste manier naar terugvalthema's

### Omschrijving

- Dialoogvlag voor JobDialog instellen

### Solid

- [solid-hardware5] Pictogram in lijst zetten in details van apparaat
- [UDisks2] Voeding van station weghalen indien ondersteund (bug 270808)

### Sonnet

- Breken van raden van taal repareren

### Syndication

- Ontbrekend README.md bestand (nodig bij verschillende scripts)

### Accentuering van syntaxis

- Syntaxisaccentueringsbestand voor z/OS CLIST 
- Nieuw bestand voor syntaxisaccentuering voor Job Control Language (JCL) wordt aangemaakt
- Open modus voor te nieuwe Qt-versie verwijderen
- verhoog voor vereiste kate-versie versie + fixup naar huidige versie van framework
- sleutelwoordregel: Invoegen van sleutelwoorden uit een andere taal/bestand ondersteunen
- Geen spellingcontrole voor Metamath behalve in commentaar
- CMake: XCode gerelateerde variabelen en geïntroduceerde eigenschappen toevoegen aan 3.13
- CMake: Nieuwe functies introduceren uit aankomende 3.13 uitgave

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
