---
aliases:
- ../announce-applications-16.08.3
changelog: true
date: 2016-11-10
description: KDE stelt KDE Applicaties 16.08.3 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 16.08.3 beschikbaar
version: 16.08.3
---
10 november 2016. Vandaag heeft KDE de derde stabiele update vrijgegeven voor <a href='../16.08.0'>KDE Applicaties 16.08</a> Deze uitgave bevat alleen reparaties van bugs en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 20 aangegeven reparaties van bugs inclusief verbeteringen aan kdepim, ark, okteta, umbrello, kmines, naast anderen.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van KDE Development Platform 4.14.26.
