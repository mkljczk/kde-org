---
aliases:
- ../../kde-frameworks-5.56.0
date: 2019-03-09
layout: framework
libCount: 70
---
### Baloo

- Se han sustituido varios Q_ASSERT con comprobaciones correctas.
- Se comprueba la longitud de la cadena para evitar un cuelgue en URL de tipo «tags:/».
- [tags_kio] Se ha corregido el etiquetado de archivos locales comprobando barras dobles en URL de tipo «tag:».
- Programar directamente en el código fuente el tiempo restante del intervalo de actualización.
- Se ha corregido una regresión para identificar explícitamente carpetas incluidas.
- Limpieza de entradas idempotentes de la tabla de mapeo de tipos MIME.
- [baloo/KInotify] Notificar si una carpeta se ha movido desde un lugar sin monitorizar (error 342224).
- Manejar carpetas coincidiendo correctamente con subcadenas de carpetas incluidas/excluidas.
- [balooctl] Normalizar las rutas a incluir/excluir antes de usarlas en la configuración.
- Optimizar el operador de asignación por copia Baloo:File, corregir Baloo:File::load(url).
- Usar el contenido para determinar el tipo MIME (error 403902).
- [Extractor] Excluir de la indexación los datos cifrados con GPG (error 386791).
- [balooctl] Interrumpir realmente una orden mal formada en lugar de solo informar de ello.
- [balooctl] Añadir la ayuda ausente para «config set», normalizar la cadena.
- Sustituir el «isDirHidden» recursivo por otro iterativo, permitir un argumento constante.
- Asegurar que solo se añaden directorios al monitor «inotify».

### Iconos Brisa

- Se ha añadido el icono «code-oss».
- [Iconos Brisa] Se han añadido iconos para la cámara de vídeo.
- [Iconos Brisa] Usar los nuevos iconos para suspender, hibernar y cambiar de usuario en el tema de iconos Brisa.
- Se han añadido versiones de 16 y 22 píxeles del icono «gamepad» en los dispositivos.
- Asegurar que los textos de ayuda emergente del tema Brisa son consistentes.
- Se han añadido iconos para las baterías.
- Se ha cambiado el nombre de los iconos «visibility» y «hint» a «view-visible» y «view-hidden».
- [Iconos Brisa] Se han añadido iconos monocromáticos y más pequeños para tarjetas SD y memorias USB (error 404231).
- Se han añadido iconos de dispositivo para drones.
- Se han cambiado los iconos de tipos MIME para las cabeceras y código fuente de C/C++ al estilo círculo/línea.
- Se han corregido las sombras que faltaban en los iconos de tipos MIME para cabeceras de C/C++ (error 401793).
- Se ha eliminado el icono monocromático de preferencias de tipo de letra.
- Se ha mejorado el icono de selección de tipo de letra.
- Usar el nuevo icono campana para todas las notificaciones del escritorio en las preferencias del usuario (error 404094).
- [Iconos Brisa] Se han añadido versiones de 16 píxeles para «gnumeric-font.svg» y un enlace de «gnumeric-font.svg» a «font.svg».
- Se han añadido enlaces simbólicos para los usuarios del sistema en las preferencias que apuntan al icono de «yast-users».
- Se ha añadido el icono «edit-none».

### Módulos CMake adicionales

- Se ha corregido la verificación del nombre de la versión cuando está incluida en un subdirectorio.
- Nuevo módulo para encontrar «Canberra».
- Se han actualizado los archivos de la cadena de herramientas para Android a la realidad.
- Se ha añadido una comprobación de compilación a «FindEGL».

### KActivities

- Usar la ordenación natural en ActivityModel (error 404149).

### KArchive

- Evitar que KCompressionDevice::open se llame cuando no hay ningún motor disponible (error 404240).

### KAuth

- Comunicar al usuario que debería usar principalmente KF5::AuthCore.
- Compilar nuestra propia herramienta auxiliar con AuthCore en lugar de Auth.
- Introducción de KF5AuthCore.

### KBookmarks

- Sustituir la dependencia de KIconThemes con el uso equivalente de QIcon.

### KCMUtils

- Usar el nombre del KCM en la cabecera del KCM.
- Añadir el «ifndef KCONFIGWIDGETS_NO_KAUTH» que faltaba.
- Adaptarse a los cambios en «kconfigwidgets».
- Sincronizar el relleno del módulo QML para que sea un reflejo de las páginas de preferencias del sistema.

### KCodecs

- Se ha corregido CVE-2013-0779.
- QuotedPrintableDecoder::decode: devolver «false» en lugar de un error en lugar de usar una aserción.
- Marcar que KCodecs::uuencode no hace nada.
- nsEUCKRProber/nsGB18030Prober::HandleData no se cuelga si «aLen» es 0.
- nsBig5Prober::HandleData: evitar cuelgue si «aLen» es 0.
- KCodecs::Codec::encode: no usar aserción ni colgarse si «makeEncoder» devuelve «null».
- nsEUCJPProbe::HandleData: evitar un cuelgue si «aLen» es 0.

### KConfig

- Escribir caracteres UTF8 válidos sin codificarlos (error 403557).
- KConfig: manejar correctamente los enlaces simbólicos de directorios.

### KConfigWidgets

- Omitir comparaciones si no se encuentran archivos de esquemas.
- Añadir una nota para KF6 para usar la versión principal de KF5::Auth.
- Usar caché para la configuración por omisión de KColorScheme.

### KCoreAddons

- Crear teléfono: enlaces para números de teléfono.

### KDeclarative

- Usar KPackage::fileUrl para permitir el uso de paquetes rcc de KCM.
- [GridDelegate] Se ha corregido la interferencia de etiquetas largas (error 404389).
- [GridViewKCM] Se ha mejorado el contraste y la legibilidad para los botones en línea de los delegados (error 395510).
- Se ha corregido el indicador de aceptación para el objeto de eventos en DragMove (error 396011).
- Usar un icono «Niguno» distinto en la vista de cuadrícula de KCM.

### KDESU

- kdesud: KAboutData::setupCommandLine() ya define la ayuda y la versión.

### KDocTools

- Se ha portado la compilación cruzada a KF5_HOST_TOOLING.
- Notificar que DocBookXML se ha encontrado solo si se ha encontrado de verdad.
- Se han actualizado la entidades del idioma español.

### KFileMetaData

- [Extractor] Se han añadido metadatos a los extractores (error 404171).
- Se ha añadido un extractor para los archivos AppImage.
- Se ha simplificado el extractor de ffmpeg.
- [ExternalExtractor] Se proporciona una salida más útil cuando el extractor falla.
- Formatear los datos EXIF sobre el flash de las fotos (error 343273).
- Evitar efectos colaterales debido a un valor de número de error obsoleto.
- Usar Kformat para tasa de bits y de muestreo.
- Añadir unidades a fotogramas por segundo y a datos de GPS.
- Añadir función para formatear cadenas a la información de las propiedades.
- Evitar fuga de memoria en un QObject en ExternalExtractor.
- Contemplar el uso de &lt;a&gt; como elemento contenedor en SVG.
- Comprobar Exiv2::ValueType::typeId antes de convertirlo en racional.

### KImageFormats

- ras: corregir cuelgue en archivos dañados.
- ras: proteger también el QVector de paletas.
- ras: arreglar la comprobación de número máximo de archivos.
- xcf: corregir el uso de memoria sin inicializar en documentos dañados.
- Añadir «const», que ayuda a entender mejor la función.
- ras: arreglar el tamaño máximo que «cabe» en un QVector.
- ras: no usar aserción cuando tratemos de asignar un vector enorme.
- rae: proteger contra divisiones por cero.
- xcf: no dividir por cero.
- tga: fallar con elegancia si se producen errores de lectura de datos en bruto.
- ras: fallar con elegancia si altura*anchura*bpp &gt; tamaño.

### KIO

- kioexec: KAboutData::setupCommandLine() ya define la ayuda y la versión.
- Se ha corregido un cuelgue en Dolphin producido al soltar un archivo borrado en la papelera (error 378051).
- Elidir los nombres de archivos muy largos por el centro en textos de error (error 404232).
- Añadir el uso de portales a KRun.
- [KPropertiesDialog] Corregir listas desplegables agrupadas (error 403074).
- Properly attempt to locate the kioslave bin in $libexec AND $libexec/kf5
- Usar AuthCore en lugar de Auth.
- Añadir el nombre de icono a los proveedores de servicios en el archivo .desktop.
- Leer el icono del proveedor de búsquedas IKWS del archivo de escritorio.
- [PreviewJob] Difundir también que somos la aplicación de creación de miniaturas al hacer «stat» sobre los archivos (error 234754).

### Kirigami

- remove the broken messing with contentY in refreshabeScrollView
- Se ha añadido «OverlayDrawer» a las cosas que puede documentar «doxygen».
- map currentItem to the view
- proper color to the arrow down icon
- SwipeListItem: Hacer espacio para las acciones cuando no se permiten eventos de ratón (error 404755).
- ColumnView and partial C++ refactor of PageRow
- we can use at most controls 2.3 as Qt 5.10
- Se ha corregido la altura de los cajones horizontales.
- Se ha mejorado la ayuda emergente en el componente «ActionTextField».
- Se ha añadido un componente «ActionTextField».
- Se ha corregido el espaciado de los botones (error 404716).
- Se ha corregido el tamaño de los botones (error 404715).
- GlobalDrawerActionItem: Hacer referencia correcta del icono usando la propiedad del grupo.
- show separator if header toolbar is invisible
- Se ha añadido un fondo predeterminado para las páginas.
- DelegateRecycler: Fix translation using the wrong domain
- Fix warning when using QQuickAction
- Se han eliminado algunas construcciones de QString innecesarias.
- Don't show the tooltip when the drop-down menu is shown (bug 404371)
- hide shadows when closed
- Se han añadido las propiedades necesarias para el color alternativo.
- revert most of the icon coloring heuristics change
- properly manage grouped properties
- [PassiveNotification] Don't start timer until window has focus (bug 403809)
- [SwipeListItem] Use a real toolbutton to improve usability (bug 403641)
- support for optional alternating backgrounds (bug 395607)
- Solo se muestran asas cuando existen acciones visibles.
- Permitir el uso de iconos de color en los botones de las acciones.
- always show the back button on layers
- Update SwipeListItem doc to QQC2
- fix logic of updateVisiblePAges
- expose visible pages in pagerow
- hide breadcrumb when the current page has a toolbar
- support the toolbarstyle page override
- new property in page: titleDelegate to override the title in toolbars

### KItemModels

- KRearrangeColumnsProxyModel: hacer públicos los dos métodos de mapeo de columnas.

### KNewStuff

- Filtrar contenido no válido de las listas.
- Corregir una fuga de memoria encontrada con «asan».

### KNotification

- Portar a «findcanberra» desde ECM.
- Listar Android como oficialmente soportado.

### Framework KPackage

- Eliminar la advertencia de uso desaconsejado de kpackage_install_package.

### KParts

- plantillas: KAboutData::setupCommandLine() ya define la ayuda y la versión.

### Kross

- Install Kross modules to ${KDE_INSTALL_QTPLUGINDIR}

### KService

- kbuildsycoca5: No hay necesidad de repetir el trabajo de KAboutData::setupCommandLine().

### KTextEditor

- try to improve painting height for text lines - bug 403868 avoid to cut _ and other parts still broken: double height things like mixed english/arab, see bug 404713
- Usar QTextFormat::TextUnderlineStyle en lugar de QTextFormat::FontUnderline (error 399278).
- Hacer posible que se muestren todos los espacios del documento (error 342811).
- No imprimir la líneas sangradas.
- KateSearchBar: Show also search has wrapped hint in nextMatchForSelection() aka Ctrl-H
- katetextbuffer: refactorrizar TextBuffer::save() para separar mejor las rutas de código.
- Usar AuthCore en lugar de Auth.
- Refactorizar KateViewInternal::mouseDoubleClickEvent(QMouseEvent *e).
- Mejoras a la terminación de palabras.
- Set the color scheme to Printing for Print Preview (bug 391678)

### KWayland

- Only commit XdgOutput::done if changed (bug 400987)
- FakeInput: add support for pointer move with absolute coordinates
- Add missing XdgShellPopup::ackConfigure
- [server] Add surface data proxy mechanism
- [server] Add selectionChanged signal

### KWidgetsAddons

- Usar el icono «no» correcto en KStandardGuiItem.

### Framework de Plasma

- [Icon Item] Block next animation also based on window visibility
- Mostrar una advertencia si un complemento necesita una versión más reciente.
- Bump the theme versions because icons changed, to invalidate old caches
- [breeze-icons] Revamp system.svgz
- Asegurar que los textos de ayuda emergente del tema Brisa son consistentes.
- Change glowbar.svgz to smoother style (bug 391343)
- Do background contrast fallback at runtime (bug 401142)
- [breeze desktop theme/dialogs] Add rounded corners to dialogs

### Purpose

- pastebin: no mostrar notificaciones de avance (error 404253).
- sharetool: Mostrar el URL compartido en la parte superior.
- Corregir un error al compartir archivos con espacios o comillas en su nombre usando Telegram.
- Hacer que «ShareFileItemAction» proporcione una salida o un error si se proporcionan (error 397567).
- Permitir que se puedan compartir URL mediante correo electrónico.

### QQC2StyleBridge

- Usar PointingHand al situar el cursor sobre enlaces en etiquetas.
- Respetar la propiedad «display» de los botones.
- Al pulsar sobre áreas vacías de comporta como RePág/AvPág (error 402578).
- Permitir el uso de iconos en listas desplegables.
- Permitir el uso de la API de posicionamiento de texto.
- Permitir el uso de iconos de archivos locales en botones.
- Usar el cursor correcto cuando se sitúe el puntero del ratón sobre la parte editable de una casilla de incremento.

### Solid

- Elevar FindUDev.cmake a los estándares de ECM.

### Sonnet

- Contemplar el caso de que «createSpeller» se pase a un idioma no disponible.

### Resaltado de sintaxis

- Corregir la advertencia de borrado de repositorio.
- MustacheJS: resaltar también los archivos de plantilla, corregir la sintaxis y mejorar el uso de «handlebars».
- Hacer que los contextos no usados sean fatales para el indexador.
- Actualizar example.rmd.fold y test.markdown.fold con nuevos números.
- Instalar la cabecera DefinitionDownloader.
- Actualizar octave.xml a Octave 4.2.0.
- Mejorar el resaltado de TypeScript (y React) y añadir más pruebas para PHP.
- Se ha ampliado el resaltado de sintaxis para lenguajes anidados en markdown.
- Devolver definiciones ordenadas para los nombres de archivos y los tipos MIME.
- Se ha añadido una actualización de referencia que faltaba.
- BrightScript: números unarios y hexadecimales, @attribute.
- Evitar archivos *-php.xml duplicados en «data/CMakeLists.txt».
- Añadir funciones para devolver todas las definiciones para un tipo MIME o nombre de archivo.
- Actualizar el tipo MIME «literate» de haskell.
- Prevenir aserción en la carga de expresiones regulares.
- cmake.xml: actualizaciones para la versión 3.14.
- CubeScript: corregir la codificación de escape de la continuación de líneas en las cadenas.
- Añadir algún «howto» mínimo para añadir pruebas.
- R Markdown: mejorar el plegado de bloques.
- HTML: resaltar el código JSX, TypeScript y MustacheJS en la etiqueta &lt;script&gt; (error 369562).
- AsciiDoc: Añadir el plegado para secciones.
- Resaltado de sintaxis para el esquema de FlatBuffers.
- Añadir algunas constantes y funciones de Maxima.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
