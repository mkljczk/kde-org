---
aliases:
- ../announce-applications-15.04.1
changelog: true
date: '2015-05-12'
description: KDEk, Aplikazioak 15.04.1 kaleratzen du.
layout: application
title: KDEk, KDE Aplikazioak 15.04.1 kaleratzen du
version: 15.04.1
---
May 12, 2015. Today KDE released the first stability update for <a href='../15.04.0'>KDE Applications 15.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 50 recorded bugfixes include improvements to kdelibs, kdepim, kdenlive, okular, marble and umbrello.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.19, KDE Development Platform 4.14.8 and the Kontact Suite 4.14.8.
