---
aliases:
- ../announce-applications-19.08.2
changelog: true
date: 2019-10-10
description: KDEk, Aplikazioak 19.08.2 kaleratu du.
layout: application
major_version: '19.08'
release: applications-19.08.2
title: KDEk, Aplikazioak 19.08.2 kaleratu du
version: 19.08.2
---
{{% i18n_date %}}

Today KDE released the second stability update for <a href='../19.08.0'>KDE Applications 19.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than twenty recorded bugfixes include improvements to Kontact, Dolphin, Gwenview, Kate, Kdenlive, Konsole, Lokalize, Spectacle, among others.

Improvements include:

- High-DPI support got improved in Konsole and other applications
- Switching between different searches in Dolphin now correctly updates search parameters
- KMail can again save messages directly to remote folders
