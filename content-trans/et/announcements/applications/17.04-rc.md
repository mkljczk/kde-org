---
aliases:
- ../announce-applications-17.04-rc
date: 2017-04-07
description: KDE Ships Applications 17.04 Release Candidate.
layout: application
release: applications-17.03.90
title: KDE toob välja rakenduste 17.04 väljalaskekandidaadi
---
7. aprill 2017. KDE laskis täna välja rakenduste uute versioonide väljalaskekandidaadi. Kuna sõltuvused ja omadused on külmutatud, on KDE meeskonnad keskendunud vigade parandamisele ja tarkvara viimistlemisele.

Check the <a href='https://community.kde.org/Applications/17.04_Release_Notes'>community release notes</a> for information on new tarballs, tarballs that are now KF5 based and known issues. A more complete announcement will be available for the final release

KDE rakendused 17.04 vajavad põhjalikku testimist kvaliteedi ja kasutajakogemuse tagamiseks ja parandamiseks. Kasutajatel on tihtipeale õigus suhtuda kriitiliselt KDE taotlusse hoida kõrget kvaliteeti, sest arendajad pole lihtsalt võimelised järele proovima kõiki võimalikke kombinatsioone. Me loodame oma kasutajate peale, kes oleksid suutelised varakult vigu üles leidma, et me võiksime need enne lõplikku väljalaset ära parandada. Niisiis - palun kaaluge mõtet ühineda  meeskonnaga beetat paigaldades <a href='https://bugs.kde.org/'>ja kõigist ette tulevatest vigadest teada andes</a>.
