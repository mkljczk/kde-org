---
date: '2014-04-16'
description: KDE Ships Applications and Platform 4.13.
hidden: true
title: KDE rakendused 4.13 pakuvad semantilise otsingu uut varianti ja mitmeid uusi
  omadusi
---
KDE kogukond annab uhkusega teada KDE rakenduste uusimatest uuendustest, mis pakuvad uusi võimalusi ja parandusi. Palju on vaeva nähtud Isikliku teabe halduri Kontacti kallal, mis pakub uusi omadusi ja on tänu KDE semantilise otsingu tehnoloogia täiustamisele palju mugavam kasutada. Dokumendinäitaja Okulari ja täiustatud tekstiredaktori Kate kasutajaliidest on lihvitud ja omadusi täiustatud. Mängude ja õpirakenduste vallas pakume täiesti uut võõrkeeles kõnelemise harjutamise rakendust Artikulate. Töölauagloobus Marble toetab nüüd Päikese, Kuu ja planeetide näitamist, jalgrattateid ja meremiile. Puslemäng Palapeli pakub erakordseid uusi omadusi ja dimensioone.

{{<figure src="/announcements/4/4.13.0/screenshots/applications.png" class="text-center" width="600px">}}

## KDE Kontact uute omadustega ja palju kiirem

KDE Kontacti komplekt pakub mitme osarakenduse puhul mitmeid uusi omadusi. KMail võimaldab nüüd pilve salvestada ja on täiustanud Sieve toetust serveripoolse filtreerimise hõlbustamiseks. KNotes võib nüüd häireid genereerida ja pakub otsinguvõimalust. Mitmeti on täiustatud Kontacti andmepuhvrikihti, mis kiirendab peaaegu kõiki toiminguid.

### Pilvesalvestuse toetus

KMail pakub nüüd salvestusteenuse toetust, mis lubab suuri manuseid salvestada pilveteenusesse ja lisada vastava lingi kirja. Salvestusteenustest on toetatud Dropbox, Box, KolabServer, YouSendIt, UbuntuOne, Hubic ning üldine WebDavi võimalus. Nendes teenustes hoitavaid faile aitab hallata uus tööriist <em>storageservicemanager</em>.

{{<figure src="/announcements/4/4.13.0/screenshots/CloudStorageSupport.png" class="text-center" width="600px">}}

### Palju parem Sieve toetus

Sieve filtrid ehk tehnoloogia, mis lubab KMailil käidelda filtreid serveris, tuleb nüüd toime puhkuseteadetega mitmes serveris korraga. Tööriist KSieveEditor lubab kasutajal Sieve filtreid muuta ilma tarviduseta server Kontacti lisada.

### Muud muudatused

Kiirotsinguriba on pisut viimistletud ja see saab suurt kasu üldisest otsingu täiustamisest, mida võimaldas KDE arendusplatvormi väljalase 4.13. Otsing on tunduvalt kiirem ja usaldusväärsem. Kirja koostamisel on nüüd toetatud URL-ide lühendamine, olemasolevate tõlgete ja tekstijuppide kasutamine.

PIM-i andmete sildid ja annotatsioonid salvestatakse nüüd Akonadis. Edaspidi on neid võimalik salvestada ka serveris (IMAP või Kolab), mis lubab silte jagada paljude arvutite vahel. Akonadile on lisatud Google Drive'i API toetus. Toetatud on otsimine kolmanda poole pluginatega (mis tähendab tulemuste väga kiiret saamist) ja otsin serveris (elementide otsimiseks, mida kohalik indekseerimisteenus ei ole indekseerinud).

### KNotes, KDE aadressiraamat

Rohkelt vigu ja tüütuid ebakõlasid on parandatud KNotesis. Uued võimalused on märkmetele häirete määramine ja märkmete seas otsimine. Täpsemalt kõnelekse sellest <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>siin</a>. KDE aadressiraamat sai trükkimise toetuse, mille kohta võib lähemalt lugeda <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>siit</a>.

### Jõudluse parandamine

Kontact performance is noticeably improved in this version. Some improvements are due to the integration with the new version of KDE’s <a href='http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search'>Semantic Search</a> infrastructure, and the data caching layer and loading of data in KMail itself have seen significant work as well. Notable work has taken place to improve support for the PostgreSQL database. More information and details on performance-related changes can be found in these links:

- Storage Optimizations: <a href='http://www.progdan.cz/2013/11/kde-pim-sprint-report/'>sprint report</a>
- speed up and size reduction of database: <a href='http://lists.kde.org/?l=kde-pim&amp;m=138496023016075&amp;w=2'>mailing list</a>
- optimization in access of folders: <a href='https://git.reviewboard.kde.org/r/113918/'>review board</a>

### KNotes, KDE aadressiraamat

Rohkelt vigu ja tüütuid ebakõlasid on parandatud KNotesis. Uued võimalused on märkmetele häirete määramine ja märkmete seas otsimine. Täpsemalt kõnelekse sellest <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>siin</a>. KDE aadressiraamat sai trükkimise toetuse, mille kohta võib lähemalt lugeda <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>siit</a>.

## Okular viimistletuma kasutajaliidesega

This release of the Okular document reader brings a number of improvements. You can now open multiple PDF files in one Okular instance thanks to tab support. There is a new Magnifier mouse mode and the DPI of the current screen is used for PDF rendering, improving the look of documents. A new Play button is included in presentation mode and there are improvements to Find and Undo/Redo actions.

{{<figure src="/announcements/4/4.13.0/screenshots/okular.png" class="text-center" width="600px">}}

## Kate pakub täiustatud olekuriba, sulgude sobitamise animeerimist ja täiustatud pluginaid

The latest version of the advanced text editor Kate introduces <a href='http://kate-editor.org/2013/11/06/animated-bracket-matching-in-kate-part/'>animated bracket matching</a>, changes to make <a href='http://dot.kde.org/2014/01/20/kde-commit-digest-5th-january-2014'>AltGr-enabled keyboards work in vim mode</a> and a series of improvements in the Kate plugins, especially in the area of Python support and the <a href='http://kate-editor.org/2014/03/16/coming-in-4-13-improvements-in-the-build-plugin/'>build plugin</a>. There is a new, much <a href='http://kate-editor.org/2014/01/23/katekdevelop-sprint-status-bar-take-2/'>improved status bar</a> which enables direct actions like changing the indent settings, encoding and highlighting, a new tab bar in each view, code completion support for <a href='http://kate-editor.org/2014/02/20/lumen-a-code-completion-plugin-for-the-d-programming-language/'>the D programming language</a> and <a href='http://kate-editor.org/2014/02/02/katekdevelop-sprint-wrap-up/'>much more</a>. The team has <a href='http://kate-editor.org/2014/03/18/kate-whats-cool-and-what-should-be-improved/'>asked for feedback on what to improve in Kate</a> and is shifting some of its attention to a Frameworks 5 port.

## Muud võimalused kõikjal mujal

Konsool muutus pisut paindlikumaks, lubades nüüd kaarte juhtida kohandatud laaditabelitega. Profiili saab salvestada soovitud veergude ja ridade suuruse. Täpsemalt kõneldakse kõigest sellest <a href='http://blogs.kde.org/2014/03/16/konsole-new-features-213'>siin</a>.

Umbrello võimaldab kloonida skeeme ja toob välja targad kontekstimenüüd, mille sisu kohaneb vastavalt valitud vidinale.Täiustatud on nii tagasivõtmist kui ka visuaalseid omadusi. Gwenview <a href='http://agateau.com/2013/12/12/whats-new-in-gwenview-4.12/'>võimaldab nüüd toorpiltide eelvaatlust</a>.

{{<figure src="/announcements/4/4.13.0/screenshots/marble.png" class="text-center" width="600px">}}

Helimikserit KMix saab nüüd DBUS-i protsessidevahelise suhtlemisprotokolli abil juhtida ka eemalt (<a href='http://kmix5.wordpress.com/2013/12/28/kmix-dbus-remote-control/'>üksikasjad</a>), helimenüüd on täiustatud ja lisatud on uus seadistustedialoog (<a href='http://kmix5.wordpress.com/2013/12/23/352/'>üksikasjad</a>), lisaks veel mitmeid veaparandusi ja väiksemaid täiustusi.

Dolphinis on muudetud otsinguliidest, et see kasutaks täielikult ära uut otsingutaristut, samuti on jätkuvalt parandatud jõudlust. Täpsemalt antakse ülevaade möödunud aasta <a href='http://freininghaus.wordpress.com/2013/12/12/a-brief-history-of-dolphins-performance-and-memory-usage'>optimeerimispingutustest siin</a>.

KDE abikeskus võimaldab nüüd mooduleid sortida tähestiku järgi ja on kategooriaid hõlpsama kasutamise huvides ümber korraldanud.

## Mängud ja õpitarkvara

KDE mängud ja õpirakendused on selles väljalaskes saanud palju uuendusi. KDE puslemäng Palapeli sai <a href='http://techbase.kde.org/Schedules/KDE4/4.13_Feature_Plan#kdegames'>sai vahvaid uusi omadusi</a>, mis muudab ka suurte puslede (kuni 10 000 tükki) lahendamise hõlpsamaks neile, kes söandavad niisuguse ülesande üldse ette võtta. KNavalBattle näitab nüüd pärast mängu lõppu laevade asukoht, et näeksid, kuidas ja kus eksiteele sattusid.

{{<figure src="/announcements/4/4.13.0/screenshots/palapeli.png" class="text-center" width="600px">}}

KDE õpirakendustes on mitmeid uusi võimalusi. KStarsis on nüüd D-BUS-i kaudu skriptimisliides ja rakendus võib tarvitada mälukasutuse optimeerimiseks astrometry.net-i veebiteenuste API-t. Cantori skriptiredaktor võimaldab nüüd süntaksi esiletõstmist ning redaktoris on toetatud ka Scilabi ja Python 2 taustaprogrammid. Kaardi- ja navigeerimisrakendus Marble näitab nüüd ka <a href='http://kovalevskyy.tumblr.com/post/71835769570/news-from-marble-introducing-sun-and-the-moon'>Päikese, Kuu</a> ja <a href='http://kovalevskyy.tumblr.com/post/72073986685/news-from-marble-planets'>planeetide asukohta</a> ning võimaldab <a href='http://ematirov.blogspot.ch/2014/01/tours-and-movie-capture-in-marble.html'>virtuaalsete retkede ajal sisse lülitada filmimise režiimi</a>. Varasemat jalgrattateede näitamist on täiendatud cyclestreets.net-i toetusega. Nüüd on toetatud ka meremiilide kasutamine ning klõpsamine <a href='http://en.wikipedia.org/wiki/Geo_URI'>Gei URI-le</a> avab nüüd Marble.

#### KDE rakenduste paigaldamine

KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations and CPU architectures such as ARM and x86, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href='http://windows.kde.org'>KDE software on Windows</a> site and Apple Mac OS X versions on the <a href='http://mac.kde.org/'>KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported. <a href='http://plasma-active.org'>Plasma Active</a> is a user experience for a wider spectrum of devices, such as tablet computers and other mobile hardware.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.13.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Tarkvarapaketid

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.13.0 for some versions of their distribution, and in other cases community volunteers have done so.

##### Pakettide asukohad

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.13.0'>Community Wiki</a>.

The complete source code for 4.13.0 may be <a href='/info/4/4.13.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.13.0 are available from the <a href='/info/4/4.13.0#binary'>4.13.0 Info Page</a>.

#### Nõuded süsteemile

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.

In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.
