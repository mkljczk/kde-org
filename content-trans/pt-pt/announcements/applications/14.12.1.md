---
aliases:
- ../announce-applications-14.12.1
changelog: true
date: '2015-01-08'
description: O KDE Lança as Aplicações do KDE 14.12.1.
layout: application
title: O KDE Lança as Aplicações do KDE 14.12.1
version: 14.12.1
---
13 de Janeiro de 2015. Hoje o KDE lançou a primeira actualização de estabilidade para as <a href='../14.12.0'>Aplicações do KDE 14.12</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 50 correcções de erros registadas incluem melhorias na ferramenta de pacotes Ark, na aplicação de aprendizagem de pronúncia Artikulate, no visualizador de documentos Okular, no cliente de ambientes de trabalho remotos KRDC e na ferramenta de UML Umbrello.

Esta versão também inclui as versões de Suporte de Longo Prazo da Área de Trabalho do Plasma 4.11.15, a Plataforma de Desenvolvimento do KDE 4.14.4 e o pacote Kontact 4.14.4.
