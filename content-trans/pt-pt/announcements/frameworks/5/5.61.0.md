---
aliases:
- ../../kde-frameworks-5.61.0
date: 2019-08-10
layout: framework
libCount: 70
---
### Baloo

- Compilação com o KIOCore em vez do KIOWidgets nos 'kioslaves'
- [IndexCleaner] ignorar os itens inexistentes dentro da configuração

### BluezQt

- Correcção de estoiro devido ao ponteiro 'q' nunca ser inicializado
- Não incluir o 'bluezqt_dbustypes.h' nos ficheiros de inclusão instalados

### Ícones do Brisa

- Adição do ícone "user-others" (erro 407782)
- Mudança do "edit-none" para uma ligação simbólica para o "dialog-cancel"
- Eliminação das versões redundantes e monocromáticas do 'applications-internet'
- Adição dos ícones 'view-pages-*', necessários para a selecção de disposição das páginas no Okular (erro 409082)
- Uso de setas no sentido dos ponteiros do relógio para os ícones '_refresh_' e 'update-*' (erro 409914)

### Módulos Extra do CMake

- android: Permitir a substituição do ANDROID_ARCH e do ANDROID_ARCH_ABI como variáveis de ambiente
- Notificação dos utilizadores quando não usarem o KDE_INSTALL_USE_QT_SYS_PATHS sobre o prefix.sh
- Oferta de um valor predefinido do CMAKE_INSTALL_PREFIX mais válido
- Tornar predefinido o tipo de compilação "Debug" (Depuração) ao compilar uma versão extraída do Git

### KActivitiesStats

- Adição do termo Date às estatísticas do KActivities para filtrar pela data do evento no recurso

### KActivities

- Simplificação do código do 'previous-/nextActivity' no 'kactivities-cli'

### Ferramentas de Doxygen do KDE

- Correcção da verificação de pastas para o 'metainfo.yaml' com caracteres não-ASCII e o Python 2.7
- Registo de localizações de ficheiros inválidas (com o repr()) em vez de estoirar por inteiro
- geração da lista de ficheiros de dados na hora

### KArchive

- KTar::openArchive: Não estoirar uma validação se o ficheiro tiver duas pastas de raiz
- KZip::openArchive: Não estoirar a validação ao abrir ficheiros com problemas

### KCMUtils

- adaptação às alterações de interface no KPageView

### KConfig

- Segurança: remoção do suporte para $(...) nas chaves de configuração com a marcação [$e]
- Inclusão da definição da classe usada no cabeçalho

### KCoreAddons

- Adição da função KFileUtils::suggestName para sugerir um nome de ficheiro único

### KDeclarative

- Scrollview - Não preencher o item-pai com a área (erro 407643)
- introdução do FallbackTapHandler
- 'Proxy' de QML do KRun: correcção da confusão entre locais/URL's
- Eventos do calendário: possibilidade de os 'plugins' mostrarem detalhes do evento

### KDED

- ficheiro .desktop do kded5: uso de tipo válido (Service) para eliminar o aviso do kservice

### Suporte para a KDELibs 4

- 'Plugin' do Designer: uso consistente do "KF5" nos nomes &amp; textos dos grupos
- Não aconselhar o uso do KPassivePopup

### KDesignerPlugin

- exposição do novo KBusyIndicatorWidget
- Remoção da geração de 'plugins' do Designer para o KF5WebKit

### WebKit do KDE

- Uso da antevisão do ECMAddQtDesignerPlugin em vez do KF5DesignerPlugin
- Adição de opção para compilar um 'plugin' do Qt Designer (BUILD_DESIGNERPLUGIN, por omissão ON)

### KFileMetaData

- Actualização da extracção do 'mobipocket', mas mantida desactivada

### KHolidays

- Adição dos dias de substituição dos feriados públicos na Rússia para 2019-2020
- Actualização dos feriados da Rússia

### KIconThemes

- Reposição da "Verificação se group &lt; LastGroup', dado que o KIconEffect não trata o UserGroup de qualquer forma"

### KIO

- Descontinuação do 'suggestName' em detrimento do existente no KCoreAddons
- Correcção do problema em não conseguir entrar em dadas pastas nalguns servidores de FTP com a configuração regional em Turco (erro 409740)

### Kirigami

- Remodelação do Kirigami.AboutPage
- Uso consistente do Units.toolTipDelay em vez de valores fixos
- ajuste adequado do tamanho dos cartões quando o mesmo está restrito
- esconder as ondinhas quando não queremos poder carregar nos itens
- fazer com que a pega siga a altura arbitrária do contentor
- [SwipeListItem] Ter em conta a visibilidade da barra de deslocamento e as proporções de tamanho para as acções da pega e internas
- Remoção da escala da unidade do tamanho dos ícones no isMobile
- mostrar sempre o botão 'recuar' nas camadas &gt; 1
- esconder as acções com submenus do menu 'mais'
- posição predefinida do ActionToolBar no Cabeçalho
- Não aparecer um Z grande debaixo das janelas
- uso da opacidade para esconder os botões que não cabem
- adição do espaço apenas quando preencher a largura
- completamente retrocompatível com o 'showNavigationButtons' como booleano
- mais granularidade no globalToolBar.showNavigationButtons

### KItemModels

- O David Faure é agora o responsável de manutenção pelo KItemModels
- KConcatenateRowsProxyModel: adição de nota em que o Qt 5.13 oferece o QConcatenateTablesProxyModel

### Plataforma KPackage

- Oferta do 'metadata.json' ao pedir os meta-dados do pacote
- PackageLoader: Uso do âmbito correcto para o KCompressionDevice

### KPeople

- declarativo: actualização da lista de acções quando muda a pessoa
- declarativo: não estoirar quando a API for mal usada
- personsmodel: Adição do 'phoneNumber' (nº telefone)

### KService

- Exposição do X-KDE-Wayland-Interfaces
- Correcção da compilação do KService no Android
- KService: remoção do conceito problemático de base de dados global do Sycoca
- Remoção de código de remoção bastante perigoso com o 'kbuildsycoca5 --global'
- Correcção de recursividade infinita e verificação quando a BD do Sycoca é ilegível pelo utilizador (p.ex., pertencente ao 'root')
- Descontinuação do KDBusServiceStarter. Toda a sua utilização no 'kdepim' já foi eliminada, sendo a activação por D-Bus uma melhor solução
- Possibilidade do KAutostart ser construído com uma localização absoluta

### KTextEditor

- Gravação e carregamento das margens da página
- Não persistir a autenticação
- Reconfiguração do atalho predefinido para "Mudar de modo de introdução de dado" para não entrar em conflito com o 'konsolepart' (erro 409978)
- Fazer com que o modelo de completação de palavras-chave devolva uma HideListIfAutomaticInvocation por omissão
- Minimapa: Não capturar o 'click' com o botão esquerdo do rato nos botões para cima/baixo
- permitir até 1024 intervalos de realce em ve de não realçar essa linha de todo se for atingido esse limite
- correcção da dobragem de linhas com a posição final na coluna 0 de uma dada linha (erro 405197)
- Adição de opção para tratar alguns caracteres também como "parêntesis automáticos" só quando tivermos uma selecção
- Adição de uma acção para inserir uma mudança de linha não-indentada (erro 314395)
- Adição de configuração para activar/desactivar o arrastamento e largada de texto (activado por omissão)

### KUnitConversion

- Adição de unidades de Dados Binários (bits, quilobytes, kibibytes ... yotabytes)

### Plataforma da KWallet

- Mudança da inicialização da 'kwalletd' para um momento anterior (erro 410020)
- Remoção por completo do agente de migração do kde4 (erro 400462)

### KWayland

- Uso do 'wayland-protocols'

### KWidgetsAddons

- introdução do conceito de cabeçalho e rodapé no KPageView
- [Indicador de Ocupado] Correspondência da duração à versão do QQC2-desktop-style
- Adição de uma janela de aviso com uma secção de detalhes colapsável
- nova classe KBusyIndicatorWidget semelhante ao BusyIndicator do QtQuick

### KWindowSystem

- [platforms/xcb] Uso da extensão XRES para obter o PID real da janela (erro 384837)
- Migração do KXMessages para fora do QWidget

### KXMLGUI

- Adição de espaços de expansão como uma opção de personalização para as barras de ferramentas
- Uso de ícones de acção monocromáticos para os botões do KAboutData
- Remoção da ligação do 'visibilityChanged' em detrimento do 'eventFilter' existente

### ModemManagerQt

- Possibilidade de actualizar o tempo-limite predefinido do DBus em todas as interfaces

### NetworkManagerQt

- dispositivo: inclusão do reapplyConnection() na interface

### Plataforma do Plasma

- [ToolButtonStyle] Uso do mesmo grupo de cores para o estado à passagem do cursor
- Tratamento do ficheiro de cores no instalador de temas do Plasma falso
- Instalação do tema do Plasma na XDG_DATA_DIR local para o teste dos ícones
- Aplicação da mudança de duração do indicador de ocupado do D22646 para o estilo do QQC2
- Compilação dos 'plugins' de estrutura de pacotes para a sub-pasta esperada
- Mudança do realce para ButtonFocus
- Correcção da execução do 'dialognativetest' sem instalar
- Pesquisa pelo 'plugin' do outro plasmóide
- [Indicador de Ocupado] Correspondência da duração à versão do QQC2-desktop-style
- Adição de componentes em falta no org.kde.plasma.components 3.0
- Actualização dos ícones de actualização e reinício para reflectir as novas versões dos ícones do Brisa (erro 409914)
- O 'itemMouse' não está definido no plasma.components 3.0
- uso do 'clearItems' quando for apagada uma 'applet'
- Correcção de estoiro se o 'switchSize' for ajustado durante a configuração inicial
- Melhoria na 'cache' de 'plugins'

### Purpose

- Phabricator: abertura de uma nova diferença automaticamente no navegador
- Correcção da extracção. Modificação de Victor Ryzhykh

### QQC2StyleBridge

- Correcção de guarda inválida que proíbe as barras de estilo com valores negativos
- Abrandamento da velocidade de rotação do indicador de ocupado
- Correcção do "Erro de tipo" ao criar um TextField com o campo 'focus: true'
- [ComboBox] Configuração da política de fecho para fechar ao carregar fora dele, em vez de ser só fora do item-pai (erro 408950)
- [SpinBox] Definição do 'renderType' (erro 409888)

### Solid

- Certificação de que as infra-estruturas do Solid são reentrantes

### Realce de Sintaxe

- TypeScript: correcções das palavras-chave nas expressões condicionais
- Correcção dos locais do gerador e dos testes do CMake
- Adição do suporte para palavras-chave de QML adicionais que não fazem parte do JavaScript
- Actualização do realce do CMake

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
