---
aliases:
- ../announce-applications-14.12.1
changelog: true
date: '2015-01-08'
description: KDE Ships KDE Applications 14.12.1.
layout: application
title: KDE에서 KDE 프로그램 14.12.1 출시
version: 14.12.1
---
January 13, 2015. Today KDE released the first stability update for <a href='../14.12.0'>KDE Applications 14.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

압축 도구 Ark, Umbrello UML 모델러, 문서 뷰어 Okular, 발음 학습 프로그램 Artikulate 및 원격 데스크톱 클라이언트 KRDC에 50개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.15, KDE Development Platform 4.14.4 and the Kontact Suite 4.14.4.
