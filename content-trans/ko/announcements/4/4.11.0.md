---
aliases:
- ../4.11
custom_about: true
custom_contact: true
date: 2013-08-14
description: KDE Ships Plasma Workspaces, Applications and Platform 4.11.
title: KDE 소프트웨어 모음 4.11
---
{{<figure src="/announcements/4/4.11.0/screenshots/plasma-4.11.png" class="text-center" caption=`KDE Plasma 작업 공간 4.11` >}} <br />

August 14, 2013. The KDE Community is proud to announce the latest major updates to the Plasma Workspaces, Applications and Development Platform delivering new features and fixes while readying the platform for further evolution. The Plasma Workspaces 4.11 will receive long term support as the team focuses on the technical transition to Frameworks 5. This then presents the last combined release of the Workspaces, Applications and Platform under the same version number.<br />

이 릴리스는 인도 출신의 자유 오픈소스 소프트웨어 챔피언 <a href='http://en.wikipedia.org/wiki/Atul_Chitnis'>Atul 'toolz' Chitnis</a>를 기립니다. Atul은 2001년부터 인도의 자유 오픈소스 소프트웨어의 주요 행사인 Linux 방갈로르 및 FOSS.IN 컨퍼런스를 이끌었습니다. 인도 KDE 팀은 2005년 12월 첫 번째 FOSS.in에서 결성되었습니다. FOSS.IN의 KDE Project Day에서 Atul이 적극적으로 지지한 덕분에 가능했습니다. Atul은 암 투병 끝에 6월 3일 세상을 떠났습니다. 고인의 명복을 빕니다. 저희는 고인의 더 나은 세상을 위한 공헌을 기념합니다.

이 릴리스는 언어 54개로 번역되었으며 KDE의 추가 월간 버그 수정 릴리스에서 더 많은 언어가 추가될 수도 있습니다. 문서화 팀에서는 이번 릴리스에서 프로그램 도움말 91개를 업데이트했습니다.

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 Continues to Refine User Experience</a>

Gearing up for long term maintenance, Plasma Workspaces delivers further improvements to basic functionality with a smoother taskbar, smarter battery widget and improved sound mixer. The introduction of KScreen brings intelligent multi-monitor handling to the Workspaces, and large scale performance improvements combined with small usability tweaks make for an overall nicer experience.

## <a href="./applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Applications 4.11 Bring Huge Step Forward in Personal Information Management and Improvements All Over</a>

This release marks massive improvements in the KDE PIM stack, giving much better performance and many new features. Kate improves the productivity of Python and Javascript developers with new plugins, Dolphin became faster and the educational applications bring various new features.

## <a href="./platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 Delivers Better Performance</a>

이번 KDE 플랫폼 4.11 릴리스는 안정성에 계속 초점을 맞추고 있습니다. 미래의 KDE 프레임워크 5.0 릴리스에 추가할 기능이 계속 구현 중이며, 안정 릴리스에는 Nepomuk 프레임워크 최적화가 추가되었습니다.

<br />
When upgrading, please observe the <a href='http://community.kde.org/KDE_SC/4.11_Release_Notes'>release notes</a>.<br />

## 주위에 알리고 일어나는 일 보기: &quot;KDE&quot; 태그

KDE를 소셜 웹에 널리 알려 주십시오. 뉴스 사이트에 글을 올리고 delicious, digg, reddit, twitter, identi.ca와 같은 채널로 널리 알려 주십시오. Facebook, Flickr, ipernity, Picasa와 같은 서비스에 새로운 환경의 스크린샷을 업로드하고 그룹과 공유하십시오. 스크린 캐스트를 만들어 YouTube, Blip.tv 및 Vimeo에 업로드해 주십시오. 블로그나 소셜 미디어에 글을 게시할 때 "KDE" 태그나 문구를 추가하면 KDE 홍보 팀에서 KDE 소프트웨어 4.11 릴리스가 얼마나 잘 알려져 있는지를 쉽게 파악할 수 있습니다.

## 릴리스 파티

평소처럼 KDE 커뮤니티 회원들은 전 세계에서 릴리스 파티를 주최합니다. 일부는 이미 예정되어 있고 추가로 더 개최될 예정입니다. 파티 전체 목록을 보려면 <a href='http://community.kde.org/Promo/Events/Release_Parties/4.11'>여기</a>를 참조하십시오. 누구나 참여할 수 있습니다! 새로운 사람들을 만나고 정보를 교환하는 것 외에도 간단한 다과가 있을 수 있습니다. KDE에서 일어나는 일을 익히고, KDE에 참여하고, 다른 사람들과 기여자를 만날 수 있는 장소가 될 것입니다.

릴리스 파티를 직접 개최하는 것을 추천합니다. 누구나 참여할 수 있는 파티를 열어 보세요! <a href='http://community.kde.org/Promo/Events/Release_Parties'>파티를 주최하는 방법</a>에 대해서 더 알아볼 수 있습니다.

## 릴리스 공지 정보

These release announcements were prepared by Jos Poortvliet, Sebastian Kügler, Markus Slopianka, Burkhard Lück, Valorie Zimmerman, Maarten De Meyer, Frank Reininghaus, Michael Pyne, Martin Gräßlin and other members of the KDE Promotion Team and the wider KDE community. They cover highlights of the many changes made to KDE software over the past six months.

#### KDE 지원

<a href="http://jointhegame.kde.org/"> <img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3" alt="Join the Game"/> </a>

KDE e.V.'s new <a href='http://jointhegame.kde.org/'>Supporting Member program</a> is now open. For &euro;25 a quarter you can ensure the international community of KDE continues to grow making world class Free Software.
