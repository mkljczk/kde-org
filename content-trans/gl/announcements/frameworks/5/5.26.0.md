---
aliases:
- ../../kde-frameworks-5.26.0
date: 2016-09-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Engadir Qt5Network como dependencia pública

### BluezQt

- Corrixir o directorio de inclusión no ficheiro pri

### Iconas de Breeze

- Engadir definicións de prefixo de nome de espazo que faltaban
- Comprobar que as iconas SVG están ben formadas
- Corrixir todas as iconas de edit-clear-location-ltr (fallo 366519)
- engadir unha icona para efectos de KWin
- cambiar o nome de caps-on a input-caps-on
- engadir iconas de maiúsculas para a entrada de texto
- engadir algunhas iconas específicas de GNOME de Sadi58
- engadir iconas de aplicación de gnastyle
- Optimizáronse as iconas de Dolphin, Konsole e Umbrello para 16 px, 22 px e 32 px
- Actualizar a icona de VLC para 22 px, 32 px e 48 px
- Engadir unha icona de aplicación para Subtitle Composer
- Corrixir a nova icona de Kleopatra
- Engadiuse unha icona de aplicación para Kleopatra
- Engadíronse iconas para Wine e Wine-qt
- corrixir un fallo de palabra de presentación grazas a Sadi58 (fallo 358495)
- engadir unha icona de system-log-out de 32 px
- engadir iconas de sistema de 32 px, retirar iconas de sistema coloradas
- engadir iconas para Pidgin e Banshee
- retirar a icona de aplicación de VLC por un problema de licenza, engadir unha nova icona de VLC (fallo 366490)
- Engadir compatibilidade coas iconas de gthumb
- usar HighlightedText para as iconas de cartafoles
- agora as iconas do cartafol de lugares usan a folla de estilos (cor de salientado)

### Módulos adicionais de CMake

- ecm_process_po_files_as_qm: Saltar as traducións difusas
- O nivel predeterminado das categorías de rexistro debería ser Info (información) en vez de Warning (aviso)
- Documentar a variábel ARGS nos destinos create-apk-*
- Crear unha proba que valida a información de appstream dos proxectos

### Ferramentas de Doxygen de KDE

- Engadir a condición se as plataformas dos grupos non están definidos
- Modelo: ordenar as plataformas alfabeticamente

### KCodecs

- Traer de kdelibs o ficheiro que se usa para xerar kentities.c

### KConfig

- Engadir unha entrada de Doar a KStandardShortcut

### KConfigWidgets

- Engadir a acción estándar de Doar

### KDeclarative

- [kpackagelauncherqml] Asumir que o nome do ficheiro de escritorio é o mesmo que o pluginId
- Cargar a configuración de renderización de QtQuick dun ficheiro de configuración e definir a predeterminada
- icondialog.cpp - corrección de compilador axeitada que non fai sombra a m_dialog
- Corrixir unha quebra cando non hai ningún QApplication dispoñíbel
- expoñer o dominio da tradución

### Compatibilidade coa versión 4 de KDELibs

- Corrixir un erro de compilación en Windows en kstyle.h

### KDocTools

- engadir rutas de configuración, caché e datos a general.entities
- Sincronizado coa versión en inglés
- Engadir as entidades de tecla Espazo e Meta a src/customization/en/user.entities

### KFileMetaData

- Só requirir Xattr se o sistema operativo é Linux
- Restaurar a construción de Windows

### KIdleTime

- [xsync] XFlush en simulateUserActivity

### KIO

- KPropertiesDialog: retirar a nota de aviso da documentación, solucionouse o fallo
- [programa de probas] resolver as rutas relativas usando QUrl::fromUserInput
- KUrlRequester: corrixir a caixa de erro ao seleccionar un ficheiro e abrir de novo o diálogo de ficheiro
- Fornecer unha reserva se os escravos non listan a entrada . (fallo 366795)
- Corrixir a creación de ligazóns simbólicas polo protocolo «desktop»
- KNewFileMenu: usar KIO::linkAs en vez de KIO::link ao crear ligazóns simbólicas
- KFileWidget: corrixir unha «/» dupla nunha ruta
- KUrlRequester: usar a sintaxe estática de connect(), era inconsistente
- KUrlRequester: pasar window() como pai a QFileDialog
- evitar chamar a connect(null, .....) desde KUrlComboRequester

### KNewStuff

- descomprimir os arquivos en subcartafoles
- Deixar de permitir instalar no cartafol de datos xenérico por mor dun potencial problema de seguranza

### KNotification

- Obter a propiedade ProtocolVersion de StatusNotifierWatcher de maneira asíncrona

### Infraestrutura de paquetes

- silenciar os avisos de obsolescencia de contentHash

### Kross

- Reverter «Retirar dependencias innecesarias de KF5»

### KTextEditor

- retirar un conflito de aceleradores (fallo 363738)
- corrixir o salientado de enderezo de correo electrónico en Doxygen (fallo 363186)
- detectara algúns ficheiros JSON adicionais, como os nosos propios proxectos ;)
- mellorar a detección de tipo MIME (fallo 357902)
- Fallo 363280 - realce: c++: #if 1 #endif #if defined (A) aaa #elif defined (B) bbb #endif (fallo 363280)
- Fallo 363280 - realce: c++: #if 1 #endif #if defined (A) aaa #elif defined (B) bbb #endif
- Fallo 351496 - O pregado de Python non funciona ao escribir ao principio (fallo 351496)
- Fallo 365171 - Realce de sintaxe de Python: non funciona correctamente para secuencias de escape (fallo 365171)
- Fallo 344276 - o nowdoc de PHP non se prega correctamente (fallo 344276)
- Fallo 359613 - Algunhas propiedades de CSS3 non están permitidas no realce de sintaxe (fallo 359613)
- Fallo 367821 - sintaxe de wineHQ: a sección dun ficheiro de rexistro non se realza correctamente (fallo 367821)
- Mellorar a xestión de ficheiros de intercambio se se indica un directorio de intercambio
- Corrixir unha quebra ao cargar de novo documentos con liñas axustadas polo límite da lonxitude de liña (fallo 366493)
- Corrixir quebras constantes relacionadas coa barra de ordes de vi (fallo 367786)
- Corrección: agora os números de liña dos documentos impresos empezan en 1 (fallo 366579)
- Facer unha copia de seguranza de ficheiros remotos: tamén tratar os ficheiros montados como ficheiros remotos
- limpar a lóxica da creación da barra de busca
- engadir salientado para Magma
- Permitir unicamente un nivel de recursión
- Corrixir o ficheiro de intercambio roto en Windows
- Parche: engadir compatibilidade con bitbake para o motor de realce de sintaxe
- chaves automáticas: ver o atributo de corrección ortográfica onde se escribiu o carácter (fallo 367539)
- Salientar QMAKE_CFLAGS
- Non retirar o contexto principal
- Engadir algúns nomes de executábel habituais

### KUnitConversion

- Engadir a unidade de masa británica «pedra»

### Infraestrutura de KWallet

- Mover o docbook kwallet-query ao subdirectorio correcto
- Corrección gramatical de inglés: an → one

### KWayland

- Facer linux/input.h opcional en tempo de compilación

### KWidgetsAddons

- Corrixir o fondo dos caracteres non BMP
- Engadir busca de UTF-8 escapada en octal de C
- Facer que o KMessageBoxDontAskAgainMemoryStorage predeterminado garde en QSettings

### KXMLGUI

- Migrar á acción estándar de doar
- Deixar de usar a obsoleta authorizeKAction

### Infraestrutura de Plasma

- corrixir a icona de dispositivo de 22 px, a icona non funcionaba no ficheiro anterior
- WindowThumbnail: facer as chamadas de GL no fío correcto (fallo 368066)
- Make plasma_install_package work with KDE_INSTALL_DIRS_NO_DEPRECATED
- engadir marxe e espazo á icona start.svgz
- corrixir cousas da folla de estilos na icona de computador
- engadir unha icona de computador e outra de portátil para Kicker (fallo 367816)
- Corrixir o aviso de que non se pode asignar undefined a double en DayDelegate
- corrixir que os ficheiros svgz de folla de estilos non están namorados de min
- cambiar o nome das iconas de 22 px a 22-22-x e a das iconas de 32 px a x para Kicker
- [TextField de PlasmaComponents] Non molestarse en cargar iconas de botóns que non se usan
- Protección adicional en Containment::corona no caso especial de área de notificacións
- Ao marcar un contedor como eliminado, marcar tamén todos os miniaplicativos subordinados como eliminados; corrixe que non se elimine a configuración dos contedores da área de notificacións
- Corrixir a icona do notificador de dispositivos
- engadir system-search a system nos tamaños 32 px e 22 px
- engadir iconas monocroma a Kicker
- Definir o esquema de cores na icona system-search
- Mover system-search a system.svgz
- Corrixir o «X-KDE-ParentApp» incorrecto ou non definido nas definicións dos ficheiros de escritorio
- Corrixir a documentación da API de Plasma::PluginLoader: mestura de miniaplicativos, motores de datos, servizos, etc.
- engadir unha icona de system-search para o tema de SDDM
- engadir a icona 32px de Nepomuk
- actualizar a icona de área táctil para a área de notificacións
- Retirar código inútil
- [ContainmentView] Mostrar paneis cando a interface de usuario estea lista
- Non declarar a propiedade implicitHeight unha segunda vez
- usar QQuickViewSharedEngine::setTranslationDomain (fallo 361513)
- engadir as iconas de Breeze de Plasma de 22 px e 32 px
- retirar as iconas de sistema coloradas e engadir as monocroma de 32 px
- Engadir un botón opcional de mostrar o contrasinal en TextField
- Os consellos estándar invértense nos idiomas con escrita de dereita a esquerda
- Mellorouse en gran medida o rendemento ao cambiar de mes no calendario

### Sonnet

- Non cambiar os nomes de idioma a minúsculas na análise de trigramas
- Corrixir unha quebra inmediata no inicio por mor dun punteiro de complemento nulo
- Xestionar dicionarios sen nomes correctos
- Substituír a lista manual de asociacións de linguaxes de script, usar os nomes correctos das linguaxes
- Engadir unha ferramenta para xerar trigramas
- Aquelar un pouco a detección de idioma
- Usar os idiomas seleccionados como suxestión para a detección
- Usar correctores ortográficos da caché na detección de idioma, mellorar un pouco o rendemento
- Mellorar a detección de idioma
- Filtrar as listas de suxestións segundo os dicionarios dispoñíbeis, retirar os duplicados
- Lembrar engadir a coincidencia do último trigrama
- Comprobar se algún dos trigramas coincidiu
- Xestionar varios idiomas coa mesma puntuación no analizador de trigramas
- Non comprobar o tamaño mínimo dúas veces
- Limpar a lista de idiomas segundo os idiomas dispoñíbeis
- Usar a mesma lonxitude mínima en toda a detección de idioma
- Comprobar que o modelo cargado ten a cantidade correcta de trigramas para cada idioma

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
