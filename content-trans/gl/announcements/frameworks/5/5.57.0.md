---
aliases:
- ../../kde-frameworks-5.57.0
date: 2019-04-13
layout: framework
libCount: 70
---
### Attica

- Aceptar calquera estado de HTTP entre 100 e 199 como benigno

### Baloo

- [DocumentIdDB] Silenciar as mensaxes de depuración que non son erros, avisar sobre erros
- [baloosearch] Permitir indicar unha hora ao usar, por exemplo, mtime
- [indexcleaner] Evitar retirar os cartafoles incluídos dentro dos excluídos
- [MTimeDB] Corrixir a busca do intervalo LessEqual
- [MTimeDB] Corrixir a busca cando o intervalo de tempo debería devolver un conxunto baleiro
- Corrixir asercións e xestións de erros en MTimeDB
- Protexer de pais incorrectos no IdTreeDB
- Retirar os documentos de MTimeDB ou DocumentTimeDB aínda que a súa marca de tempo sexa 0
- Ser máis preciso coa detección de tipos MIME (fallo 403902)
- [cronoloxía] Converter os URL en canónicos
- [cronoloxía] Corrixir as chamadas a SlaveBase::finished() que faltaban ou estaban mal colocadas
- [balooshow] Saída de información de ficheiro básica en varias extensións
- [cronoloxía] Corrixir un aviso, engadir unha entrada de UDS que faltaba para «.»
- [balooctl] Reducir o nivel de aniñado dos argumentos de addOption, limpeza
- Reaccionar a actualizacións da configuración dentro do indexador (fallo 373430)
- Corrixir unha regresión ao abrir a base de datos en modo de lectura e escritura (fallo 405317)
- [balooctl] Limpar os espazos ao final
- [motor] Corrixir código, reverter o cambio de nome de Transaction::abort()
- Harmonizar a xestión de barra baixa no analizador de consultas
- Motor de Baloo: tratar todos os códigos que non sexan de éxito como fallo (fallo 403720)

### BluezQt

- Mover a interface Media dentro de Adapter
- Manager: non requirir a interface Media1 para a inicialización (fallo 405478)
- Device: comprobar a ruta do obxecto na rañura «removed» das interfaces (fallo 403289)

### Iconas de Breeze

- Engadir as iconas «notifications» e «notifications-disabled» (fallo 406121)
- Facer «start-here-kde» tamén dispoñíbel como «start-here-kde-plasma»
- Icona de Sublime Merge
- Dar máis contraste a «applications-games» e «input-gaming» con Breeze escuro
- Facer que o «go-up» de 24px use 24px de verdade
- Engadir as iconas «preferences-desktop-theme-applications» e «preferences-desktop-theme-windowdecorations»
- Engadir ligazóns simbólicas de «preferences-desktop-theme» a «preferences-desktop-theme-applications»
- Retirar «preferences-desktop-theme» en preparación para convertelo nunha ligazón simbólica
- Engadir collapse/expand-all e window-shade/unshade (fallo 404344)
- Mellorar a consistencia de window-* e engadir máis
- Facer que go-bottom/first/last/top se parezan máis a media-skip*
- Cambiar o destino das ligazóns simbólicas go-up/down-search a go-up/down
- Mellorar o aliñamento á grade de píxeles de go-up/down/next/previous/jump
- Cambiar o estilo de media-skip* e media-seek*
- Aplicar o novo estilo da icona de silenciado a todas as iconas de acción

### Módulos adicionais de CMake

- Activar de novo a opción de QT_PLUGIN_PATH
- ecm_add_wayland_client_protocol: Mellorar as mensaxes de erro
- ECMGeneratePkgConfigFile: make all vars dependent on ${prefix}
- Engadir un módulo para atopar UDev
- ECMGeneratePkgConfigFile: engadir variábeis que pkg_check_modules usa
- Restaurar a compatibilidade cara atrás de FindFontconfig para plasma-desktop
- Engadir un módulo para atopar Fontconfig

### Integración de infraestruturas

- Usar unha icona específica de Plasma máis axeitada para a categoría Plasma
- Usar a icona de Plasma como icona para a categoría de notificacións de Plasma

### Ferramentas de Doxygen de KDE

- Actualizar os URL para usar https

### KArchive

- Corrixir unha quebra en KArchive::findOrCreate con ficheiros rotos
- Corrixir unha lectura de memoria sen inicializar en KZip
- Engadir Q_OBJECT a KFilterDev

### KCMUtils

- [KCModuleLoader] Pasar os argumentos ao KQuickAddons::ConfigModule creado
- Pasar o foco á barra de busca filla ao enfocar KPluginSelector (fallo 399516)
- Mellorar a mensaxe de erro de KCM
- Engadir unha protección en tempo de execución que asegure que as páxinas son KCM en KCMultiDialog (fallo 405440)

### KCompletion

- Non definir un completador nulo nunha caixa de selección non editábel

### KConfig

- Engadir a funcionalidade de Notify a revertToDefault
- Apuntar o README á páxina do wiki
- kconfig_compiler: novos argumentos HeaderExtension e SourceExtension para kcfgc
- [kconf_update] Pasar de tecnoloxía de rexistro personalizada a qCDebug
- Retirar a referencia de const KConfigIniBackend::BufferFragment &amp;
- KCONFIG_ADD_KCFG_FILES macro: asegurarse de que os cambios de File= en kcfg se recollen

### KCoreAddons

- Corrixir «* foo *», non queremos esta cadea en letra grosa
- Corrixir o fallo 401996 — Ao premer o URL web dun contacto non se selecciona todo (fallo 401996)
- Imprimir strerror cando inotify falla (motivo habitual: «demasiados ficheiros abertos»)

### KDBusAddons

- Converter dous connect do estilo antigo ao novo

### KDeclarative

- [GridViewKCM] Corrixir un cálculo de anchura implícito
- Mover a vista de grade a un ficheiro de seu
- Evitar fraccións nos tamaños e aliñamentos de GridDelegate

### Compatibilidade coa versión 4 de KDELibs

- Retirar os módulos de atopar que xa fornece ECM

### KDocTools

- Actualizar a tradución ao ucraíno
- Actualizacións de catalán
- entidades de it: actualizar os URL para usar https
- Actualizar os URL para usar https
- Usar a tradución indonesia
- Actualizar o deseño para asemellarse a kde.org
- Engadir os ficheiros necesarios para usar o idioma indonesio nativo para toda a documentación en indonesio

### KFileMetaData

- Permitir escribir información de puntuación co escritor de taglib
- Engadir máis etiquetas ao escritor de taglib
- Reescribir o escritor de taglib para usar a interface de propiedades
- Probar o extractor de ffmpeg usando o asistente de tipos MIME (fallo 399650)
- Propoñer a Stefan Bruns como mantedor de KFileMetaData
- Declarar PropertyInfo como QMetaType
- Protexer de ficheiros incorrectos
- [TagLibExtractor] Usar o tipo MIME correcto en caso de herdanza
- Engadir un asistente para determinar o tipo MIME permitido real do pai
- [taglibextractor] Probar a extracción de propiedades con varios valores
- Xerar unha cabeceira para o novo MimeUtils
- Usar a función de Qt para formatar listas de cadeas
- Corrixir o número de propiedades de localización
- Verificar os tipos MIME de todos os ficheiros e mostra existentes, e engadir algúns máis
- Engadir unha función asistente para determinar o tipo MIME segundo o contido e a extensión (fallo 403902)
- Permitir extraer datos de ficheiros ogg e ts (fallo 399650)
- [ffmpegextractor] Engadir un caso de proba de vídeo de Matroska (fallo 403902)
- Reescribir o extractor de taglib para usar a interface xenérica PropertyMap (fallo 403902)
- [ExtractorCollection] Cargar os complementos de extractores cando sexan necesarios
- Corrixir a extracción da propiedade de proporcións
- Aumentar a precisión da propiedade de taxa de fotogramas

### KHolidays

- Ordenar as categorías de vacacións polacas

### KI18n

- Informar cun erro lexíbel por humanos se se necesita Qt5Widgets pero non se atopa

### KIconThemes

- Corrixir o recheo de iconas que non se corresponden co tamaño solicitado (fallo 396990)

### KImageFormats

- ora:kra: qstrcmp → memcmp
- Corrixir RGBHandler::canRead
- xcf: non quebrar con ficheiros con modos de capa incompatíbeis

### KIO

- Substituír currentDateTimeUtc().toTime_t() por currentSecsSinceEpoch()
- Substituír QDateTime::to_Time_t/from_Time_t por to/fromSecsSinceEpoch
- Mellorar as iconas dos botóns dos diálogos de executábeis (fallo 406090)
- [KDirOperator] Mostrar a vista de árbore detallada de maneira predeterminada
- KFileItem: chamar a stat() cando se necesite, engadir a opción SkipMimeTypeDetermination
- KIOExec: corrixir o erro cando o URL remoto non ten nome de ficheiro
- [KFileWidget] No modo de gardar un único ficheiro, premer Intro en KDirOperator dispara slotOk (fallo 385189)
- [KDynamicJobTracker] Usar a interface de DBus xerada
- [KFileWidget] Ao gardar, realzar o nome de ficheiro tras premer o ficheiro existente tamén ao usar clic duplo
- Non crear miniaturas para caixas fortes cifradas (fallo 404750)
- Corrixir o cambio de nome de directorio de WebDAV se KeepAlive está desactivado
- Mostrar a lista de etiquetas en PlacesView (fallo 182367)
- Diálogo de confirmación de eliminar ou botar no lixo: corrixir o título confuso
- Mostrar o ficheiro ou ruta correctos nas mensaxes de erro «grande de máis para fat32» (fallo 405360)
- Usar GiB en vez de GB na mensaxe de erro (fallo 405445)
- openwithdialog: usar a marca de recursividade no filtro de proxys
- Retirar que se obteñan os URL ao completarse o traballo de listado (fallo 383534)
- [CopyJob] Tratar os URL como sucios ao cambiar de nome un ficheiro como resolución de conflito
- Pasar a ruta local do ficheiro a KFileSystemType::fileSystemType()
- Corrixir o cambio de nome a maiúsculas ou minúsculas en sistemas de ficheiros que non distinguen maiúsculas de minúsculas
- Corrixir os avisos «URL incorrecto: QUrl("algo.txt")" no diálogo de gardar (fallo 373119)
- Corrixir a quebra ao mover ficheiros
- Corrixir a comprobación oculta de NTFS para ligazóns simbólicas a puntos de montaxe de NTFS (fallo 402738)
- Facer un pouco máis seguro sobrescribir ficheiros (fallo 125102)

### Kirigami

- corrixir o implicitWidth de listItems
- Entropía de Shannon para adiviñar iconas monocromáticas
- Evitar que desapareza o caixón de contexto
- Retirar actionmenuitembase
- Non intentar obter a versión de construcións estáticas
- [Mnemonic Handling] Substituír só a primeira aparición
- Sincronizar cando se actualiza calquera propiedade do modelo
- Usar icon.name en back/forward
- Corrixir a barra de ferramentas para capas
- Corrixir os erros dos ficheiros de exemplo de Kirigami
- Engadir os compoñentes SearchField e PasswordField
- Corrixir as iconas de asa (bug 404714)
- [InlineMessage] Non debuxar sombras ao redor da mensaxe
- Dispoñer inmediatamente ao cambiar de orde
- Corrixir a disposición do ronsel
- Non mostrar nunca a barra de ferramentas cando así o solicite o elemento actual
- Xestionar atrás e adiante tamén no filtro
- Permitir os botóns do rato de atrás e adiante
- Engadir a creación das instancias dos submenús cando se necesite
- Corrixir a barra de ferramentas para capas
- kirigami_package_breeze_icons: buscar tamén entre as iconas de tamaño 16
- Corrixir a construción baseada en Qmake
- Obter a propiedade anexada do elemento correcto
- Corrixir a lóxica ao mostrar a barra de ferramentas
- Permitir desactivar a barra de ferramentas para as páxinas das capas
- Mostrar sempre a barra de ferramentas global nos modos globais
- emitir Page.contextualActionsAboutToShow
- Un pouco de espazo á dereita do título
- Dispoñer de novo tras os cambios de visibilidade
- ActionTextField: colocar as accións de maneira axeitada
- topPadding e BottomPadding
- O texto sobre imaxes ten que ser sempre branco (fallo 394960)
- Cortar a folla de capas (fallo 402280)
- Evitar usar OverlaySheet como pai de ColumnView
- Usar un qpointer para a instancia do tema (fallo 404505)
- agochar o ronsel en páxinas que non queren unha barra de ferramentas (fallo 404481)
- Non intentar sobrepor a propiedade enabled (fallo 404114)
- Posibilidade dunha cabeceira e pé de páxina personalizados en ContextDrawer (fallo 404978)

### KJobWidgets

- [KUiServerJobTracker] Actualizar destUrl antes de rematar o traballo

### KNewStuff

- Cambiar os URL a https
- Actualizar a ligazón ao proxecto fsearch
- Xestionar as ordes incompatíbeis de OCS, e non votar de máis (fallo 391111)
- Novo lugar para ficheiros KNSRC
- [knewstuff] Retirar un método marcado como obsoleto en Qt 5.13

### KNotification

- [KStatusNotifierItem] Enviar un consello de desktop-entry
- Permitir definir consellos personalizados para as notificacións

### KNotifyConfig

- Permitir seleccionar só ficheiros de son compatíbeis (fallo 405470)

### Infraestrutura KPackage

- Corrixir que se atope o ficheiro de destinos das ferramentas da máquina no entorno de Docker de Android
- Engadir a posibilidade de compilación cruzada de kpackagetool5

### KService

- Engadir X-GNOME-UsesNotifications como clave recoñecida
- Engadir 2.4.1 como versión mínima de bison por mor de %code

### KTextEditor

- Corrección: aplicar correctamente as cores de texto do esquema seleccionado (fallo 398758)
- DocumentPrivate: Engadir a opción «Cargar de novo automaticamente o documento» ao menú de Vista (fallo 384384)
- DocumentPrivate: permitir definir o dicionario ao seleccionar un bloque
- Corrixir a cadea de palabras e caracteres en katestatusbar
- Corrixir o minimapa co estilo QtCurve
- KateStatusBar: mostrar a icona de cadeado nas etiquetas modificadas no modo de só lectura
- DocumentPrivate: saltar as comiñas automáticas cando xa parezan emparelladas (fallo 382960)
- Engadir a interface Variable a KTextEditor::Editor
- Relaxar o código para aseverar só nas construcións de depuración e funcionar nas construcións de publicación
- Asegurar a compatibilidade con configuracións vellas
- Uso maior da interface de configuración xenérica
- Simplificar QString KateDocumentConfig::eolString()
- Transferir a configuración de sonnet á configuración de KTextEditor
- Asegurar que non hai espazos baleiros nas claves de configuración
- Converter máis cousas á interface de configuración xenérica
- Uso maior da interface de configuración xenérica
- Interface de configuración xenérica
- Non quebrar ante ficheiros de realce de sintaxe incorrectos
- IconBorder: aceptar eventos de arrastrar e soltar (fallo 405280)
- ViewPrivate: facer máis útil a anulación de selección mediante teclas de frecha (fallo 296500)
- Corrección da mostra da árbore de consellos de argumentos nunha pantalla distinta da principal
- Migrar un método obsoleto
- Restaurar o tipo e posición das mensaxes axustadas pola busca (fallo 398731)
- ViewPrivate: facer «Aplicar axuste de liñas» máis cómodo (fallo 381985)
- ModeBase::goToPos: Asegurar que o destino do salto sexa correcto (fallo 377200)
- ViInputMode: Retirar os atributos de texto incompatíbeis da barra de estado
- KateStatusBar: Engadir un botón de dicionario
- Engadir un exemplo para o problema de altura das liñas

### KWidgetsAddons

- Facer KFontRequester consistente
- Actualizar kcharselect-data a Unicode 12.0

### KWindowSystem

- Enviar o contraste de esvaer ou fondo en píxeles de dispositivo (fallo 404923)

### NetworkManagerQt

- WireGuard: facer que funcione serializar ou desserializar segredos dun mapa
- Engadir a compatibilidade que faltaba con WireGuard na clase de configuración base
- Wireguard: xestionar as chaves privadas como segredos
- Wireguard: a propiedade «peers» debería ser NMVariantMapList
- Engadir compatibilidade co tipo de conexión «Wireguard»
- ActiveConnecton: engadir o sinal stateChangedReason no que pode consultarse o motivo do cambio de estado

### Infraestrutura de Plasma

- [AppletInterface] Comprobar se hai coroa antes de acceder a ela
- [Dialog] Non reenviar o evento de cubrir se non hai onde reenvialo
- [Menu] Corrixir o disparo do sinal
- Reducir a importancia dalgunha información de depuración para que poidan verse os avisos de verdade
- [PlasmaComponents3 ComboBox] Corrixir textColor
- FrameSvgItem: capturar os cambios de marxe de FrameSvg tamén fóra dos seus propios métodos
- Engadir Theme::blurBehindEnabled()
- FrameSvgItem: corrixir textureRect para que os elementos subordinados teselados non se reduzan a 0
- Corrixir o fondo dos diálogos de Breeze con Qt 5.12.2 (fallo 405548)
- Retirar unha quebra de plasmashell
- [Icon Item] Baleirar tamén a icona de imaxe ao usar Plasma Svg (fallo 405298)
- Altura de textfield baseada unicamente en texto simple (fallo 399155)
- asociar alternateBackgroundColor

### Purpose

- Engadir o complemento de SMS de KDE Connect

### QQC2StyleBridge

- O estilo do escritorio Plasma permite colorar iconas
- [SpinBox] Mellorar o comportamento da roda do rato
- Engadir un pouco de recheo en ToolBars
- Corrixir as iconas de RoundButton
- Recheo baseado na barra de desprazamento en todos os delegados
- Buscar unha vista de desprazamento para coller a súa barra de desprazamento para as marxes

### Solid

- Permitir construír sen UDev en Linux
- Só obter clearTextPath cando se use

### Realce da sintaxe

- Engadir unha definición de sintaxe da linguaxe Elm a syntax-highlighting
- AppArmor e SELinux: retirar un sangrado en ficheiros XML
- Doxygen: non usar a cor negra nas etiquetas
- Permitir cambios de contexto de fin de liña en liñas baleiras (fallo 405903)
- Corrixir o pregado de endRegion na regras con beginRegion+endRegion (usar length=0) (fallo 405585)
- Engadir extensións ao realce de Groovy (fallo 403072)
- Engadir un ficheiro de realce de sintaxe para Smali
- Engadir «.» como weakDeliminator no ficheiro de sintaxe de Octave
- Logcat: corrixir a cor de dsError con underline="0"
- Corrixir a quebra do realzador por ficheiros de realce rotos
- Protexer as bibliotecas de ligazón dos destinos para versións anteriores de CMake (fallo 404835)

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
