---
aliases:
- ../announce-applications-15.12.2
changelog: true
date: 2016-02-16
description: KDE publica a versión 15.12.2 das aplicacións de KDE
layout: application
title: KDE publica a versión 15.12.2 das aplicacións de KDE
version: 15.12.2
---
February 16, 2016. Today KDE released the second stability update for <a href='../15.12.0'>KDE Applications 15.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis de 30 correccións de erros inclúen melloras en, entre outros, KDE Libs, KDE PIM, Kdenlive, Marble, Konsole, Spectacle, Akonadi, Ark e Umbrello.

This release also includes Long Term Support version of KDE Development Platform 4.14.17.
