---
aliases:
- ../announce-applications-17.08.1
changelog: true
date: 2017-09-07
description: KDE publica a versión 17.08.1 das aplicacións de KDE
layout: application
title: KDE publica a versión 17.08.1 das aplicacións de KDE
version: 17.08.1
---
September 7, 2017. Today KDE released the first stability update for <a href='../17.08.0'>KDE Applications 17.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis de 20 correccións de erros inclúen melloras en, entre outros, Kontact, Gwenview, Kdenlive, Konsole, KWalletManager, Okular, Umbrello e KDE games.

This release also includes Long Term Support version of KDE Development Platform 4.14.36.
