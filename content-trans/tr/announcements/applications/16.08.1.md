---
aliases:
- ../announce-applications-16.08.1
changelog: true
date: 2016-09-08
description: KDE, KDE Uygulamaları 16.08.1'i Gönderdi
layout: application
title: KDE, KDE Uygulamaları 16.08.1'i Gönderdi
version: 16.08.1
---
September 8, 2016. Today KDE released the first stability update for <a href='../16.08.0'>KDE Applications 16.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 45 recorded bugfixes include improvements to kdepim, kate, kdenlive, konsole, marble, kajongg, kopete, umbrello, among others.

This release also includes Long Term Support version of KDE Development Platform 4.14.24.
