---
aliases:
- ../../kde-frameworks-5.8.0
date: '2015-03-13'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
New frameworks:

- KPeople, provides access to all contacts and the people who hold them
- KXmlRpcClient, interaction with XMLRPC services

### Genel

- A number of build fixes for compiling with the upcoming Qt 5.5

### KActivities

- Resources scoring service is now finalized

### KArchive

- Stop failing on ZIP files with redundant data descriptors

### KCMUtils

- Restore KCModule::setAuthAction

### KCoreAddons

- KPluginMetadata: add support for Hidden key

### KDeclarative

- Prefer exposing lists to QML with QJsonArray
- Handle non default devicePixelRatios in images
- Expose hasUrls in DeclarativeMimeData
- Allow users to configure how many horizontal lines are drawn

### KDocTools

- Fix the build on MacOSX when using Homebrew
- Better styling of media objects (images, ...) in documentation
- Encode invalid chars in paths used in XML DTDs, avoiding errors

### KGlobalAccel

- Activation timestamp set as dynamic property on triggered QAction.

### KIconThemes

- Fix QIcon::fromTheme(xxx, someFallback) would not return the fallback

### KImageFormats

- Make PSD image reader endianess-agnostic.

### KIO

- Deprecate UDSEntry::listFields and add the UDSEntry::fields method which returns a QVector without costly conversion.
- Sync bookmarkmanager only if change was by this process (bug 343735)
- Fix startup of kssld5 dbus service
- Implement quota-used-bytes and quota-available-bytes from RFC 4331 to enable free space information in http ioslave.

### KNotifications

- Delay the audio init until actually needed
- Fix notification config not applying instantly
- Fix audio notifications stopping after first file played

### KNotifyConfig

- Add optional dependency on QtSpeech to reenable speaking notifications.

### KService

- KPluginInfo: support stringlists as properties

### KTextEditor

- Add word count statistics in statusbar
- vimode: fix crash when removing last line in Visual Line mode

### KWidgetsAddons

- Make KRatingWidget cope with devicePixelRatio

### KWindowSystem

- KSelectionWatcher and KSelectionOwner can be used without depending on QX11Info.
- KXMessages can be used without depending on QX11Info

### NetworkManagerQt

- Add new properties and methods from NetworkManager 1.0.0

#### Plasma framework

- Fix plasmapkg2 for translated systems
- Improve tooltip layout
- Make it possible to let plasmoids to load scripts outside the plasma package ...

### Buildsystem changes (extra-cmake-modules)

- Extend ecm_generate_headers macro to also support CamelCase.h headers

Bu sürüm hakkında fikir ve önerilerinizi <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot makalesinde</a> yorum olarak ekleyebilirsiniz.
