---
aliases:
- ../../kde-frameworks-5.7.0
date: '2015-02-14'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Genel

- A number of fixes for compiling with the upcoming Qt 5.5

### KActivities

- Fixed starting and stopping activities
- Fixed activity preview showing wrong wallpaper occasionally

### KArchive

- Create temporary files in the temp dir rather than in the current directory

### KAuth

- Fixed generation of KAuth DBus helper service files

### KCMUtils

- Fixed assert when dbus paths contain a '.'

### KCodecs

- Added support for CP949 to KCharsets

### KConfig

- kconf_update no longer processes *.upd file from KDE SC 4. Add "Version=5" to top of the upd file for updates that should be applied to Qt5/KF5 applications
- Kayıtlar arasında bir değer değiştirirken ki KCoreConfigSkeleton onarıldı

### KConfigWidgets

- KRecentFilesAction: menü girdi sıralaması onarıldı (artık kdelibs4 sırasıyla aynı)

### KCoreAddons

- KAboutData: Uyum ve kararlılık için addHelpOption ve addVersionOption metodlarını otomatik çağır.
- KAboutData: Bir e-posta/URL belirlenmemişse "Lütfen hataları bildirmek için http://bugs.kde.org kullanın." metnini geri getir
- KAutoSaveFile: allStaleFiles() artık yerel dosyalar için beklendiği gibi çalışıyor. staleFiles() de çözüldü.
- KRandomSequence artık int'leri dahili olarak kullanıyor ve 64-bit kararsızlığı için int-api'sini ortaya çıkarıyor
- Mime türü tanımları: *.qmltypes ve *.qmlproject dosyaları ayrıca text/x-qml mime türü metnini de içeriyor
- KShell: quoteArgs alıntı URL'lerini QChar::isSpace() ile yap. Alışılmadık boşluk karakterleri düzgün ele alınmıyordu
- KSharedDataCache: önbellek içeren dizin oluşturulmasını düzenle (geçiş hatası)

### KDBusAddons

- Daha fazla kded benzeri yardımcılar (kiod benzeri) yazabilmek için KDEDModule::moduleForMessage yardımcı metodu eklendi

### KDeclarative

- Çizici bileşen eklendi
- Formats::formatDuration için int alan overload metodu eklendi
- QPixmapItem ve QImageItem içerisine yeni paintedWidth ve paintedHeight özellikleri eklendi
- QImageItem ve QPixmapItem boyaması onarıldı

### Kded

- JSON üst verisi ile kded modülleri yükleme desteği eklendi

### KGlobalAccel

- Bunu artık bir tier3 çalışma çerçevesi yapan çalışma zamanı bileşenini dahil ediyor
- Windows arka ucunun tekrar çalışmasını sağla
- Tekrar etkinleştirilen Mac arka ucu
- KGlobalAccel X11 çalışma zamanı kapatmasındaki hata onarıldı

### KI18n

- API hatalı kullanıldığında uyarı için sonuçları istendiği gibi işaretle
- Gömülü sistemlerde azaltılmış özellik küöesine izin veren BUILD_WITH_QTSCRIPT derleme sistemi seçeneği eklendi

### KInit

- OSX: geçerli paylaşımlı kitaplıklarını çalışma zamanında yükle
- Mingw derleme onarımları

### KIO

- KIOWidgets'e bağlantı yapılırken görevlerde çökmeyi onar ancak yalnızca bir QCoreApplication kullanılırken
- Web kısayollarının düzenlenmesi onarıldı
- Yalnızca KIOCore ve yardımcı programlarını derleyip, gerekli bağımlılıkların sayısını oldukça azaltmak için KIOWidgets veya KIOFileWidgets'ın derlenmesini kapatan KIOCORE_ONLY seçeneği eklendi
- Açılır menülere Kopyala /Taşı özelliği ekleyen KFileCopyToMenu sınıfı eklendi
- SSL etkin protokoller: TLSv1.1 ve TLSv1.2 protokolleri için destek eklendi, SSLv3 kaldırıldı
- Asıl anlaşılmış protokolü döndürmek için negotiatedSslVersion ve negotiatedSslVersionName onarıldı
- URL gezginini kırıntı kipine dönüştüren düğmeye tıklanıldığında girilen adresi görünüme uygula
- Kopyala/taşı görevlerinin göründüğü iki çubuk/pencere ilerlemesini onar
- KIO, daha önce kded içinde çalışan süreç hizmetleri için bağımlılıkları azaltmak üzere artık kendi yardımcısı kiod'u kullanıyor; şu anda yalnızca kssld yerine
- Kioexec tetiklendiğindeki "&lt;yol&gt; a yazılamadı" hatası onarıldı
- KFilePlacesModel kullanırken "QFileInfo::absolutePath: Constructed with empty filename" uyarıları onarıldı

### KItemModels

- Fixed KRecursiveFilterProxyModel for Qt 5.5.0+, due to QSortFilterProxyModel now using the roles parameter to the dataChanged signal

### KNewStuff

- Always reload xml data from remote urls

### KNotifications

- Documentation: mentioned the file name requirements of .notifyrc files
- Fixed dangling pointer to KNotification
- Fixed leak of knotifyconfig
- Install missing knotifyconfig header

### KPackage

- Renamed kpackagetool man to kpackagetool5
- Fixed installation on case-insensitive filesystems

### Kross

- Fixed Kross::MetaFunction so it works with Qt5's metaobject system

### KService

- Include unknown properties when converting KPluginInfo from KService
- KPluginInfo: fixed properties not being copied from KService::Ptr
- OS X: performance fix for kbuildsycoca4 (skip app bundles)

### KTextEditor

- Fixed high-precision touchpad scrolling
- Do not emit documentUrlChanged during reload
- Do not break cursor position on document reload in lines with tabs
- Do not re(un)fold the first line if it was manually (un)folded
- vimode: command history through arrow keys
- Do not try to create a digest when we get a KDirWatch::deleted() signal
- Performance: remove global initializations

### KUnitConversion

- Fixed infinite recursion in Unit::setUnitMultiplier

### K Cüzdan

- Automatically detect and convert old ECB wallets to CBC
- Fixed the CBC encryption algorithm
- Ensured wallet list gets updated when a wallet file gets removed from disk
- Remove stray &lt;/p&gt; in user-visible text

### KWidgetsAddons

- Use kstyleextensions to specify custom control element for rendering kcapacity bar when supported, this allow the widget to be styled properly
- Provide an accessible name for KLed

### KWindowSystem

- Fixed NETRootInfo::setShowingDesktop(bool) not working on Openbox
- Added convenience method KWindowSystem::setShowingDesktop(bool)
- Fixes in icon format handling
- Added method NETWinInfo::icccmIconPixmap provides icon pixmap from WM_HINTS property
- Added overload to KWindowSystem::icon which reduces roundtrips to X-Server
- Added support for _NET_WM_OPAQUE_REGION

### NetworkmanagerQt

- Do not print a message about unhandled "AccessPoints" property
- Added support for NetworkManager 1.0.0 (not required)
- Fixed VpnSetting secrets handling
- Added class GenericSetting for connections not managed by NetworkManager
- Added property AutoconnectPriority to ConnectionSettings

#### Plasma framework

- Fixed errorneously opening a broken context menu when middle clicking Plasma popup
- Trigger button switch on mouse wheel
- Never resize a dialog bigger than the screen
- Undelete panels when applet gets undeleted
- Fixed keyboard shortcuts
- Restore hint-apply-color-scheme support
- Reload the configuration when plasmarc changes
- ...

### Solid

- Added energyFull and energyFullDesign to Battery

### Buildsystem changes (extra-cmake-modules)

- New ECMUninstallTarget module to create an uninstall target
- Make KDECMakeSettings import ECMUninstallTarget by default
- KDEInstallDirs: warn about mixing relative and absolute installation paths on the command line
- Added ECMAddAppIcon module to add icons to executable targets on Windows and Mac OS X
- Fixed CMP0053 warning with CMake 3.1
- Do not unset cache variables in KDEInstallDirs

### Frameworkintegration

- Fix updating of single click setting at runtime
- Multiple fixes to the systemtray integration
- Only install color scheme on toplevel widgets (to fix QQuickWidgets)
- Update XCursor settings on X11 platform

Bu sürüm hakkında fikir ve önerilerinizi <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot makalesinde</a> yorum olarak ekleyebilirsiniz.
