---
title: Blue Friday is here
layout: bluefriday
scssFiles:
- /scss/bluefriday.scss
---

Just kidding! **All KDE stuff will always be free**.

Instead of spending all your hard-earned money on dodgy Black Friday bargains, why not donate to KDE? We bring you the best every day of the year!

**You can donate to KDE now!**

With your help, we can continue to provide you (and the world) with top Free and Open Source [apps](https://apps.kde.org/); environments for your [desktop](https://kde.org/plasma-desktop/), [mobile phone](https://plasma-mobile.org/) and [smart TV](https://plasma-bigscreen.org/); [frameworks](https://develop.kde.org/products/frameworks/); and everything in between.

