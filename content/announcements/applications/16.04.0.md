---
aliases:
- ../announce-applications-16.04.0
changelog: true
date: 2016-04-20
description: KDE Ships KDE Applications 16.04.0
layout: application
title: KDE Ships KDE Applications 16.04.0
version: 16.04.0
---

April 20, 2016. KDE introduces today KDE Applications 16.04 with an impressive array of upgrades when it comes to better ease of access, the introduction of highly useful functionalities and getting rid of those minor issues which lead to KDE Applications now getting a step closer to offering you the perfect setup for your device.

<a href='https://www.kde.org/applications/graphics/kcolorchooser/'>KColorChooser</a>, <a href='https://www.kde.org/applications/utilities/kfloppy/'>KFloppy</a>, <a href='https://www.kde.org/applications/games/kmahjongg/'>KMahjongg</a> and <a href='https://www.kde.org/applications/internet/krdc/'>KRDC</a> have now been ported to KDE Frameworks 5 and we look forward to your feedback and insight into the newest features introduced with this release. We would also highly encourage your support for <a href='https://www.kde.org/applications/education/minuet/'>Minuet</a> as we welcome it to our KDE Applications and your input on what more you'd like to see.

### KDE's Newest Addition

{{<figure src="/announcements/applications/16.04.0/minuet1604.png" class="text-center" width="600px">}}

A new application has been added to the KDE Education suite. <a href='https://minuet.kde.org'>Minuet</a> is a Music Education Software featuring full MIDI support with tempo, pitch and volume control, which makes it suitable for both novice and experienced musicians.

Minuet includes 44 ear-training exercises about scales, chords, intervals and rhythm, enables the visualization of musical content on the piano keyboard and allows for the seamless integration of your own exercises.

### More Help to you

{{<figure src="/announcements/applications/16.04.0/khelpcenter1604.png" class="text-center" width="600px">}}

KHelpCenter which was previously distributed under Plasma is now distributed as a part of KDE Applications.

A massive bug triaging and cleanup campaign taken up by the KHelpCenter team resulted in 49 resolved bugs, many of which were related to improving and restoring the previously non-functional search functionality.

The internal document search which relied on the deprecated software ht::/dig has been replaced with a new Xapian-based indexing and search system which leads to the restoring of functionalities such as searching within man pages, info pages and KDE Software provided Documentation, with the added provision of bookmarks for the documentation pages.

With the removal of KDELibs4 Support and a further cleanup of the code and some other minor bugs, the maintenance of the code has also been thoroughly furnished to give you KHelpCenter in a newly shiny form.

### Aggressive Pest Control

The Kontact Suite had a whopping 55 bugs resolved; some of which were related to issues with setting alarms, and in the import of Thunderbird mails, downsizing Skype &amp; Google talk icons in the Contacts Panel View, KMail related workarounds such as folder imports, vCard imports, opening ODF mail attachments, URL inserts from Chromium, the tool menu differences with the app started as a part of Kontact as opposed to a standalone use, missing 'Send' menu item and a few others. The support for Brazilian Portuguese RSS feeds has been added along with the fixing of the addresses for the Hungarian and Spanish feeds.

The new features include a redesign of the KAddressbook contact editor, a new default KMail Header theme, improvements to the Settings Exporter and a fix of Favicon support in Akregator. The KMail composer interface has been cleaned up along with the introduction of a new Default KMail Header Theme with the Grantlee theme used for the 'About' page in KMail as well as Kontact and Akregator. Akregator now uses QtWebKit - one of the major engines to render webpages and execute javascript code as the renderer web engine and the process of implementing support for QtWebEngine in Akregator and other Kontact Suite applications has already begun. While several features were shifted as plugins in kdepim-addons, the pim libraries were split into numerous packages.

### Archiving in Depth

{{<figure src="/announcements/applications/16.04.0/ark1604.png" class="text-center" width="600px">}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, the archive manager, has had two major bugs fixed so that now Ark warns the user if an extraction fails due to lack of sufficient space in the destination folder and it does not fill up the RAM during the extraction of huge files via drag and drop.

Ark now also includes a properties dialog which displays information like the type of archive, compressed and uncompressed size, MD5/SHA-1/SHA-256 cryptographic hashes about the currently opened archive.

Ark can also now open and extract RAR archives without utilizing the non-free rar utilities and can open, extract and create TAR archives compressed with the lzop/lzip/lrzip formats.

The User Interface of Ark has been polished by reorganizing the menubar and the toolbar so as to improve the usability and to remove ambiguities, as well as to save vertical space thanks to the status-bar now hidden by default.

### More Precision to Video Edits

{{<figure src="/announcements/applications/16.04.0/kdenlive1604.png" class="text-center" width="600px">}}

<a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a>, the non-linear video editor has numerous new features implemented. The titler now has a grid feature, gradients, the addition of text shadow and adjusting letter/line spacing.

Integrated display of audio levels allows easy monitoring of the audio in your project alongwith new video monitor overlays displaying playback framerate, safe zones and audio waveforms and a toolbar to seek to markers and a zoom monitor.

There has also been a new library feature which allows to copy/paste sequences between projects and a split view in timeline to compare and visualise the effects applied to the clip with the one without them.

The render dialog has been rewritten with an added option to get faster encoding hence, producing large files and the speed effect has been made to work with sound as well.

Curves in keyframes have been introduced for a few effects and now your top chosen effects can be accessed quickly via the favourite effects widget. While using the razor tool, now the vertical line in the timeline will show the accurate frame where the cut will be performed and the newly added clip generators will enable you to create color bar clips and counters. Besides this, the clip usage count has been re-introduced in the Project Bin and the audio thumbnails have also been rewritten to make them much faster.

The major bug fixes include the crash while using titles (which requires the latest MLT version), fixing corruptions occurring when the project frames per second rate is anything other than 25, the crashes on track deletion, the problematic overwrite mode in the timeline and the corruptions/lost effects while using a locale with a comma as the separator. Apart from these, the team has been consistently working to make major improvements in stability, workflow and small usability features.

### And more!

<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, the document viewer brings in new features in the form of customizable inline annotation width border, allowance of directly opening embedded files instead of only saving them and also the display of a table of content markers when the child links are collapsed.

<a href='https://www.kde.org/applications/utilities/kleopatra'>Kleopatra</a> introduced improved support for GnuPG 2.1 with a fixing of selftest errors and the cumbersome key refreshes caused by the new GnuPG (GNU Privacy Guard) directory layout. ECC Key generation has been added with the display of Curve details for ECC certificates now. Kleopatra is now released as a separate package and support for .pfx and .crt files has been included. To resolve the difference in the behaviour of imported secret keys and generated keys, Kleopatra now allows you to set the owner trust to ultimate for imported secret keys. 