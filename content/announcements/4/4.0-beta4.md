---
aliases:
- ../announce-4.0-beta4
date: '2007-10-30'
description: KDE Project Ships Fourth Beta Release for Leading Free Software Desktop.
title: KDE 4.0 Beta 4
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Project Ships Fourth Beta Release for Leading Free Software Desktop,
Codename "Kippie"
</h3>
<p align="justify">
With the fourth Beta, the KDE project would like to collect feedback to
ensure the quality of KDE 4.0.
</p>

<p>

The KDE Community is happy to release <a
href="/announcements/4/4.0-beta4">the
fourth Beta for KDE 4.0</a>. This Beta aimed at further polishing of
the KDE codebase and we would love to start receiving feedback from
testers.<br />

As KDE has largely has been in bugfix mode, this latest Beta aims to
encourage testers to have a look at it to help us find and solve
the remaining problems and bugs. Besides the stabilization of the
codebase, some minor features have been added, including but not
limited to much work on <a href="http://plasma.kde.org">Plasma</a>,
the KDE 4 desktop shell.<br />
Sebastian K&uuml;gler notes: "The improvements have been huge, and plasma
is much closer to what it needs to be before the release. I am
confident we will be able to finish it and present a very usable
plasma to our userbase with KDE 4.0. We will then be able to extend
on that and present truly innovative desktop interfaces throughout
the KDE 4 lifecycle.

</p>

<p>
The new Beta 4 incorporates many improvements from previous Beta and alpha
releases: <a href="/announcements/4/4.0-beta3">Beta
3</a>, <a href="/announcements/4/4.0-beta2">Beta
2</a>, <a href="/announcements/4/4.0-beta1">Beta
1</a>, <a href="/announcements/4/4.0-alpha2">Alpha
2</a> and <a
href="/announcements/4/4.0-alpha1-visual-guide">Alpha 1</a>.
</p>
<h2>Beta 4</h2>
<p>
With the fourth Beta, the KDE developers hope to collect comments and bug reports
from the wider KDE community. With their help, we hope to solve the most
pressing problems with the current KDE 4 codebase to ensure the final 4.0
release is usable. We would like to encourage anyone who is willing and able to
spend some time on testing to find and <a href="http://bugs.kde.org">report</a>
problems to the KDE developers. 
</p>

<div class="text-center">
<a href="/announcements/4/4.0-beta4/kde4.0-beta4.png">
<img src="/announcements/4/4.0-beta4/kde4.0-beta4_small.png" class="img-fluid">
</a> <br/>
<em>The current KDE 4 Desktop with Dolphin and Systemsettings</em>
</div>
<br/>

<p>
You can find <a
href="http://techbase.kde.org/Contribute/Bugsquad/How_to_create_useful_crash_reports">some documentation</a> on the pages of our
<a href="http://techbase.kde.org/Contribute/Bugsquad">Bugsquad</a> and <a
href="http://www.chiark.greenend.org.uk/~sgtatham/bugs.html">in other
places</a>. There will be weekly <a
href="http://aseigo.blogspot.com/2007/10/kde4-krush-days-saturday.html">KDE 4
Krush Days on Saturdays</a> and everyone is invited to help out. Having a recent
SVN checkout (follow instructions on <a
href="http://techbase.kde.org">TechBase</a>) is great, but using the regularly
updated <a href="http://home.kde.org/~binner/kde-four-live/">openSUSE KDE 4 Live CD</a> 
or the <a href="http://pkg-kde.alioth.debian.org/kde4livecd.html">Debian KDE4 Beta4 Live CD</a>, 
while very easy, is also a big help to us.
</p>

<h4>Release blockers</h4>
<p>
Several <a
href="http://techbase.kde.org/Schedules/KDE4/4.0_Release_Beta_Goals">Beta
Goals</a> have been formulated, and as long as these are not ready, we will
continue to release Betas.
</p>

<h4>For developers</h4>
<p>
At the same time, the KDE community 
<a href="/announcements/4/4.0-platform-rc1">unleashes 
a release candidate</a> for the 
<em>KDE Development Platform</em>, the collection of libraries that are needed
to develop and run KDE 4 applications. This development platform aims at making
it possible for third parties to start porting their applications without the
need to compile everything from KDE's source code repository. The final 
KDE 4 Development Platform  will follow shortly, as soon as it is made sure that
the current snapshot does not contains any grave bugs.
</p>
<h3>Next Up</h3>
<p>
While the last improvements will be made in the coming weeks, as soon as the <a
href="http://techbase.kde.org/Schedules/KDE4/4.0_Release_Beta_Goals">Betacycle
objectives</a> are met, we will start preparing our first Release Candidate.
This marks the beginning of a short period where we will allow only the most
important bugfixes in preparation for the release.
</p>
<h4>Get it, run it, test it...</h4>
<p>
For those interested in getting packages to test and contribute, several
distributions notified us that they will have KDE 4.0 Beta 4 packages available
at or soon after the release. The complete and current list can be found on the
<a href="/info/1-2-3/3.95">KDE 4 Beta 4 Info Page</a>, where you
can also find links to the source code, information about compiling, security
and other issues.
</p>

<br>
<i>Compiled by Jos Poortvliet and Sebastian K&uuml;gler with extensive help 
from the KDE community</i>

<h2>About KDE 4</h2>
<p>
KDE 4 is the next generation of the popular KDE Desktop Environment which seeks
to fulfill the need for a powerful yet easy to use desktop for UNIX and LINUX
workstations. The aim of the KDE project for the 4.0 release is to put the
foundations in place for future innovations on the Free Desktop. The many newly
introduced technologies incorporated in the KDE libraries will make it easier
for developers to add rich functionality to their applications, combining and
connecting different components in any way they want.
</p>


