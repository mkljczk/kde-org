---
aliases:
- ../4.4
- ../4.4.sl
date: '2010-02-09'
title: Najava izida KDE SC 4.4.0 Caikaku
---

<h3 align="center">
  KDE Software Compilation 4.4.0 prinaša vmesnik za male prenosnike, okna v zavihkih in ogrodje za overjanje
</h3>

<p align="justify">
  <strong>
    Izšel je KDE Software Compilation 4.4.0 (kodno ime <i>»Caikaku«</i>)
  </strong>
</p>

<p align="justify">
Skupnost <a href="/">KDE</a> danes najavlja takojšnjo razpoložljovost KDE Software Compilation 4.4, <i>»Caikaku«</i>, ki uporabnikom proste programske opreme prinaša inovativno zbirko programov. Dodane so bile večje nove tehnologije, med njimi zmožnosti za družabna omrežja in sodelovanje prek spleta, nov vmesnik namenjen malim prenosnikom (netbookom) ter ogrodje za overjanje KAuth. Odpravljenih je bilo več kot 7200 hroščev in implementiranih več kot 1400 novih zmožnosti. Skupnost KDE se zahvaljuje vsem, ki so pomagali in omogočili izid te različice.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/general-desktop.png">
	<img src="/announcements/4/4.4.0/thumbs/general-desktop_thumb.png" class="img-fluid" alt="Delovno okolje KDE Plasma Desktop">
	</a> <br/>
	<em>Delovno okolje KDE Plasma Desktop</em>
</div>
<br/>

<p align="justify">
  Za podrobnosti o izboljšavah si oglejte <a href="/announcements/4/4.4.0/guide">Slikovni vodič po KDE Software Compilation 4.4</a>. Sledi pregled.
</p>

<h3>
  Delovno okolje Plasma prinaša družabne in spletne zmožnosti
</h3>
<br />
<p align="justify">
  Delovna okolja Plasma ponujajo osnovne funkcije, ki jih uporabnik potrebuje za zaganjanje un upravljanje programov. Razvijalci KDE-jevih delovnih okolij Plasma so pripravili nov vmesnik za male prenosnike, izpopolnili grafično podobo in dodali izboljšave za razne postopke uporabe. Uvajanje družabnih in spletnih zmožnosti izhaja iz kulture sodelovanja znotraj skupnosti KDE, ki deluje po načelih prostega programja.
</p>

<!-- Plasma Screencast -->
<div class="text-center">
	<em>Izboljšana interakcija z okoljem Plasma Desktop </em><br/>
<a href="http://blip.tv/file/get/Sebasje-KDEPlasmaIn44540.ogg">prenos</a></div>
<br/>

<p align="justify">
<ul>
  <li>
    V različici 4.4 je prvič na voljo <strong>Plasma Netbook</strong>. Plasma Netbook je alternativni grafični vmesnik, ki je na voljo poleg Plasma Desktop, ki je posebaj načrtovan za ergonomično uporabo na malih prenosnikih (netbookih). Ogrodje Plasma je bilo že od samega začetka zgrajeno tudi z mislijo na naprave, ki niso namizni računalniki. Plasma Netbook in Plasma Desktop si delita veliko komponent, vendar je Plasma Netbook načrtovana posebaj za čimboljše izkoriščanje omejenega prostora in uporabo zaslonov na dotik. Vmesnik Plasma Netbook vsebuje celozaslonski zaganjalnik programov in iskalnik ter izgled, ki spominja na časopis in ponuja več gradnikov, ki prikazujejo vsebino s spleta, ter pripomočkov, ki so že znani iz Plasma Desktop.
  </li>
  <li>
    Iniciativa <strong>Social Desktop</strong> prinaša izboljšave za gradnik openDesktop in sedaj uporabnikom omogoča iskanje prijateljev ter pošiljanje sporočil kar iz gradnika na namizju. Novi gradnik Aktivnosti openDesktop v živo prikazuje, kaj se dogaja znotraj uporabnikovega družabnega omrežja, gradnik Zbirka znanja uporabnikom omogoča iskanje po vprašanjih in odgovorih, ki se zbirajo pri različnih spletnih storitvah, na primer pri openDesktop.org.
  </li>
  <li>
    Nova zmožnost upravljalnika oken KWin omogoča <strong>združevanje oken v zavihke</strong>, kar olajša upravljanje velikega števila programov in napravi delo z njimi bolj učinkovito. Med dodatne izboljšave dela z okni spada tlakovanje oken na vodoravni polovici namizja z vleko oken ob stranska robova zaslona ter povečevanje oken z vleko ob vrhnji rob zaslona. Razvijalci upravljalnika oken KWin so tudi sodelovali z razvijalci Plasme in dodatno izboljšali interakcijo z delovnim okoljem. Rezultat sodelovanja so bolj tekoče animcaije in splošne pohitritve. Umetniki lahko sedaj lažje ustvarjajo in delijo teme za okraske oken, saj je po novem na voljo pogon Aurorae, ki omogoča uporabo tem izdelanih v razširljivi vektorski grafiki SVG.
  </li>
</ul>
</p>

<!-- Window grouping Screencast -->
<div class="text-center">
	<em>Olajšano upravljanje oken</em><br/>
<a href="http://blip.tv/file/get/Sebasje-WindowManagingFeaturesInKDEPlasma44222.ogg">prenos</a></div>
<br/>

<h3>
  Inovacije v KDE-jevih programih
</h3>
<p align="justify">
Skupnost KDE ponuja veliko zmogljivih programov, ki so preprosti za uporabo. Ta izdaja prinaša več manjših izboljšav in nekaj inovativnih novih tehnologij.
</p>
<p align="justify">
<ul>
  <li>
    Vmesnik <strong>GetHotNewStuff</strong> je bil v tej izdaji KDE Software Compilation močno izboljšan. To ogrodje je bilo načrtovano z vizijo, da bi ogromni skupnosti prispevkarjev v KDE <strong>olajšal povezovanje</strong> z milijoni uporabniki njihovih prispevkov. Uporabniki lahko s pomočjo GetHotNewStuff neposredno iz samega programa prenesejo <a href="http://dimsuz.wordpress.com/2009/11/11/katomic-new-feature-level-sets-support/">nove stopnje za KAtomic</a> in druge igre, nove kataloge nebesnih objektov za KStars, ali pa skripte, ki dodajo nove funkcije. V tej izdaji so dodane nove <strong>družabne zmožnosti</strong>, kot sta na primer komentiranje in ocenjevanje prispevka. Uporabnik pa lahko postane tudi oboževalec prispevka, kar pomeni, da bo uporabnik v gradniku Aktivnost openDesktop za ta prispevek lahko videl posodobitve. Uporabniki lahko iz več programov svoje sadove ustvarjanja pošljejo kar neposredno, brez porebe po dolgočasnem procesu pakiranja in ročnega pošiljanja na spletno stran.
  </li>
  <li>
    V tej izdaji sta obrodila sadove še dva dolgoročna projekta skupnosti KDE. Nepomuk, mednarodni raziskovalni projekt na temo semantičnega namizja, ki ga financira Evropska Unija, je končno postal dovolj stabilen in hiter za uporabo. <strong><a href="http://ppenz.blogspot.com/2009/11/searching.html">Integracija namiznega iskanja v upravljalnik datotek Dolphin</a></strong> uporablja Nepomuk in je uporabniku v pomoč pri iskanju in organiziranju datotek. Novi časovni prikaz nedavno uporabljene datoteke prikaže razvrščene po času. Ekipa KDE PIM je prenovila svoj prvi program, tako da le ta sedaj uporablja sistem Akonadi za hranjenje in pridobivanje podatkov. KDE-jev adresar je bil napisan na novo in je sedaj razdeljen na 3 podokna, z možnostjo preklopa na zelo preprost izgled. Obsežnejši prenos preostalih programov na te nove tehnologije bo na voljo v prihodnjih izdajah KDE Software Compilation.
  </li>
  <li>
    Poleg integracije prej omenjenih tehnologij so razvijalske ekipe na več načinov izboljšale svoje programe. KGet je dobil podporo za preverjanje digitalnih podpisov in prenašanje datotek iz večih virov hkrati. Gwenview sedaj premore orodje za uvažanje fotografij, ki je preprosto za uporabo. V tej izdaji so na voljo tudi povsem novi ali prenovljeni programi. Palapeli je igra, v kateri na svojem računalniku sestavljate sliko, ki je razrezana na koščke. Uporabniki lahko ustvarijo in na spletu delijo tudi sestavljanke, ki jih ustvarijo sami. Cantor je preprost in intuitiven vmesnik za zmogljivo statistično in znanstveno programsko opremo (<a href="http://r-project.org/">R</a>, <a href="http://sagemath.org/">SAGE</a> in <a href="http://maxima.sourceforge.net/">Maxima</a>). Rocs je študentom v pomoč pri učenju teorije grafov. V zbirko KDE PIM je bil dodan program Blogilo za pisanje spletnih dnevnikov (blogov).
  </li>
</ul>
</p>
<div class="text-center">
	<a href="/announcements/4/4.4.0/dolphin-search.png">
	<img src="/announcements/4/4.4.0/thumbs/dolphin-search_thumb.png" class="img-fluid" alt="Namizno iskanje, integrirano v upravljalnik datotek Dolphin">
	</a> <br/>
	<em>Namizno iskanje, integrirano v upravljalnik datotek Dolphin</em>
</div>
<br/>

<h3>
  Platforma pospešuje razvoj
</h3>
<p align="justify">
Močno osredotočanje na izjemne tehnologije v skupnosti KDE je privedlo do ene izmed najbolj popolnih, konsistenčnih in učinkovitih razvojnih platform, ki so na voljo. Izdaja 4.4 v KDE-jeve programske knjižnice prinaša mnoge nove tehnologije za sodelovanje in družabna omrežja. Na voljo so tudi zmogljive in odprte alternative obstoječim tehnologijam. Namesto priklenjanja uporabnikov na naše produkte, se osredotočamo na omogočanje inovacij tako na spletu kot tudi na namizju.
<p align="justify">
<ul>
  <li>
    Osnovna infrastruktura za KDE-jevo programsko opremo je doživela pomembne nadgradnje. Kot prvo, <strong>Qt 4.6</strong> prinaša podporo za platformo Symbian, novo ogrodje za animacije, podporo za večdotične vmesnike in pohitritve. Tehnologija za <strong>družabno namizje</strong>, ki je bila na voljo že v prejšnji izdaji, je bila izboljšana z osrednjim upravljanjem identitet in prinaša knjižnico libattica za transparenten dostop do spletnih storitev. <strong>Nepomuk</strong> sedaj uporablja precej stabilnejšo hrbtenico in je perfektna izbira za vse, kar je povezano z metapodatki, iskanjem in indeksiranjem.
  </li>
  <li>
   !!! KAuth supports granting and revoking fine-grained authorization policies, caching of passwords and a number of dynamic UI elements providing visual hints for use in applications.

    S KAuth je prispelo novo ogrodje za overjanje. Razvijalci, ki morajo določena opravila zagnati z več pravicami, lahko uporabijo <strong>KAuth, ki omogoča varno overjanje</strong> in ponuja s tem povezane gradnike za grafični vmesnik. KAuth na operacijskem sistemu Linux kot hrbtenico uporablja PolicyKit, s čimer je zagotovljena <strong>integracija med različnimi namizji</strong>. Ogrodje se že uporablja v določenih modulih v Sistemskih nastavitvah in bo v prihodnjih mesecih integrirano še v KDE-jevo namizje Plasma Desktop in v KDE-jeve programe.

  </li>
  <li>
    <strong>Akonadi</strong> je storitev za hranjenje osebnih podatkov (koledarjev, opravil, stikov, e-pošte) in deluje tudi kot transparenten predpomnilnik za e-poštne strežnike in strežnike za skupinsko delo ter ostale spletne storitve. V izdaji 4.4 je prvič uporabljen v KDE-jevih programih. Adresar je prvi program, ki uporablja infrastrukturo Akonadi. KDE SC 4.4.0 tako označuje začetek obdobja uporabe Akonadija. V prihodnjih izdajah bodo sledili še drugi KDE-jevi programi.
  </li>
</ul>
</p>
<p>
Na voljo pod prosto licenco LGPL (kar omogoča razvoj odprto-kodnih in tudi zaprto-kodnih programov) in za več platform (GNU/Linux, UNIX, Mac OS X, Windows).
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/social-web-widgets.png">
	<img src="/announcements/4/4.4.0/thumbs/social-web-widgets_thumb.png" class="img-fluid" alt="Splet in družabna omrežja na namizju Plasma Desktop">
	</a> <br/>
	<em>Splet in družabna omrežja na namizju Plasma Desktop</em>
</div>
<br/>

<h4>
Še več sprememb
</h4>
<p align="justify">
Kot je že bilo omenjeno, je bilo predstavljenih le nekaj najpomembnejših novosti, sprememb in izboljšav delovnega okolja KDE Desktop, KDE-jevih programov in KDE-jevega ogrodja za razvoj programov. Popolnejši, a še vedno ne popoln, seznam lahko najdete na strani <a href="http://techbase.kde.org/Schedules/KDE4/4.4_Feature_Plan">KDE SC 4.4 Feature Plan</a> na <a href="http://techbase.kde.org">KDE TechBase</a>. Informacije o programih, ki se razvijajo izven ožje skupnosti KDE, lahko najdete na spletnih strani <a href="http://kde-apps.org">kde-apps.org</a>.
</p>

<h4>
    Razširite novico
</h4>

Skupnost KDE vsakogar spodbuja, da <strong>razširi novico</strong> prek družabnih omrežij. Pošljite novico na strani kot so Delicious, Digg, Reddit ali prek mikrobloganja na Twitter in identi.ca. Zaslonske posnetke objavite na Facebook, Flickr, ipernity in Picasa. Pri tem izberite ustrezno skupino. Posnemite video in ga objavite na Dailymotion, YouTube, Blip.tv, Vimeo ali drugo podobno stran. Vse kar objavite označite vsaj z <em>oznako <strong>kde</strong></em>, da bodo vsi lahko lažje našli vse te prispevke. Skupnost KDE bo na ta način tudi lažje sestavila poročilo o odzivu na izid KDE SC 4.4. <strong>Pomagajte nam razširiti novico in postanite del nje!</strong>

Vse kar se na družabnem spletu dogaja okoli izida KDE SC 4.4 lahko v živo spremljate na povsem novi strani <a href="http://buzz.kde.org"><strong>KDE Community livefeed</strong></a>. na tej strani se v živo zbirajo odzivi z identi.ca, Twitter, YouTube, Flickr, Picasa, blogov in mnogih drugih spletnih strani za druženje in mreženje. Obiščite <strong><a href="http://buzz.kde.org/">buzz.kde.org</a></strong> in bodite na tekočem. Namig: na namizje lahko dodate gradnik Spletni brskalnik in to stran odprete v njem.

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/linux_unix/KDE_Software_Compilation_4_4_0_Introduces_new_innovations"><img src="/announcements/buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/r/technology/comments/azx29/kde_software_compilation_440_introduces_netbook/"><img src="/announcements/buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde44"><img src="/announcements/buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde44"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde44"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde44"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts"><img src="/announcements/buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde44"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>

<h4>
  Namestitev KDE SC 4.4.0
</h4>
<p align="justify">
  KDE, vključno z vsemi knjižnicami in programi, je na prosto voljo pod odprto-kodnimi licencami. KDE lahko v obliki izvorne kode ali binarni obliki prenesete z <a href="http://download.kde.org/stable/4.4.0/">download.kde.org</a>, ga dobite <a href="/download">na zgoščenki</a>, ali pa je na voljo kot <a href="/distributions">del večjih distribucij operacijskih sistemov GNU/Linux, BSD in UNIX</a>.
</p>
<p align="justify">
  <em>Binarni paketi</em>:
  Nekateri ponudniki Linuxa ali UNIXa so pripravili binarne pakete s KDE SC 4.4.0 za nekatere različice svojih distribucij. V drugih primerih so to storili prostovoljci iz skupnosti. Ti binarni paketi so morda na voljo za prost prenos s KDE-jeve strani <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.4.0/">download.kde.org</a>. Dodatni binarni paketi in posodobitve trenutnih paketov bodo na voljo v prihodnjih tednih.
</p>

<p align="justify">
  <a id="package_locations"><em>Lokacije paketov</em></a>:
  Za trenutni seznam razpoložljivih binarnih paketov, o katerih je bila obveščena skupnost KDE, obiščite stran <a href="/info/4/4.4.0">KDE SC 4.4.0 Info</a>.
</p>

<h4>
  Prevajanje izvorne kode KDE SC 4.4.0
</h4>
<p align="justify">
  <a id="source_code"></a>
  Celotna izvorna koda za KDE SC 4.4.0 je na voljo za <a href="http://download.kde.org/stable/4.4.0/src/">prost prenos</a>. Navodila za prevajanje in namestitev KDE SC 4.4.0 so na voljo na strani <a href="/info/4/4.4.0#binary">KDE 4.4.0 Info</a>.
</p>

</p>



