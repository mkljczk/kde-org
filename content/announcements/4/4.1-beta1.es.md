---
aliases:
- ../announce-4.1-beta1
date: '2008-05-27'
title: Anuncio de lanzamiento de KDE 4.1 Beta1
---

<h3 align="center">
	El Proyecto KDE lanza la primera Beta de KDE 4.1
</h3>

<p align="justify">
  <strong>
La Comunidad de KDE anuncia la primera versión Beta de KDE 4.1
</strong>
</p>

<p align="justify">
El <a href="/">Proyecto KDE</a> está orgulloso de anunciar
la primera versión beta de KDE 4.1. La Beta 1 está dirigida a probadores, miembros
de la comunidad y entusiastas, de modo que puedan identificar errores y regresiones
para que la versión 4.1 pueda reemplazar por completo a KDE 3. La beta 1 de KDE 4.1
está disponible en forma de paquetes binarios para un amplio abanico de plataformas,
así como en paquetes fuente. KDE 4.1 está previsto que salga en julio de 2008.
</p>

<h4>
  <a id="changes">Principales aspectos de KDE 4.1 Beta 1</a>
</h4>

<p align="justify">
<ul>
    <li>Se ha expandido enormemente la funcionalidad y configurabilidad del interfaz de escritorio.</li>
    <li>La <i>suite</i> de gestión de información personal (KDE PIM) se ha migrado a KDE 4.</li>
    <li>Muchas aplicaciones han sido migradas y también han aparecido otras nuevas.</li>
</ul>
</p>

<h4>
Plasma crece
</h4>
<p align="justify">
Plasma, el innovador sistema que crea los menús y paneles que componen el escritorio
está madurando rápidamente. Ahora admite paneles múltiples redimensionables, 
permitiendo a los usuarios componer su escritorio de una forma tan flexible
como antes. La aplicación de menú, Kickoff, ha sido exhaustivamente pulida,
con un limpio y nuevo aspecto y muchas optimizaciones. La ventana de "Ejecutar orden"
ha sido puesta a punto para permitir a los usuarios avanzados lanzar rápidamente
aplicaciones, abrir documentos y visitar sitios web. El rendimiento del gestor de
ventanas por composición aumenta, incluyendo mejoras ergonómicas y visuales,
incluyendo la funcionalidad de Pase de Portadas (alt-tab) y el obligatorio efecto
de ondulación de ventanas.
</p>

<div class="text-center">
	<a href="/announcements/4/4.1-beta1/plasma-krunner.png">
	<img src="/announcements/4/4.1-beta1/plasma-krunner-small.png" class="img-fluid" alt="El nuevo KRunner (alt-F2)">
	</a> <br/>
	<em>El nuevo KRunner (alt-F2)</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/plasma-panelcontroller.png">
	<img src="/announcements/4/4.1-beta1/plasma-panelcontroller-small.png" class="img-fluid" alt="Vuelve la gestión de paneles">
	</a> <br/>
	<em>Vuelve la gestión de paneles</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kwin-coverswitch.png">
	<img src="/announcements/4/4.1-beta1/kwin-coverswitch-small.png" class="img-fluid" alt="Efecto de Pase de Portadas de KWin">
	</a> <br/>
	<em>Efecto de Pase de Portadas de KWin</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kwin-wobbly1.png">
	<img src="/announcements/4/4.1-beta1/kwin-wobbly1-small.png" class="img-fluid" alt="Ventanas que ondean">
	</a> <br/>
	<em>Ventanas que ondean</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kwin-wobbly2.png">
	<img src="/announcements/4/4.1-beta1/kwin-wobbly2-small.png" class="img-fluid" alt="Más ventanas que ondean">
	</a> <br/>
	<em>Más ventanas que ondean</em>
</div>
<br/>

<h4>
  Kontact vuelve
</h4>
<p align="justify">
Kontact, el gestor de información personal de KDE, y sus herramientas asociadas
han sido migradas a KDE 4 y serán presentadas por primera vez en KDE 4.1.
Muchas características de la rama Enterprise de KDE 3 han sido incorporadas,
haciendo que Kontact sea más útil en el entorno empresarial. Las mejoras incluyen nuevos
componentes como KTimeTracker y el componente para tomar notas KJots, un nuevo
aspecto, mejor soporte para múltiples calendarios y zonas horarias y manejo del
correo electrónico más robusto.
</p>

<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kontact-calendar.png">
	<img src="/announcements/4/4.1-beta1/kontact-calendar-small.png" class="img-fluid" alt="Múltiples calendarios en uso">
	</a> <br/>
	<em>Múltiples calendarios en uso</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kontact-kjots.png">
	<img src="/announcements/4/4.1-beta1/kontact-kjots-small.png" class="img-fluid" alt="KJots">
	</a> <br/>
	<em>KJots</em>
</div>
<br/>

<h4>
Las aplicaciones de KDE 4 crecen
</h4>
<p align="justify">
A lo largo y ancho de la comunidad de KDE, muchas aplicaciones se han migrado
a KDE 4 o han visto grandes mejoras en funcionalidad desde que se presentó KDE 4.
Dragon Player, el reproductor multimedia ligero, hace su debut. El reproductor de
CD de KDE vuelve. Una nueva herramienta de impresión proporciona una potencia y
flexibilidad nunca vistas en el escritorio de software libre. Konqueror obtiene
mayor soporte para navegación en sesiones web, un modo "deshacer" y un desplazamiento
más suave. Un nuevo modo de navegación de imágenes, incluyendo un interfaz a pantalla
completa ha llegado a Gwenview. Dolphin, el gestor de ficheros, ahora tiene vistas
con pestañas y muchas otras funcionalidades agradecidas por los usuarios de KDE 3,
incluyendo "Copiar A" y un árbol de carpetas mejorado. Muchas aplicaciones, incluyendo
el escritorio y las aplicaciones educativas de KDE, ahora proporcionan contenido
fresco como iconos, temas, mapas y material docente por medio de <i>Get New Stuff</i>,
que tiene un interfaz mejorado. El manejo de la red con Zeroconf se ha añadido a
varios juegos y utilidades, quitando el inconveniente de configurar los juegos
y el acceso remoto.
</p>

<div class="text-center">
		<a href="/announcements/4/4.1-beta1/dolphin-treeview.png">
		<img src="/announcements/4/4.1-beta1/dolphin-treeview-small.png" class="img-fluid" alt="Vista de árbol de Dolphin">
		</a> <br/>
		<em>Vista de árbol de Dolphin</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/dragonplayer.png">
		<img src="/announcements/4/4.1-beta1/dragonplayer-small.png" class="img-fluid" alt="Reproductor multimedia Dragon">
		</a> <br/>
		<em>Reproductor multimedia Dragon</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/games-kdiamond.png">
		<img src="/announcements/4/4.1-beta1/games-kdiamond-small.png" class="img-fluid" alt="Juego de puzzle KDiamond">
		</a> <br/>
		<em>Juego de puzzle KDiamond</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/games-kubrick.png">
		<img src="/announcements/4/4.1-beta1/games-kubrick-small.png" class="img-fluid" alt="Los años 80 en tu escritorio">
		</a> <br/>
		<em>Los años 80 en tu escritorio</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/konqueror.png">
		<img src="/announcements/4/4.1-beta1/konqueror-small.png" class="img-fluid" alt="Navegador Konqueror">
		</a> <br/>
		<em>Navegador Konqueror</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/marble-openstreetmap.png">
		<img src="/announcements/4/4.1-beta1/marble-openstreetmap-small.png" class="img-fluid" alt="Marble mostrando OpenStreetMaps">
		</a> <br/>
		<em>Marble mostrando OpenStreetMaps</em>
	</div>
	<br/>

<h4>
Refinamiento a través de <i>frameworks</i>
</h4>
<p align="justify">
Los desarrolladores han estado ocupados enriqueciendo las bibliotecas principales
de KDE y también su infraestructura. KHTML obtiene un aumento de velocidad
gracias a la carga anticipada de recursos, mientras que WebKit, su vástago,
se añade a Plasma para permitir usar en KDE <i>widgets</i> del Dashboard de Mac OS X.
El uso de la funcionalidad <i>Widgets on Canvas</i> de Qt 4.4 hace que Plasma 
sea más estable y ligero. La característica interfaz de KDE basada en un solo clic
obtiene un nuevo mecanismo de selección que promete velocidad y accesibilidad.
Phonon, el <i>framework</i> multimedia multiplataforma, incorpora soporte para
subtítulos y para los motores GStreamer, DirectShow 9 y QuickTime. La capa de
gestión de red se extiende para soportar diversas versiones de NetworkManager.
Y reconociendo que el Escritorio Libre valora la diversidad, se han comenzado
nuevos esfuerzos para compatibilizar escritorios, como soportar las especificaciones
de notificaciones emergentes y de marcadores de escritorio de freedesktop.org,
de modo que aplicaciones de otros escritorios puedan encajar en una sesión
KDE 4.1.
</p>

<h4>
  Versión final de KDE 4.1
</h4>
<p align="justify">
KDE 4.1 está planificado para ser lanzado el 29 de julio de 2008. Esta versión planificada por tiempo
se lanza seis meses después que KDE 4.0.
</p>

<h4>
  Tómalo, ejecútalo, pruébalo
</h4>
<p align="justify">
Voluntarios de la comunidad y proveedores de sistemas operativos Linux/UNIX proporcionan
paquetes binarios de KDE 4.0.80 (Beta 1) para la mayoría de distribuciones
de Linux, Mac OS X y Windows. Hay que ser consciente de que estos paquetes no están
considerados listos para su uso en producción. Se puede usar el sistema de
gestión de software de la propia distribución, o para ver instrucciones
específicas de cada una se pueden visitar los siguientes enlaces:
</p>

<ul>
<li><a href="http://fedoraproject.org/wiki/Releases/Rawhide"></a>Fedora</li>
<li><em>Debian</em> tiene KDE 4.1beta1 en <em>experimental</em>.</li>
<li>Los paquetes para <em>Kubuntu</em> se están preparando.</li>
<li><a href="http://wiki.mandriva.com/en/2008.1_Notes#Testing_KDE_4">Mandriva</a></li>
<li><a href="http://en.opensuse.org/KDE4#KDE_4_UNSTABLE_Repository_--_Bleeding_Edge"></a>openSUSE</li>
<li><a href="http://techbase.kde.org/Projects/KDE_on_Windows/Installation">Windows</a></li>
<li><a href="http://techbase.kde.org/Projects/KDE_on_Mac_OS_X">Mac OS X</a></li>
</ul>

<h4>
  Cómo compilar KDE 4.1 Beta 1 (4.0.80)
</h4>
<p align="justify">
  <a id="source_code"></a><em>Código fuente</em>.
  El código fuente completo para KDE 4.0.80 se puede <a
  href="/info/4/4.0.80">descargar libremente</a>.
Las instrucciones acerca de cómo compilar e instalar KDE 4.0.80
están disponibles en la <a href="/info/4/4.0.80">Página de Información
de KDE 4.0.80</a>, o en el <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>

<h4>
  Cómo apoyar a KDE
</h4>

<p align="justify">
KDE es un proyecto de <a href="http://www.gnu.org/philosophy/free-sw.html">Software Libre</a>
que existe y crece sólo mediante la ayuda de muchos voluntarios que donan su tiempo y esfuerzo. KDE siempre está buscando nuevos voluntarios y colaboradores, bien para ayudar programando, arreglando fallos o informando de ellos, escribiendo documentación, traducciones, promocionándolo, donando dinero, etc. Todas las colaboraciones son gratamente apreciadas y esperadas con mucho entusiasmo. Por favor, lea la página <a href="/community/donations/">Ayudar a KDE</a> para más información.
</p>

<p align="justify">
¡Esperamos noticias suyas pronto!
</p>
<h2>Acerca de KDE 4</h2>
<p>
KDE 4.0 es un innovador escritorio de Software Libre que contiene muchas aplicaciones
de uso diario, así como para propósitos específicos. Plasma es un nuevo interfaz de escritorio
desarrollado para KDE 4, proporcionando una forma intuitiva de interactuar con el escritorio y
las aplicaciones. El navegador web Konqueror integra la web en el escritorio. El gestor de
archivos Dolphin, el lector de documentos Okular y el centro de control System Settings completan
el conjunto de escritorio básico.

<br />
KDE está construido sobre las Bibliotecas KDE, las cuales proporcionan un acceso sencillo a los recursos
de la red mediante KIO y capacidades visuales avanzadas mediante Qt4. Phonon y Solid, que también son
parte de las Bibliotecas KDE, añaden un <i>framework</i> multimedia y mejor integración de hardware a
todas las aplicaciones KDE.
</p>



