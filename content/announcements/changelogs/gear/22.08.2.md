---
aliases:
- ../../fulllog_releases-22.08.2
title: KDE Gear 22.08.2 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi-contacts" title="akonadi-contacts" link="https://commits.kde.org/akonadi-contacts" >}}
+ Fix ignoring changes. [Commit.](http://commits.kde.org/akonadi-contacts/e0885eb6c1406df0a0f2133d7d1ebcc0ef39fbf9)
{{< /details >}}
{{< details id="ark" title="ark" link="https://commits.kde.org/ark" >}}
+ Stop killing extraction/compression jobs when dolphin quits. [Commit.](http://commits.kde.org/ark/291f8927211a036bf0c84e8548061c52b235c2dc)Fixes bug [#459346](https://bugs.kde.org/459346)
+ Fix incompatibility with original 7-Zip. [Commit.](http://commits.kde.org/ark/6187ddc4bed6b9503406c3b68ddfeec0358e7d99)Fixes bug [#456797](https://bugs.kde.org/456797)
{{< /details >}}
{{< details id="cantor" title="cantor" link="https://commits.kde.org/cantor" >}}
+ Show the default help information in the help panel only after the backend was already completely initialized. [Commit.](http://commits.kde.org/cantor/0a766b7ab72ab6a9d0e75b8e9e388ba420340623)Fixes bug [#459631](https://bugs.kde.org/459631)
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Doc: fix typo (Trash -> User Feedback). [Commit.](http://commits.kde.org/dolphin/e0c1c1c8c99d33be6c329264bce4b5cb08140020)
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Elide long music paths in music path listview. [Commit.](http://commits.kde.org/elisa/cfa1c05c017fa19dbad3c64d282f32f2f6f3aa5e)Fixes bug [#459451](https://bugs.kde.org/459451)
{{< /details >}}
{{< details id="eventviews" title="eventviews" link="https://commits.kde.org/eventviews" >}}
+ Handle completion of todos in distant time zones. [Commit.](http://commits.kde.org/eventviews/207702f87866d8dfbf5a6507ff096dc860ed903c)
{{< /details >}}
{{< details id="falkon" title="falkon" link="https://commits.kde.org/falkon" >}}
+ Update Firefox version in user agent manager. [Commit.](http://commits.kde.org/falkon/9082587737bf6b5a51f31588be925899ce33e18b)Fixes bug [#415467](https://bugs.kde.org/415467)
+ Searchbar: Fix show suggestions handling. [Commit.](http://commits.kde.org/falkon/f5bbdf24e178a7b97cceed27718da96a85629834)Fixes bug [#439268](https://bugs.kde.org/439268)
+ Update CHANGELOG with notable changes. [Commit.](http://commits.kde.org/falkon/feea1296442f5211ab7a3f3b17d8ddc43f4897f6)
{{< /details >}}
{{< details id="filelight" title="filelight" link="https://commits.kde.org/filelight" >}}
+ Fix appWindow.openURL. [Commit.](http://commits.kde.org/filelight/517a032923392f4626ae390b6a340fefb899d8ca)
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Fix sidebar toggle button having no icon with Fusion and Breeze style. [Commit.](http://commits.kde.org/gwenview/1054e1e14c131b3c2237f9cc8450c1311a9eb3b3)
+ Make the Share button's dropdown open instantly on left-click too. [Commit.](http://commits.kde.org/gwenview/0c3bff19555d0e13a3497d8339654824c6416b2d)
+ Mark GIMP (xcf) files as supported. [Commit.](http://commits.kde.org/gwenview/804efafb48ff354a5526d243f04d01eb5d197f88)
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Actually update the trip group when switching to a connected reservation. [Commit.](http://commits.kde.org/itinerary/2f6b78fe82a4d5707ffa352dfc8f30f036f48ab5)
+ Explicitly position event ticket header fields. [Commit.](http://commits.kde.org/itinerary/970ab2add44a359cdd8755a29fab0bf562c96b9e)
+ Retain aspect ratio of event banner images when scaling down. [Commit.](http://commits.kde.org/itinerary/a05be32409328a9a1bdd22c6ec63588a715a73a3)
+ Update current reservation id of the event page when changing tickets. [Commit.](http://commits.kde.org/itinerary/09aabe24eb7d88cbaeabcb6269f30925e3cc3a47)
{{< /details >}}
{{< details id="kaccounts-integration" title="kaccounts-integration" link="https://commits.kde.org/kaccounts-integration" >}}
+ Remove reference to a non existing id. [Commit.](http://commits.kde.org/kaccounts-integration/640ec48cf9693c43331b535e8438de0b36dcbec1)
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Bug 459901: Fix crash when alarm restored at login is deferred. [Commit.](http://commits.kde.org/kalarm/97e71e60e1f7deac26547adb4decfe3531d2b376)
{{< /details >}}
{{< details id="kalendar" title="kalendar" link="https://commits.kde.org/kalendar" >}}
+ Fix bottom padding of ScheduleView. [Commit.](http://commits.kde.org/kalendar/ca55661df8f26ea960843d53b9880ad1f0786284)
+ Fix delete contact action having unwanted empty dropdown. [Commit.](http://commits.kde.org/kalendar/d058b74763782ab701539455f7bebc3b6bc3194d)
+ Fix time label appearing in incidence info contents when no time available. [Commit.](http://commits.kde.org/kalendar/fa35d7b0b6f9d8beec94a04d018c47bf3595584b)
+ Fix more issues with time label in incidence info contents. [Commit.](http://commits.kde.org/kalendar/e57cb91a23cce4ab336b9c722c2d36c126d56e52)
+ Fix sort of overdue tasks, prevent overdue section being split up. [Commit.](http://commits.kde.org/kalendar/f68001f7c8755a2ac33021157c0f7dc968056daa)
+ Fix 'smart' section date labels for due date sort. [Commit.](http://commits.kde.org/kalendar/366310b4581261ba622bc013a9fd6aaec12a20de)
+ Fix tasks in the tasks view showing time for all day events. [Commit.](http://commits.kde.org/kalendar/2157cb9f305aa13b32a5e6b445bb4d9f63d07c0d)
+ Use standard keys for view navigation actions. [Commit.](http://commits.kde.org/kalendar/a61e2c23660b002d840ecf6c447cbea4d119fd88)
+ Fix scrolling on task completion slider not changing value. [Commit.](http://commits.kde.org/kalendar/ce818a696949b4a3d8c25cc228bb8349bfa6bc2b)
+ Fix double-click to edit in tasks view. [Commit.](http://commits.kde.org/kalendar/b555f1513730c88d2ab9bac45f9e0db447c0379a)
+ Fix keyboard selection on the todo tree view. [Commit.](http://commits.kde.org/kalendar/d5547a0451697a00334aae4b24c6d0ab4b778305)
+ Stop selecting todo delegate on right-click, only invoke context menu. [Commit.](http://commits.kde.org/kalendar/264122b945aa92f771952c92e4aaffce1219be6a)
+ Stop showing borked related incidence delegates for events. [Commit.](http://commits.kde.org/kalendar/4af6dc32ea30b92268f120fbb32ebe279671214d)
+ Fix issues with incidence info drawer sizing. [Commit.](http://commits.kde.org/kalendar/191c65228c05dd460bb390941dd251183f2b460c)
+ Fix 'Refresh all calendars' entry in hamburger menu. [Commit.](http://commits.kde.org/kalendar/b8272c93d02797672e93c8bf09c3df9621b855dd)
+ Ensure exception date strings wrap in the Incidence Info drawer. [Commit.](http://commits.kde.org/kalendar/0256a3a3ebcbccf1aededd9d0c8fc9a2fa949521)
+ Remove duplicate property definitions in incidencewrapper.h. [Commit.](http://commits.kde.org/kalendar/cb3e08b367a564f3aca4a70c25918c8fb2f42ce0)
+ Fixed tasks' tags getting compressed to nothing in the tasks view when the task summary is very long. [Commit.](http://commits.kde.org/kalendar/0d9c87530ea7ec8d1f0d02a0a4569459eed66a7b)
+ Fix openDayLayer. [Commit.](http://commits.kde.org/kalendar/17176b8091b3e6ea2f7fae1e143b3a46929d6999)
+ Fix calendar source mouse area in main sidebar. [Commit.](http://commits.kde.org/kalendar/512979e38d393540302d6de42aa21c04ad398b01)
{{< /details >}}
{{< details id="kapptemplate" title="kapptemplate" link="https://commits.kde.org/kapptemplate" >}}
+ Templates: remove accidentally addfed release info from appdata files. [Commit.](http://commits.kde.org/kapptemplate/1f1c8f675cb5ad373c3d515070afc99d5e65bbca)
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Fix crash in urlbar. [Commit.](http://commits.kde.org/kate/04ba544ea28f4dd840b2d73776ccf049cec7e97f)Fixes bug [#459612](https://bugs.kde.org/459612)
+ Avoid that we trigger setCurrentItem with invalid url, will lead to warning. [Commit.](http://commits.kde.org/kate/b2d940254a7ca1a93db4937dcd916f83ccca410c)
+ Fix compilation with KF 5.90. [Commit.](http://commits.kde.org/kate/4e2678cb15d864654fb6debf731829d34870bb9c)
+ Use QPlainTextEdit to fix display issues. [Commit.](http://commits.kde.org/kate/847da358d42ceb17ff4e0dbc7fe7f79e8d86238d)
{{< /details >}}
{{< details id="kcharselect" title="kcharselect" link="https://commits.kde.org/kcharselect" >}}
+ Remove duplicate content_rating tag. [Commit.](http://commits.kde.org/kcharselect/59eb4b75483935544fee4dd3ec1eaa6ef418f8cd)
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ Prevent null pointer dereference if there's no audio devices. [Commit.](http://commits.kde.org/kdeconnect-kde/c6c6b1f44323ddd7c22e9d366f37ce84bc907621)Fixes bug [#454917](https://bugs.kde.org/454917)
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix config and render ui tab order. [Commit.](http://commits.kde.org/kdenlive/b96f3f051cdc0af88ac64c04b9c18e8b49c79445)
+ Fix pasting ungrouped audio clip sometimes landing on unexpected track or complaining there is not enough tracks. [Commit.](http://commits.kde.org/kdenlive/0f31ad668d51f178d1e416f2943909ac6cccf824)
+ Fix tests. [Commit.](http://commits.kde.org/kdenlive/834732dc785c1e5da64e7b3d3c18f201d56b5370)
+ Ensure resource providers are not listed twice. [Commit.](http://commits.kde.org/kdenlive/c60b4f05c363293a542b38426f0fd3335f495ce1)Fixes bug [#460060](https://bugs.kde.org/460060)
+ Make timecode display listen to profile change and automatically adjust fps. [Commit.](http://commits.kde.org/kdenlive/98910a9651f44c453bc500aa022ee8331d0f5dad)
+ Remember effect keyframe status (show/hide). [Commit.](http://commits.kde.org/kdenlive/528f5b8f6f2bc42ad0907e89e1676f585e9fd03a)
+ Seek to item last frame on paste. [Commit.](http://commits.kde.org/kdenlive/da41e086b742edf1b2ea39a52aebf9973f36f68a)
+ Fix effect overlay not properly scaling on monitor zoom. [Commit.](http://commits.kde.org/kdenlive/0d159652b306d3d7ce3607ff8778390b2276cb0b)
+ Timeline preview: ensure we don't insert chunks of the wrong size (would cause 1 on 2 chunks to fail insert), ensure the orange "working" chunks disappears on stop. [Commit.](http://commits.kde.org/kdenlive/bb8e30082543a4e47ecf00afdae15881cbc9b359)
+ Fix possible profile corruption when switching to a never used profile. [Commit.](http://commits.kde.org/kdenlive/8040e2afd37420ba962c42b6f2e2aa9d7b0c7093)
+ Fix crash closing proxy test dialog. [Commit.](http://commits.kde.org/kdenlive/0add3c68425fb96301b57cd195cb9d3d68837341)
+ [Image Sequences] Fix loop option. [Commit.](http://commits.kde.org/kdenlive/5010483eb9e07be81fea8a3e9e25e100ceab4bc0)Fixes bug [#382432](https://bugs.kde.org/382432)
+ [Image Sequence] Fix wrong thumbnails. [Commit.](http://commits.kde.org/kdenlive/dc88cc22222293ef6e40483d00a9f714eb896505)
+ Fix thumbnails for loopable clips. [Commit.](http://commits.kde.org/kdenlive/a2c9a6efe32f03778647cea7d0ce723ee073d848)
+ [Online Resources] Add provider for Pixabay Videos. [Commit.](http://commits.kde.org/kdenlive/f65a48a07182c5a6aa0322caa96680c99ef4462f)Fixes bug [#435569](https://bugs.kde.org/435569)
+ [Online Resource Providers] Support object downloadUrls arrays. [Commit.](http://commits.kde.org/kdenlive/05dda422e625daf3c14a70c72d4fbd562e618640)
+ [Resource Widget] Fix open license and provider website. [Commit.](http://commits.kde.org/kdenlive/b2145f1ea45a4632f12e6611d86d23f9ae70aea9)
+ Fix track audio level empty on pause. [Commit.](http://commits.kde.org/kdenlive/e5e2c1131e61c1388f0832f2b67678322a759eba)
+ Align master audio level with MLT's audiolevel filter (use only the first 200 samples). [Commit.](http://commits.kde.org/kdenlive/248217d32b495d4b7d9772701675d0ceeaea567f)
+ Don't add unnecessary audio level filter on master. [Commit.](http://commits.kde.org/kdenlive/287442ab0112b01a07873d5cd187a70486b8928c)
+ Minor fix for updated MLT audiolevel filter (will fix track levels). [Commit.](http://commits.kde.org/kdenlive/25957fa0f25047a939539a135df4614aa980e752)
+ Deprecate MLT's old boxblur filter (replaced with new box_blur effect). [Commit.](http://commits.kde.org/kdenlive/1ff1c163028c19938a78f8650ec119e07fecd72e)
+ Fix resetting keyframe selection after deleting a keyframe from timeline. [Commit.](http://commits.kde.org/kdenlive/058c66c4c5060bda7a6838a8115e5cd720b1b0bf)
+ Fix pasting effect with keyframes partially broken. [Commit.](http://commits.kde.org/kdenlive/9ef2ee5ea3f9e9450858d430792db30493fb00b5)
+ Correctly preselect timeline toolbar when editing it from context menu. [Commit.](http://commits.kde.org/kdenlive/755def5b963f6ec8787c93cbd24a9a01e45a60d7)
+ Fix effect stack view incorrect on hide keyframes (was still showing the timecode). [Commit.](http://commits.kde.org/kdenlive/7fb1bea555a667474b4c42881e7a8a8eb18da6f8)
+ Fix ghost keyframes created when pasting an effect to a clip that has a crop start smaller than source clip and on clip speed resize. [Commit.](http://commits.kde.org/kdenlive/d9d663ed70c0d9b7217d157187d699b4f6f3f9b9)
+ Fix wrong timecode offset in keyframewidget of transitions. [Commit.](http://commits.kde.org/kdenlive/246682a095700df3081b258bb0623659209336b8)Fixes bug [#439748](https://bugs.kde.org/439748)
+ Fix crash on bin clip deletion with instance on locked track. [Commit.](http://commits.kde.org/kdenlive/505a09f8bad262540aefd51d691a1641f91cadc2)Fixes bug [#459260](https://bugs.kde.org/459260)
+ Add test for bin clip deletion with instance on locked track. [Commit.](http://commits.kde.org/kdenlive/96a87abe0148104e985b09585aec8cd76356c250)See bug [#459260](https://bugs.kde.org/459260)
+ Fixed thumbnail cache not being rebuilt anymore in "Show video preview in thumbnails" mode. [Commit.](http://commits.kde.org/kdenlive/854d3ffa346cc3f4026390a2057b8aa1eb48317d)
+ Don't update keyframe parameters when changing a keyframe selection state. [Commit.](http://commits.kde.org/kdenlive/825ad3473e790ee25690a30fab67606d70deff14)
+ Fix tests crash. [Commit.](http://commits.kde.org/kdenlive/c682bae55e2e56f630d3ecf0bc1bd68f57908819)
+ Fix vp8 with alpha render crash. [Commit.](http://commits.kde.org/kdenlive/855ca5f4294251892c72aaf2399df0543c8c018d)
+ Don't delete audio tasks when switching profile. [Commit.](http://commits.kde.org/kdenlive/cb9784cdb0ba693af941fc45c8fe6a106980ab5e)
+ Fix usage count column visible in bin. [Commit.](http://commits.kde.org/kdenlive/ed83b193e2794b3a9669c99aa6cb038416efd656)
+ Fix uninitialized var messing audio record and possible crash. [Commit.](http://commits.kde.org/kdenlive/a66448d362702828a9652610ef11bf312383230e)
+ Fix sorting by date not working for newly inserted clips, other sorting issues. [Commit.](http://commits.kde.org/kdenlive/daabb0ab708a27caf6da5a2b6ec2d91d08881f44)See bug [#458784](https://bugs.kde.org/458784)
+ Don't mess rotation data on proxy transcoding. [Commit.](http://commits.kde.org/kdenlive/34e66aaafdd07ec925ea06d16f3f33d0c0182fde)
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Allow to debug code. [Commit.](http://commits.kde.org/kdepim-addons/6136b39f63ad05869f26e0191c62090354896426)
+ Rename variable + const. [Commit.](http://commits.kde.org/kdepim-addons/f6cb1cf13c7fa9f405cee864245d26de40bf64c8)
+ Fix replace word. [Commit.](http://commits.kde.org/kdepim-addons/4c66ed9eaa133874ec20e01223ecd251a6e2bade)
+ Fix bug 459113: bad replacement by Language Too. [Commit.](http://commits.kde.org/kdepim-addons/42325595ebc81460e90bb3db5820c8f02cd7c2c9)Fixes bug [#459113](https://bugs.kde.org/459113)
+ Continue to debug grammar support. [Commit.](http://commits.kde.org/kdepim-addons/2af8dc8ca440d3a7436084af2d624f434aecb591)
+ Implement autotest. [Commit.](http://commits.kde.org/kdepim-addons/fa34e7b37e42ce98a70c628bca6bee3623c24c45)
+ Add autotest. [Commit.](http://commits.kde.org/kdepim-addons/8f4c565216ccd46262769a6e19eae1161d1a3d47)
+ Prepare to create autotest. [Commit.](http://commits.kde.org/kdepim-addons/ec572abcd433b0fdbb25cdbaf98078934850e754)
+ Remove duplicate entries (server returns duplicate entries no idea why). [Commit.](http://commits.kde.org/kdepim-addons/88705e25f502542f069bacebfe953461009c6b35)
+ Extract apply changes => we will able to autotest it. [Commit.](http://commits.kde.org/kdepim-addons/59138ed2754083201f50ff46b787d7090a9ff424)
+ Fix load list of languages. [Commit.](http://commits.kde.org/kdepim-addons/c78ac1334dae334354478f376c21c6b63518aa39)
+ Scale down event ticket banner images when necessary. [Commit.](http://commits.kde.org/kdepim-addons/06f0c7b4b5340fa2ce5ed7f40ddbc68a4df970d8)
{{< /details >}}
{{< details id="kdepim-runtime" title="kdepim-runtime" link="https://commits.kde.org/kdepim-runtime" >}}
+ Hangle imap id <= 0 better. [Commit.](http://commits.kde.org/kdepim-runtime/c08928aeeab6ca54db091f82c95d4e52b29389e9)
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Smb/kio_smb_browse: Check error code `EINVAL` to prompt the password dialog. [Commit.](http://commits.kde.org/kio-extras/95ebf9a66c4f503dd41205abadd7a522e758d974)Fixes bug [#453090](https://bugs.kde.org/453090)
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Handle German language variants of Vueling boarding passes as well. [Commit.](http://commits.kde.org/kitinerary/d4aeec952152a6c2938278c6da0b83648f6e676c)
+ Extract arrival/departure times from Vueling boarding passes. [Commit.](http://commits.kde.org/kitinerary/b7201ef06e5b917f0434bc0eed69517f7c7f4549)
+ Work around ERA SSB departure times being somewhat unreliable for Thalys. [Commit.](http://commits.kde.org/kitinerary/e0b1a42cddaf2bc1edc47ee27299a9c107c648fc)
+ Support German language variants of Thalys PDF tickets as well. [Commit.](http://commits.kde.org/kitinerary/fbb85331d236761690f09d173c878fb83d3fbcc0)
+ Support the German language variant of the SNCF PDF tickets. [Commit.](http://commits.kde.org/kitinerary/a5c780138fa85234bcbce76a2e65038e95f374b2)
+ Also handle German language variants of Inoui seat reservations. [Commit.](http://commits.kde.org/kitinerary/4ca5fea4ae3efb89239cc054911446b92bee876d)
+ Handle German language variants of SNCF Inoui tickets. [Commit.](http://commits.kde.org/kitinerary/ecba975326ba627754dcff80302fb3581976b9ba)
+ Be more tolerant when comparing locality names. [Commit.](http://commits.kde.org/kitinerary/3b907a929978b758da7c005f7c6959b00d1420d7)
+ Deal with JSON data containing XML entities. [Commit.](http://commits.kde.org/kitinerary/d595820e9bfb5f4d9ef3d5c5f62eee2ed9bd925a)
+ Consider transliteration when comparing person names. [Commit.](http://commits.kde.org/kitinerary/04f044c10a09d2d6e380442143d77eb3f0742833)
+ Move transliteration method to StringUtil. [Commit.](http://commits.kde.org/kitinerary/61eabc88037cdc04bf26fe866e80a445140e9cfc)
+ Relax Ryanair boarding pass time search a bit. [Commit.](http://commits.kde.org/kitinerary/3363dff1bdb8d5e306e176ff25e3648103a33a87)
+ Fix no-ZXing test reference data being out of data with recent changes. [Commit.](http://commits.kde.org/kitinerary/ad61e2980325cc26755185297a4168a8c6670999)
+ Ignore broken Amadeus PDF document times. [Commit.](http://commits.kde.org/kitinerary/14d29b57ff74ddd9749a28a97a8c33121f455f4e)
+ Add accessor for the PDF document producer. [Commit.](http://commits.kde.org/kitinerary/962f36a57c9d90c831aae0a5d7e3a96ff4dbb201)
+ Prefer explicit boarding time fields over relevantTime. [Commit.](http://commits.kde.org/kitinerary/e79238cb62d0add7e3e8c8dcf08fec68c51b3775)
+ Add Eventim pkpass extractor script. [Commit.](http://commits.kde.org/kitinerary/3694960cd7d36f9c037c8a55a4f0f05918a9fcc5)
+ Support SNCF regional tickets from Normandie. [Commit.](http://commits.kde.org/kitinerary/1bbd484a509bde42a0ebee323666b8f234ebf63c)Fixes bug [#451409](https://bugs.kde.org/451409)
+ Add extractor script for BlaBlaCar Bus. [Commit.](http://commits.kde.org/kitinerary/323381f1fe8ca888c351eaaf59efff18db3859a7)
+ Allow ticketedSeat for bus extractors. [Commit.](http://commits.kde.org/kitinerary/e4d2606460317be1190c60a3a97a65e3b6f81122)
{{< /details >}}
{{< details id="kjumpingcube" title="kjumpingcube" link="https://commits.kde.org/kjumpingcube" >}}
+ Fix returning to file selection dialog after not confirming overwrite. [Commit.](http://commits.kde.org/kjumpingcube/a2942231dab51f82e74bc12403a7b1210372f60c)
+ Fix double confirmation dialog on overwriting a file. [Commit.](http://commits.kde.org/kjumpingcube/e449272e84d407238f3c45a912161fe4589e12c7)
{{< /details >}}
{{< details id="konqueror" title="konqueror" link="https://commits.kde.org/konqueror" >}}
+ Fix crash caused by using original view rather than actual view. [Commit.](http://commits.kde.org/konqueror/2d050101105638f5d72f1e3486d80a0ead1fd721)Fixes bug [#458933](https://bugs.kde.org/458933)
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Set _enableReflowLines flag from profile when switching profiles. [Commit.](http://commits.kde.org/konsole/b748f4b879c3fe1c9fe12a7e860024454e6729cc)Fixes bug [#459040](https://bugs.kde.org/459040)
{{< /details >}}
{{< details id="kpimtextedit" title="kpimtextedit" link="https://commits.kde.org/kpimtextedit" >}}
+ Delay close widget. Otherwise it's hard to configure settings. [Commit.](http://commits.kde.org/kpimtextedit/1ef4fb7706868699ca346a233fd8d8d1ef83bf71)
{{< /details >}}
{{< details id="kwalletmanager" title="kwalletmanager" link="https://commits.kde.org/kwalletmanager" >}}
+ Move KCM related desktop file back to konfigurator. [Commit.](http://commits.kde.org/kwalletmanager/df86b0ef286a13dcec2d67f83648dc109a88c793)
{{< /details >}}
{{< details id="libkeduvocdocument" title="libkeduvocdocument" link="https://commits.kde.org/libkeduvocdocument" >}}
+ Also use mkpath to make sure destination exists. [Commit.](http://commits.kde.org/libkeduvocdocument/928694d3f46252caccca864f2f01375ae916950d)
+ Use sync QFile apis for moving files instead of async KIO. [Commit.](http://commits.kde.org/libkeduvocdocument/2861106a9b39f4a04868ee874f2ab823e3c62848)
{{< /details >}}
{{< details id="mailcommon" title="mailcommon" link="https://commits.kde.org/mailcommon" >}}
+ Add missing QGpgME/gpgme++ dependency. [Commit.](http://commits.kde.org/mailcommon/278ebdb03bc61cf6a7783258db74f5ce30b01563)
{{< /details >}}
{{< details id="picmi" title="picmi" link="https://commits.kde.org/picmi" >}}
+ Start using KDE Gear/Release service number for app version. [Commit.](http://commits.kde.org/picmi/ed40eea7722a60780232c532f774dda797dc0a17)
{{< /details >}}
{{< details id="pimcommon" title="pimcommon" link="https://commits.kde.org/pimcommon" >}}
+ Fix warning when create shared text menu. [Commit.](http://commits.kde.org/pimcommon/c890b0076d07821b0d7aa7cde8c042fa6c9a66c1)
{{< /details >}}
{{< details id="umbrello" title="umbrello" link="https://commits.kde.org/umbrello" >}}
+ Fixed saving unicode XMI file. [Commit.](http://commits.kde.org/umbrello/ebf03c266e01331147374eaef044d067538e7e08)
{{< /details >}}
