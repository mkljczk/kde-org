---
aliases:
- /announcements/plasma-5.6.2-5.6.3-changelog
hidden: true
plasma: true
title: Plasma 5.6.3 Complete Changelog
type: fulllog
version: 5.6.3
---

### <a name='bluedevil' href='http://quickgit.kde.org/?p=bluedevil.git'>Bluedevil</a>

- Restore adapter powered state after 1s delay. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=9c5fda0232bc18148430044b2f73617b6cef12e1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357720'>#357720</a>

### <a name='discover' href='http://quickgit.kde.org/?p=discover.git'>Discover</a>

- Hide 0 rating. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=dc289b730c27a99603fc130f7639c286d07e60bf'>Commit.</a>
- Properly report information on non-found ratings. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=3d634a93f7a2fb9e44dc16b1b0537e1356a035a2'>Commit.</a>
- Use actual installation information to infer popularity. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=204497cdaef32839776e25f04ba73cc8227bbfa4'>Commit.</a>
- Fix how we maintain the resources model sorted. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=f3e9ea57aafdfc14e5845f4c3899b0257a1610fd'>Commit.</a>
- Remove Rating::rating ambiguity. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=21ca977e717c52362a6652f8fd76945f7912275e'>Commit.</a>
- Expose the rating count on the ResourcesModel. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=9abc609cd9c6f296f884e6f7ee296a6e0eec98ce'>Commit.</a>
- Account for components having no package as installation candidate. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=683642fb214bfacec23b69dac9f82481337d572a'>Commit.</a>

### <a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a>

- [Weather] Fix: do not open browser url on clicking area left to credit line. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=3f298efd0b72fc410d6310ebbd887a64d81987f6'>Commit.</a>
- [Weather] do not allow in NotificationArea, broken in Plasma 5.6. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=17f023020674377ec8465b8141a6dcc0e6f89ac6'>Commit.</a>
- [Weather] Fix: do not trigger wrong timeout message on location selection. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=30d8ca7e35e71b31a8cabba4e1b0b8c5f235fcbf'>Commit.</a>
- [libplasmaweather] add workaround for crash risk with empty associatedAppUrls. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=b811ce4c817ff352afeeb887db2f0044626318d3'>Commit.</a>
- [Weather] Fix: prevent credit line being drawn outside widget borders. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=13743cab777425df6a7828ebf5b4c6be43b120af'>Commit.</a>
- [libplasmaweather] Do not set an empty url as associated application url. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=8e5e8314de7cf72c2275220b27298e8e0ced3e8f'>Commit.</a>
- Add i18n() to a UI facing string. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=fc115e595d6d3c3caf8a7f98794de449bce52b64'>Commit.</a>
- Fix typo in string. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=9439869d92814c05631199477c7c7f1949231d6a'>Commit.</a>
- Fix null values in currency conversion. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=59614f7d8d9a811eff9d7ab1d63ea2d08124ab6e'>Commit.</a>

### <a name='kscreen' href='http://quickgit.kde.org/?p=kscreen.git'>KScreen</a>

- Extract strings from ui files to make the kcm fully translated. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=36617b909a418ed2c934a107c09926919703ba1c'>Commit.</a>

### <a name='kwayland' href='http://quickgit.kde.org/?p=kwayland.git'>KWayland</a>

- [client] Fix FakeInput::requestPointerAxis. <a href='http://quickgit.kde.org/?p=kwayland.git&amp;a=commit&amp;h=05ab9e49a2a3941f90f4a958244e514f96119235'>Commit.</a>

### <a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a>

- [autotests] Only start Xwayland if compositor created a scene. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=380c5e0bd84f116e97dd8ace704f95e92b6d735a'>Commit.</a>
- Only start Xwayland server if Compositor created a Scene. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=df2c26e3d9a2c3e95e0bd3def290c564ff62a278'>Commit.</a>
- Fix crash on repainting an invalid sizes decoration. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0df4406c2cf8df56f90a7a006eb911775a120886'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361551'>#361551</a>

### <a name='libkscreen' href='http://quickgit.kde.org/?p=libkscreen.git'>libkscreen</a>

- Verify rotation when updating screen size in XRandR backend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c3cb380aa0835c268a84a4c480854b91ab9a76f2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356228'>#356228</a>

### <a name='libksysguard' href='http://quickgit.kde.org/?p=libksysguard.git'>libksysguard</a>

- [KSysGuardProcessList] Don't forward keys with modifiers (except shift) to the search field. <a href='http://quickgit.kde.org/?p=libksysguard.git&amp;a=commit&amp;h=4b14421cf723b961699975973735cb050d98312d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/335177'>#335177</a>

### <a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a>

- Make sure desktop toolbox has integer and even size. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e2d5d00f0469b8754174833a871eb812757fba53'>Commit.</a>
- Fix hover effect on desktop in pager. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=7a66cb7d63a505da0e630361dd8ae9377d0ba0d6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361392'>#361392</a>

### <a name='plasma-nm' href='http://quickgit.kde.org/?p=plasma-nm.git'>Plasma Networkmanager (plasma-nm)</a>

- Applet: do not hardcode width for password field. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=370d4057603be06525f251765752a193344a6a95'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361812'>#361812</a>
- Right-align labels in IPv4/IPv6 dialogs. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=744a2daa25152fba36d093c0e0a88da496b59650'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127643'>#127643</a>
- Mark placeholder strings as non translatable. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=2ab08a82033a039fb340a89dd230670dc4aa4da0'>Commit.</a>

### <a name='plasma-pa' href='http://quickgit.kde.org/?p=plasma-pa.git'>Plasma Audio Volume Control</a>

- Fix MapBase::reset(). <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=2edb4f296e31bf6d2d500aa32a82290fec5ef8b8'>Commit.</a>

### <a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a>

- Close popup upon item select. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ddcf590f968f9b11c392208bf05f4b2072c6e334'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361629'>#361629</a>
- TaskManager: Group tasks when losing demands attention state. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=509de050b81f473a182c997683735d9ffc43a01c'>Commit.</a>
- [weather bbcukmet] Add missing "thick cloud", improve some cloud mapping. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=266dc037196ba81425368ec5e3ded3507241654a'>Commit.</a>
- Guard KPluginInfo access. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ae978485290127c033b81013c33c10222d8936ff'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360927'>#360927</a>