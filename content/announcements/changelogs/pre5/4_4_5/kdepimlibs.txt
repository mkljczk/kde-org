------------------------------------------------------------------------
r1135440 | krake | 2010-06-07 22:08:29 +1200 (Mon, 07 Jun 2010) | 8 lines

If the AgentManager instance is created before Akonadi is running, its D-Bus interface is not valid yet.
However, it never becomes valid afterwards, so it never knows any types or instances, even if they are available.

This lead to ServerManager believing that Akonadi did not start correctly, letting it run into the safety timeout and producing a self test log.
Unfortunately this self test log then contains wrong information ("no resource agents found")

BUG: 236538

------------------------------------------------------------------------
r1135935 | tokoe | 2010-06-09 00:32:02 +1200 (Wed, 09 Jun 2010) | 2 lines

Backport of bugfix #240617

------------------------------------------------------------------------
r1140144 | smartins | 2010-06-20 12:29:11 +1200 (Sun, 20 Jun 2010) | 2 lines

Use utf-8 insteaf of iso8859

------------------------------------------------------------------------
r1140147 | smartins | 2010-06-20 13:34:11 +1200 (Sun, 20 Jun 2010) | 5 lines

Don't crash when formating an incidence.

BUG: 236462
BUG: 242191

------------------------------------------------------------------------
r1140155 | smartins | 2010-06-20 13:55:43 +1200 (Sun, 20 Jun 2010) | 4 lines

Crash even less.

MERGE: trunk,e35

------------------------------------------------------------------------
r1140666 | smartins | 2010-06-21 21:51:12 +1200 (Mon, 21 Jun 2010) | 16 lines

SVN_MERGE:
Merged revisions 1140662 via svnmerge from 
svn+ssh://smartins@svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim

........
  r1140662 | smartins | 2010-06-21 09:38:16 +0000 (Mon, 21 Jun 2010) | 8 lines
  
  Fixed some unitentional fallthroughs on monthly and yearly recurence.
  
  Also added some breaks for readability (didn't falthrough here due to returns, but wasn't explicit)
  
  Fix compiler warning about function not returning.
  
  MERGE: trunk
........

------------------------------------------------------------------------
r1141996 | scripty | 2010-06-24 14:00:27 +1200 (Thu, 24 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1142729 | tokoe | 2010-06-26 02:10:07 +1200 (Sat, 26 Jun 2010) | 5 lines

Check for akonadiItemId property when doing a Nepomuk search.

This backport fix performance issues when searching contacts
(e.g. resolving distribution lists or lookup names)

------------------------------------------------------------------------
r1142807 | mueller | 2010-06-26 06:41:24 +1200 (Sat, 26 Jun 2010) | 2 lines

KDE 4.4.5

------------------------------------------------------------------------
