2008-09-27 15:57 +0000 [r865445]  lueck <lueck@localhost>:

	* branches/KDE/4.1/kdegames/doc/kolf/index.docbook,
	  branches/KDE/4.1/kdegames/doc/kpat/index.docbook,
	  branches/KDE/4.1/kdegames/doc/ksirk/index.docbook,
	  branches/KDE/4.1/kdegames/doc/bovo/index.docbook: documentation
	  backport for 4.1.3 from trunk

2008-10-05 05:50 +0000 [r867980]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdegames/konquest/konquest.desktop,
	  branches/KDE/4.1/kdegames/libkmahjongg/backgrounds/default.desktop,
	  branches/KDE/4.1/kdegames/kgoldrunner/themes/black-on-white.desktop,
	  branches/KDE/4.1/kdegames/libkdegames/digits/themes/individual-digit-sample.desktop,
	  branches/KDE/4.1/kdegames/libkdegames/digits/themes/fourteen-segment-sample.desktop,
	  branches/KDE/4.1/kdegames/libkdegames/carddecks/svg-oxygen-white/index.desktop,
	  branches/KDE/4.1/kdegames/knetwalk/src/knetwalk.desktop,
	  branches/KDE/4.1/kdegames/bovo/themes/spacy/themerc,
	  branches/KDE/4.1/kdegames/ksirk/ksirk/ksirk.desktop,
	  branches/KDE/4.1/kdegames/bovo/themes/scribble/themerc,
	  branches/KDE/4.1/kdegames/kgoldrunner/themes/nostalgia.desktop,
	  branches/KDE/4.1/kdegames/libkmahjongg/tilesets/traditional.desktop,
	  branches/KDE/4.1/kdegames/libkdegames/carddecks/decks/deck28.desktop,
	  branches/KDE/4.1/kdegames/kgoldrunner/themes/default.desktop,
	  branches/KDE/4.1/kdegames/kjumpingcube/pics/default.desktop,
	  branches/KDE/4.1/kdegames/kjumpingcube/kjumpingcube.desktop,
	  branches/KDE/4.1/kdegames/kbounce/themes/geometry.desktop,
	  branches/KDE/4.1/kdegames/bovo/themes/highcontrast/themerc,
	  branches/KDE/4.1/kdegames/kollision/kollision.desktop,
	  branches/KDE/4.1/kdegames/libkdegames/carddecks/svg-oxygen/index.desktop,
	  branches/KDE/4.1/kdegames/libkmahjongg/tilesets/alphabet.desktop,
	  branches/KDE/4.1/kdegames/kgoldrunner/themes/nostalgia-blues.desktop,
	  branches/KDE/4.1/kdegames/libkdegames/carddecks/decks/deck0.desktop,
	  branches/KDE/4.1/kdegames/libkmahjongg/backgrounds/summerfield.desktop,
	  branches/KDE/4.1/kdegames/libkdegames/carddecks/svg-nicu-white/index.desktop,
	  branches/KDE/4.1/kdegames/bovo/themes/gomoku/themerc,
	  branches/KDE/4.1/kdegames/klines/klines.desktop,
	  branches/KDE/4.1/kdegames/kmahjongg/layouts/pyramid.desktop,
	  branches/KDE/4.1/kdegames/kubrick/src/kubrick.desktop,
	  branches/KDE/4.1/kdegames/kgoldrunner/src/KGoldrunner.desktop,
	  branches/KDE/4.1/kdegames/kmines/themes/green.desktop: SVN_SILENT
	  made messages (.desktop file)

2008-10-06 17:17 +0000 [r868579]  aacid <aacid@localhost>:

	* branches/KDE/4.1/kdegames/ktuberling/hi128-app-ktuberling.png:
	  Make the background transparent

2008-10-12 06:11 +0000 [r870247]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdegames/kdiamond/src/kdiamond.notifyrc:
	  SVN_SILENT made messages (.desktop file)

2008-10-14 06:16 +0000 [r871192]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdegames/kdiamond/src/kdiamond.notifyrc,
	  branches/KDE/4.1/kdegames/klines/themes/klines-gems.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-17 06:07 +0000 [r872392]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdegames/ktuberling/pics/potato-game.desktop,
	  branches/KDE/4.1/kdegames/konquest/konquest.desktop,
	  branches/KDE/4.1/kdegames/libkmahjongg/backgrounds/default.desktop,
	  branches/KDE/4.1/kdegames/lskat/grafix/default.desktop,
	  branches/KDE/4.1/kdegames/ksudoku/src/gui/ksudoku.desktop,
	  branches/KDE/4.1/kdegames/ksudoku/src/themes/default.desktop,
	  branches/KDE/4.1/kdegames/kdiamond/src/kdiamond.notifyrc,
	  branches/KDE/4.1/kdegames/knetwalk/themes/default.desktop,
	  branches/KDE/4.1/kdegames/kmahjongg/layouts/default.desktop,
	  branches/KDE/4.1/kdegames/ksirk/ksirk/ksirk.desktop,
	  branches/KDE/4.1/kdegames/libkdegames/carddecks/svg-tigullio-international/index.desktop,
	  branches/KDE/4.1/kdegames/klines/themes/default.desktop,
	  branches/KDE/4.1/kdegames/kbounce/themes/default.desktop,
	  branches/KDE/4.1/kdegames/kmines/themes/default.desktop,
	  branches/KDE/4.1/kdegames/kbreakout/themes/default.desktop,
	  branches/KDE/4.1/kdegames/kblocks/themes/default.desktop,
	  branches/KDE/4.1/kdegames/ktuberling/pics/christmas.desktop,
	  branches/KDE/4.1/kdegames/libkdegames/carddecks/decks/deck27.desktop,
	  branches/KDE/4.1/kdegames/kgoldrunner/themes/default.desktop,
	  branches/KDE/4.1/kdegames/kdiamond/src/kdiamond.desktop,
	  branches/KDE/4.1/kdegames/kbreakout/themes/simple.desktop,
	  branches/KDE/4.1/kdegames/kbounce/themes/geometry.desktop,
	  branches/KDE/4.1/kdegames/bovo/bovo.desktop,
	  branches/KDE/4.1/kdegames/kfourinline/kfourinline.desktop,
	  branches/KDE/4.1/kdegames/kmahjongg/layouts/triangle.desktop,
	  branches/KDE/4.1/kdegames/knetwalk/src/knetwalk.notifyrc,
	  branches/KDE/4.1/kdegames/libkmahjongg/tilesets/default.desktop,
	  branches/KDE/4.1/kdegames/libkdegames/carddecks/svg-gm-paris/index.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-18 13:25 +0000 [r872990]  fela <fela@localhost>:

	* branches/KDE/4.1/kdegames/knetwalk/src/mainwindow.cpp: backport
	  of commit 867509 Fixed a KXmlGuiWindow issue that was causing the
	  status bar to be hidden on application restart. BUG:170344

2008-10-20 06:39 +0000 [r873772]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdegames/kdiamond/src/kdiamond.desktop,
	  branches/KDE/4.1/kdegames/bovo/bovo.desktop,
	  branches/KDE/4.1/kdegames/kfourinline/kfourinline.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-21 06:50 +0000 [r874285]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdegames/kolf/courses/ReallyEasy,
	  branches/KDE/4.1/kdegames/kjumpingcube/kjumpingcube.desktop,
	  branches/KDE/4.1/kdegames/klines/themes/klines-gems.desktop,
	  branches/KDE/4.1/kdegames/kmines/data/kmines.notifyrc,
	  branches/KDE/4.1/kdegames/kolf/courses/Impossible: SVN_SILENT
	  made messages (.desktop file)

2008-10-24 11:50 +0000 [r875419]  dimsuz <dimsuz@localhost>:

	* branches/KDE/4.1/kdegames/kshisen/board.cpp,
	  branches/KDE/4.1/kdegames/kshisen/board.h,
	  branches/KDE/4.1/kdegames/kshisen/app.cpp: Backport a bugfix
	  (r875409) from trunk

2008-10-24 14:38 +0000 [r875464]  dimsuz <dimsuz@localhost>:

	* branches/KDE/4.1/kdegames/kshisen/board.cpp,
	  branches/KDE/4.1/kdegames/kshisen/board.h,
	  branches/KDE/4.1/kdegames/kshisen/app.cpp: Backport revisions
	  r875450 and r875463

2008-10-28 19:33 +0000 [r877154]  lueck <lueck@localhost>:

	* branches/KDE/4.1/kdegames/doc/ksirk/quit.png (added),
	  branches/KDE/4.1/kdegames/doc/ksirk/newGame.png (added): added
	  missing png's

