#!/usr/bin/env ruby
#
# Copyright (C) 2008 Harald Sitter <apachelogger@ubuntu.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of
# the License or (at your option) version 3 or any later version
# accepted by the membership of KDE e.V. (or its successor approved
# by the membership of KDE e.V.), which shall act as a proxy
# defined in Section 14 of version 3 of the license.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'fileutils'
fu = FileUtils

if ARGV.length < 2 or ARGV[0].include?("help")
    puts " Error, not enough arguments"
    puts " Please use this syntax: create_full_changelog.rb REV1 REV2 [BRANCHVERSION]\n"
    puts " Example:\n"
    puts "\tcreate_full_changelog.rb 123456 134567 4.1\n"
    puts " to create a changelog of KDE 4.1 between the revisions 123456 and 134567."

    exit 1
end

SREVISION = ARGV[0] # start revision
EREVISION = ARGV[1] # end revision
BVERSION  = ARGV[2] # branch version

@svnpath = "svn://anonsvn.kde.org/home/kde"

if BVERSION
    @svnpath += "/branches/KDE/#{BVERSION}"
else
    @svnpath += "/trunk/KDE"
end

@modules = %x[svn ls #{@svnpath}].split("\n")

if File.exist?("fullchangelogs")
    fu.rm_rf("fullchangelogs")
end
Dir.mkdir("fullchangelogs")

for m in @modules
    m = m.chop
    system("svn log -r#{SREVISION}:#{EREVISION} #{@svnpath}/#{m} > fullchangelogs/#{m}.txt")
    if $? != 0
        puts "Module #{m} failed"
    else
        puts "Module #{m} finished"
    end
end 

puts "All done :-)"
