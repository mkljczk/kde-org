2006-01-29 19:59 +0000 [r503653]  larkang

	* branches/KDE/3.5/kdemultimedia/kscd/kscd.cpp: Make sure current
	  track gets selected when cddb-lookup is done, and some smaller
	  cleanup BUG: 119061

2006-02-02 01:24 +0000 [r504742-504741]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/playlist.cpp,
	  branches/KDE/3.5/kdemultimedia/juk/playlist.h: Fix bug 121176
	  (Manual Column widths notice interferes with dragging columns) in
	  JuK. Basically, when you drag a column to resize it, when JuK is
	  busy setting the widths automatically, JuK will automatically
	  switch to manual resize. But, it also notifies you of the switch.
	  This has the effect of Qt missing the mouse release event, so the
	  header still thinks it is being dragged and just causes all
	  around weirdness. I've fixed this by delaying the notification
	  dialog until after the resize is done, which makes more sense
	  anyways I think. cc'ing Carsten for changelog update please. :)
	  BUG:121176 CCMAIL:cniehaus@kde.org

	* branches/KDE/3.5/kdemultimedia/juk/main.cpp: I missed this for
	  KDE 3.5.1, so bump the version before I forget about 3.5.2.

2006-02-02 19:03 +0000 [r504987]  mueller

	* branches/KDE/3.5/kdemultimedia/juk/ktrm.cpp: port fixes for
	  libtunepimp 0.4x in amarok to this code as well.

2006-02-03 19:21 +0000 [r505379]  jriddell

	* branches/KDE/3.5/kdemultimedia/debian (removed): Remove debian
	  directory, now at
	  http://svn.debian.org/wsvn/pkg-kde/trunk/packages/kdemultimedia
	  svn://svn.debian.org/pkg-kde/trunk/packages/kdemultimedia

2006-02-09 01:17 +0000 [r507316]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/gstreamerplayer.cpp: Use the
	  correct sentinel value for g_object calls when using gstreamer.
	  Could cause a crash on 64-bit architectures it seems, although
	  there is no reported JuK bug to this effect. See also Amarok's
	  bug 90869.

2006-02-09 12:12 +0000 [r507506]  jriddell

	* branches/KDE/3.5/kdemultimedia/kmix/restore_kmix_volumes.desktop:
	  Add OnlyShowIn=KDE; to autostart files that won't interest
	  non-KDE users

2006-02-09 23:11 +0000 [r507751-507749]  wheeler

	* branches/KDE/3.5/kdemultimedia/juk/tag.cpp: Print the file being
	  loaded so that if there's a taglib crash we can figure out which
	  file it happened in.

	* branches/KDE/3.5/kdemultimedia/juk/filehandle.cpp: Just an extra
	  sanity check that I had sitting around.

2006-02-16 02:16 +0000 [r510010]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/k3bexporter.cpp: Take
	  advantage of relaxed message freeze to improve two messages in
	  JuK. Unfortunately two related (but different) actions had the
	  same string, meaning that they are identical in the Keyboard
	  Shortcuts and Configure Toolbars dialog boxes. "Add To Audio or
	  Data CD" has been split into the appropriate two clarified
	  strings. The affected file is kdemultimedia/juk/k3bexporter.cpp
	  CCMAIL:kde-i18n@kde.org

2006-02-16 20:13 +0000 [r510298]  larkang

	* branches/KDE/3.5/kdemultimedia/libkcddb/cdinfo.cpp: Fix handling
	  of multiline comments BUG: 122093

2006-02-17 01:28 +0000 [r510357]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/k3bexporter.h,
	  branches/KDE/3.5/kdemultimedia/juk/playlistcollection.cpp,
	  branches/KDE/3.5/kdemultimedia/juk/playlistcollection.h,
	  branches/KDE/3.5/kdemultimedia/juk/k3bexporter.cpp: Fix bug
	  121621 (Action List in Configure Shortcuts includes up to as many
	  Add to CD items as you have playlists). I'm still not in love
	  with the patch so the underlying code may change a bit between
	  here and there however. Mailing Carsten for changelog update
	  please. :) BUG:121621 CCMAIL:cniehaus@gmx.de

2006-02-17 02:54 +0000 [r510372]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/upcomingplaylist.cpp,
	  branches/KDE/3.5/kdemultimedia/juk/upcomingplaylist.h: Fix bug
	  100564 (Crash after hiding Play Queue) in JuK. Some of the
	  involved code was of dubious design. Unfortunately, I was the one
	  who wrote it. =D. The problem was that JuK uses a class to handle
	  choosing the next playlist item, which is independant of the
	  playlist code (a separate class). But when using the Play Queue,
	  those functions are combined using two special classes for that
	  purpose. When de-selecting the Play Queue, if an item wasn't
	  already playing, I forgot to re-install the default track
	  selector class, and now there was a split where none was
	  expected, and things degenerated from there. CC'ing Carsten again
	  for changelog update, please. BUG:100564 CCMAIL:cniehaus@gmx.de

2006-02-18 22:52 +0000 [r511125]  esken

	* branches/KDE/3.5/kdemultimedia/kmix/kmixdockwidget.cpp,
	  branches/KDE/3.5/kdemultimedia/kmix/kmix.cpp: Fix update problems
	  of the KMix dock icon, which were introduced with the
	  QSocketNotifier stuff in KDE3.5. See the 'Fix kmix sytray icon
	  disappears' thread on kde-core-devel for more details.

2006-02-20 03:01 +0000 [r511482]  wheeler

	* branches/KDE/3.5/kdemultimedia/juk/gstreamerplayer.h,
	  branches/KDE/3.5/kdemultimedia/juk/gstreamerplayer.cpp,
	  branches/KDE/3.5/kdemultimedia/juk/configure.in.in: GStreamer
	  0.10 support (0.8 is still there)

2006-02-21 20:48 +0000 [r512112]  esken

	* branches/KDE/3.5/kdemultimedia/kmix/viewapplet.cpp,
	  branches/KDE/3.5/kdemultimedia/kmix/viewapplet.h,
	  branches/KDE/3.5/kdemultimedia/kmix/mdwslider.cpp: Fix the "icons
	  ruin vertical panel applet usability" issue. Fix the "Split
	  channel in kmix applet shows one channel as a slider, the other
	  numerical" issue. BUGS: 122114 BUGS: 97666

2006-02-22 13:15 +0000 [r512410]  thiago

	* branches/KDE/3.5/kdemultimedia/mpeglib_artsplug/Makefile.am:
	  Adding -no-undefined: Link to your dependency libraries

2006-02-23 23:57 +0000 [r512943]  esken

	* branches/KDE/3.5/kdemultimedia/kmix/kmixtoolbox.cpp: Fix bug
	  121451 : Playback and Capture Sliders now save their GUI
	  properties indepencently (thus you can for example show the
	  playback and hide the capture controls). The bugfix contains a
	  "compatibilty loader", that loads the old properties if new
	  ".Capture" properties are not found. This fix needs confirmation
	  from testers. CCBUGS: 121451

2006-02-24 13:36 +0000 [r513083]  wheeler

	* branches/KDE/3.5/kdemultimedia/juk/playlist.cpp: This has been
	  bugging me. Set the keyboard focus using the "jump to playing"
	  button.

2006-02-26 01:06 +0000 [r513669]  bmeyer

	* branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/rippingandencoding.png
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/index.docbook
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/generalconfiguration.png
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/lameconfiguration.png
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/encoderconfiguration.png
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/cddbconfigurationsubmit.png
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/setalbumcategory.png
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/jobcontrol.png
	  (added), branches/KDE/3.5/kdemultimedia/doc/kaudiocreator
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/cddbconfigurationlookup.png
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/cdconfiguration.png
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/kaudiocreatormainwindow800.png
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/confirmartistcarryover.png
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/readytorip.png
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/cdinserted.png
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/rippingandencoding2.png
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/entersong1.png
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/startalbumeditor.png
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/Makefile.am
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/jobshavestarted.png
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/ripperconfiguration.png
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/encodernotfound.png
	  (added): docs for kaudiocreator

2006-02-26 09:10 +0000 [r513732]  hasso

	* branches/KDE/3.5/kdemultimedia/juk/googlefetcher.cpp: Don't
	  punish non-english users and make getting cover from Internet
	  working again for them - run search always in english. I will not
	  close #116181 though because mentioned asserts are still there.
	  CCMAIL:kde-et@linux.ee CCBUG:116181

2006-03-17 21:34 +0000 [r519786]  coolo

	* branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm: tagging 3.5.2

