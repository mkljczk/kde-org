2008-01-31 19:19 +0000 [r769182]  gateau

	* branches/KDE/4.0/kdegraphics/gwenview/app/mainwindow.cpp,
	  branches/KDE/4.0/kdegraphics/gwenview/lib/thumbnailview.cpp: Make
	  sure thumbnail view is always synced with the thumbnail slider on
	  start, even if the slider is already using the current thumbnail
	  size. Based on a patch by Ilya Konkov. Backported
	  https://svn.kde.org/home/kde/trunk/KDE/kdegraphics@769174

2008-02-01 00:46 +0000 [r769283-769282]  pino

	* branches/KDE/4.0/kdegraphics/okular/core/generator.cpp: instead
	  of assert()'ing, just acts like a Document and delete the pixmap
	  request

	* branches/KDE/4.0/kdegraphics/okular/core/document.cpp,
	  branches/KDE/4.0/kdegraphics/okular/core/document_p.h: Keep a
	  list of the currently active pixmap requests (usually one at
	  most), and wait for their finish when closing the document.

2008-02-01 20:37 +0000 [r769680]  lueck

	* branches/KDE/4.0/kdegraphics/doc/gwenview/index.docbook,
	  branches/KDE/4.0/kdegraphics/doc/kgamma/index.docbook,
	  branches/KDE/4.0/kdegraphics/doc/okular/index.docbook:
	  documentation backport from trunk

2008-02-02 16:03 +0000 [r770015]  pino

	* branches/KDE/4.0/kdegraphics/okular/part.cpp,
	  branches/KDE/4.0/kdegraphics/okular/shell/main.cpp,
	  branches/KDE/4.0/kdegraphics/okular/ui/annotationwidgets.cpp:
	  Adapt to the name change: okular -> Okular. (can be done as the
	  new message is already in the pot)

2008-02-02 19:15 +0000 [r770093]  mlaurent

	* branches/KDE/4.0/kdegraphics/gwenview/part/gvpart.cpp: Backport:
	  don't allow to reduce image to null size

2008-02-05 19:08 +0000 [r771341]  tokoe

	* branches/KDE/4.0/kdegraphics/okular/part.cpp,
	  branches/KDE/4.0/kdegraphics/okular/core/bookmarkmanager.cpp,
	  branches/KDE/4.0/kdegraphics/okular/part.h,
	  branches/KDE/4.0/kdegraphics/okular/core/bookmarkmanager.h:
	  Backport of bugfix #157189

2008-02-06 14:06 +0000 [r771606]  lueck

	* branches/KDE/4.0/kdegraphics/doc/okular/index.docbook: backport
	  from trunk

2008-02-11 01:37 +0000 [r773441]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/pageviewutils.cpp,
	  branches/KDE/4.0/kdegraphics/okular/ui/pageviewutils.h,
	  branches/KDE/4.0/kdegraphics/okular/ui/pageview.cpp: Backport:
	  use the id of the form fields as id, not the name.

2008-02-11 01:48 +0000 [r773445]  pino

	* branches/KDE/4.0/kdegraphics/okular/generators/fictionbook/converter.cpp:
	  Backport SVN commit 773347 by tokoe: Remove unneeded code, if we
	  don't have a creator, don't tell okular about it.

2008-02-11 17:35 +0000 [r773747-773745]  pino

	* branches/KDE/4.0/kdegraphics/cmake/modules/FindOkular.cmake:
	  polish up a bit: - lowercase the function calls - use
	  FindPackageHandleStandardArgs - a bit of doc

	* branches/KDE/4.0/kdegraphics/cmake/modules/CMakeLists.txt
	  (added), branches/KDE/4.0/kdegraphics/cmake/CMakeLists.txt
	  (added): nstall the "public" cmake modules, only FindOkular atm

2008-02-11 17:44 +0000 [r773749]  pino

	* branches/KDE/4.0/kdegraphics/okular/aboutdata.h,
	  branches/KDE/4.0/kdegraphics/okular/CMakeLists.txt,
	  branches/KDE/4.0/kdegraphics/okular/core/version.h (added): Add
	  an installed header with the okular version as number and string
	  (ala kdeversion.h); use the version string in the about data.

2008-02-12 11:01 +0000 [r774014]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/formwidgets.cpp: don't
	  enable read-only widgets

2008-02-14 09:41 +0000 [r774877]  pino

	* branches/KDE/4.0/kdegraphics/okular/generators/ooo/CMakeLists.txt:
	  link to less libraries (no need for linking to poppler here)

2008-02-16 14:56 +0000 [r775746]  gateau

	* branches/KDE/4.0/kdegraphics/gwenview/lib/urlutils.cpp (added),
	  branches/KDE/4.0/kdegraphics/gwenview/app/mainwindow.cpp,
	  branches/KDE/4.0/kdegraphics/gwenview/lib/urlutils.h (added),
	  branches/KDE/4.0/kdegraphics/gwenview/lib/CMakeLists.txt,
	  branches/KDE/4.0/kdegraphics/gwenview/lib/document/loadingdocumentimpl.cpp,
	  branches/KDE/4.0/kdegraphics/gwenview/lib/thumbnailloadjob.cpp:
	  Make sure thumbnail view is always synced with the thumbnail
	  slider on start, even if the slider is already using the current
	  thumbnail size. Based on a patch by Ilya Konkov. Backported
	  https://svn.kde.org/home/kde/trunk/KDE/kdegraphics@769174

2008-02-26 11:50 +0000 [r779522]  pino

	* branches/KDE/4.0/kdegraphics/okular/core/version.h,
	  branches/KDE/4.0/kdegraphics/okular/VERSION: bump version to
	  0.6.2

2008-02-26 13:02 +0000 [r779542]  pino

	* branches/KDE/4.0/kdegraphics/okular/generators/poppler/CMakeLists.txt,
	  branches/KDE/4.0/kdegraphics/okular/generators/fictionbook/CMakeLists.txt,
	  branches/KDE/4.0/kdegraphics/okular/generators/tiff/CMakeLists.txt,
	  branches/KDE/4.0/kdegraphics/okular/generators/kimgio/CMakeLists.txt:
	  link to less libraries

2008-02-26 18:00 +0000 [r779637]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/bookmarklist.cpp:
	  Backport: add a tooltip for the bookmark name. CCBUG: 158219

2008-02-27 00:41 +0000 [r779800]  pino

	* branches/KDE/4.0/kdegraphics/okular/generators/dvi/fontpool.cpp:
	  Backport: make sure to unload the fonts before unloading
	  freetype. CCBUG: 155317

