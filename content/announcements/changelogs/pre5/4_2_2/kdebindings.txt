------------------------------------------------------------------------
r933068 | arnorehn | 2009-02-27 22:11:18 +0000 (Fri, 27 Feb 2009) | 2 lines

* backport a patch from Eric Butler.

------------------------------------------------------------------------
r933256 | arnorehn | 2009-02-28 13:52:47 +0000 (Sat, 28 Feb 2009) | 3 lines

* backport bug fix from trunk (marshall QStringLists with
  QString::fromUtf8)

------------------------------------------------------------------------
r933303 | arnorehn | 2009-02-28 16:12:05 +0000 (Sat, 28 Feb 2009) | 2 lines

* backport escaped-characters fix from trunk

------------------------------------------------------------------------
r934313 | arnorehn | 2009-03-02 19:08:46 +0000 (Mon, 02 Mar 2009) | 3 lines

backports from trunk (check for name clashes
of public variable accessors with enums)

------------------------------------------------------------------------
r934774 | rdale | 2009-03-03 19:14:27 +0000 (Tue, 03 Mar 2009) | 5 lines

* The initializer function for the QtTest dll should be Init_qttest(),
and not Init_QtTest(). Thanks to FireRabbit for finding the bug.

CCMAIL: kde-bindings@kde.org

------------------------------------------------------------------------
r936933 | sharhalakis | 2009-03-08 17:06:32 +0000 (Sun, 08 Mar 2009) | 3 lines

Remove initial empty line from generated script.
Add UTF-8 coding marker for generated script.

------------------------------------------------------------------------
r937870 | rdale | 2009-03-10 17:01:41 +0000 (Tue, 10 Mar 2009) | 4 lines

* Add method_missing and const_missing handlers for top level modules such
  as Soprano


------------------------------------------------------------------------
r941221 | sedwards | 2009-03-19 07:16:21 +0000 (Thu, 19 Mar 2009) | 3 lines

Added kde4.py back in. It is needed by anyone runtime loading a .ui file which contains KDE widgets.


------------------------------------------------------------------------
r941225 | sedwards | 2009-03-19 07:22:45 +0000 (Thu, 19 Mar 2009) | 2 lines

(revert the last change to CMakeLists.txt)

------------------------------------------------------------------------
r942174 | sedwards | 2009-03-21 10:33:07 +0000 (Sat, 21 Mar 2009) | 3 lines

Enabled KIconDialog::setup() and KIconButton::setIconType().


------------------------------------------------------------------------
r942475 | arnorehn | 2009-03-21 20:20:45 +0000 (Sat, 21 Mar 2009) | 2 lines

backport the dbusreply & variant fixes from trunk

------------------------------------------------------------------------
r943351 | arnorehn | 2009-03-23 17:49:53 +0000 (Mon, 23 Mar 2009) | 2 lines

backport mono_jit_cleanup fix from trunk

------------------------------------------------------------------------
r943760 | arnorehn | 2009-03-24 13:42:42 +0000 (Tue, 24 Mar 2009) | 2 lines

backport the qvariantmap-marshaller fix from trunk

------------------------------------------------------------------------
r944264 | rdale | 2009-03-25 11:25:50 +0000 (Wed, 25 Mar 2009) | 6 lines

* QVariantExtras.cs wasn't compiling as a ToHash() method was missing, so
  promote the version of QVariant.cs in the trunk to the release branch.
  Thanks to 'int' on irc for pointing out the problem

CCMAIL: kde-bindings@kde.org

------------------------------------------------------------------------
r944512 | rdale | 2009-03-25 15:55:41 +0000 (Wed, 25 Mar 2009) | 4 lines

* Promote the QtRuby 2.0.3 release version to the KDE release branch

CCMAIL: kde-bindings@kde.org

------------------------------------------------------------------------
r944530 | rdale | 2009-03-25 16:26:11 +0000 (Wed, 25 Mar 2009) | 2 lines

* Promote the trunk version of the soprano ruby bindings to the release branch 

------------------------------------------------------------------------
