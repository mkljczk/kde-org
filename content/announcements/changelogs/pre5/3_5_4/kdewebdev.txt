2006-06-10 13:38 +0000 [r549953]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/main.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectnewgeneral.cpp:
	  Fix the strings marked with "todo" and extract the copyright year
	  from the i18n'd string.

2006-06-11 21:09 +0000 [r550470]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/data/tips,
	  branches/KDE/3.5/kdewebdev/quanta/dialogs/donationdialog.ui: Use
	  the correct web address. If there are objection, you can revert
	  it (I will not be near a computer for some days). CCMAIL:
	  kde-i18n-doc@kde.org

2006-06-11 21:24 +0000 [r550473]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/main.cpp: Make the
	  copyright year separation work.

2006-07-03 09:03 +0000 [r557428]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/treeviews/structtreeview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Make Open File in
	  context menu work for remote projects as well.

2006-07-06 10:25 +0000 [r558935]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/dialogs/actionconfigdialog.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Make possible to
	  remove a shortcut assigned to an action.

2006-07-06 11:23 +0000 [r558958]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/project/project.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Don't become
	  confused if exitting is cancelled during the toolbar saving
	  process.

2006-07-06 13:41 +0000 [r559030]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Default to a better
	  place when saving a global toolbar to the local toolbar directory
	  and improve the error message if a toolbar cannot be saved.
	  CCBUG: 130236

2006-07-06 13:52 +0000 [r559040]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp: This is better
	  for newly created toolbars.

2006-07-06 14:35 +0000 [r559068]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp: Go to the
	  Quanta homepage as the menu item says.

2006-07-06 16:06 +0000 [r559114]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/project/project.cpp: Don't
	  loose the last project setting.

2006-07-23 13:48 +0000 [r565443]  coolo

	* branches/KDE/3.5/kdewebdev/quanta/src/quanta.h: preparing KDE
	  3.5.4

2006-07-23 14:02 +0000 [r565479]  coolo

	* branches/KDE/3.5/kdewebdev/kdewebdev.lsm: preparing KDE 3.5.4

