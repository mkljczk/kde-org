---
aliases:
- ../changelog2_2_1to2_2_2
hidden: true
title: KDE 2.2.1 to 2.2.2 Changelog
---

<p>
This page tries to present as much as possible of the problem
corrections that occurred in KDE between the 2.2.1 and 2.2.2 releases.
The primary goal of the 2.2.2 release is to deliver a number of bugfixes
for problems that have been solved for KDE 3.0 without the need to wait
for KDE 3.0.
</p>

<p>
Some security related issues in kssl, kdm and klprfax (efax) have been
addressed in this release. We strongly recommend people who run KDE in
multi-user environments to upgrade to this version.
</p>

<h3>General</h3><ul>
  <li>Icon loading has received a fix leading to 5% speedup.</li>
</ul>

<h3>kdelibs</h3><ul>
<li>kssl: minor security fix in SSL certificate handling.</li>
<li>kssl: reworked SSL certificate caching to avoid races (created KSSLD).</li>
<li>kfile: filedialog speedups.</li>
<li>kfile: fixed saving of recently selected files in the filedialog.</li>
<li>kfile: fixed case sensitivity and sorting settings in the filedialog's splitted view.</li>
<li>kjs: regexp support fix, added access to attributes.</li>
<li>kio: clear clipboard after moving files with cut and paste.</li>
<li>kio: fixed handling of HTTP links that redirect to FTP.</li>
<li>kio-http: Make POST action work when using an SSL tunnel through a proxy.</li>
<li>kio-http: Fix filename determination from content-disposition and also allow it from HTTP/1.0 servers.</li>
<li>kio-http: Fixed bug that caused encrypted headers to be send to proxy servers.</li>
<li>kcontrol-nework-preferences:  Fix the dialog used to set timeout settings.  It was hoplessly broken.
<li>kcontrol-webbrowsing-cookies: Cosmetic fixes of the cookie management dialog box
<li>kio-ftp: fixed parsing of answers to PASV and EPSV commands.</li>
<li>kio-smb: don't leave smbclients using 100% cpu hanging around.</li>
<li>kio-smb: fix handling of filenames and usernames containing non-ascii characters.</li>
<li>kdeui: fixed bug with some Qt styles, where toolbar buttons would have 
no caption beside/under.</li>
<li>kdeui: fixed KRootPixmap losing tinting when resizing</li>
<li>kdeui: fixed KSharedPixmap displaying bitmap with wrong offset if window overlapped left/above desktop edge</li>
<li>kdecore: fixed various problems with KExtendedSocket.</li>
</ul>

<h3>kdeaddons</h3><ul>
</ul>

<h3>kdeadmin</h3><ul>
</ul>

<h3>kdeartwork</h3>No changes

<h3>kdebase</h3><ul>
<li>kdesktop: fixed positionning icon at drop position, disable paste when nothing to paste.</li>
<li>kdesktop: fixed coredump on exit.</li>
<li>kdesktop: fixed multiple wallpapers changing always 1 minute too late.</li>
<li>konqueror: support for CodeWeaver plugins.</li>
<li>konqueror: fix for spurious .directory files being created everywhere.</li>
<li>konqueror: deselect the automatically selected item when selecting items using the mouse.</li>
<li>konqueror: fewer repaints in konqueror's iconview.</li>
<li>konqueror: fixed sorting in konqueror's textview.</li>
<li>kate: removed warning message in filedialog when attempting to save a remote (http) file.</li>
<li>kate: improved/fixed handling of large files.</li>
<li>konsole: added ukrainian symbol "ghe with upturn".</li>
<li>konsole: fixed double-click marking for koi8-u encoding.</li>
<li>konsole: fixed font selection and font save settings bugs.</li>
<li>konsole: fixed middle after double-left click detection.</li>
<li>konsole: remove kwrited's utmp entry at logout.</li>
<li>konsole: removed &quot;--nowelcome&quot; parameter and &quot;Settings/Locale&quot; menu entry.</li>
<li>konsole: use "Konsole Default" schema as Konsole default.</li>
<li>konsole: added missing definitions for Alt+Backspace and "newSession" to keytables.</li>
<li>konsole: changed order of windows title to &quot;[&lt;caption&gt;(if set) - ]&lt;session name&gt;&quot;</li>
<li>konsole: fixed performance loss when switching desktop with sticky transparent Konsole on it.</li>
<li>konsole: added support for esc[s and esc[u to save and restore the cursor position.
<li>konsole: setting a scroll region now takes effect on both primary and alternate screen.</li>
<li>konsole: when embedded, fixed quoting of special chars in auto "cd" command.</li>
<li>konsole: fixed bug where window failed to close when multiple sessions were active.</li>
<li>kthememgr: make CommonDesktop work even if there's no wallpaper.</li>
<li>kthememgr: make PluginLib (KWin decoration) work even for builtin KWin decorations.</li>
<li>kthememgr: making snapshots of the current settings should work better now.</li>
<li>kdm: fixed symlink vulnerability in .wmrc access introduced in 2.2.</li>
<li>kdm: fixed potential security problem in pam invocation.</li>
<li>kdm: fixed .wmrc access problems (session type saving).</li>
<li>kdm: fixed crash on chooser invocation.<li>
<li>kdm: fixed potentially harmful side-effects of failed session starts.<li>
<li>kdm: minor fixes 
<li>kcontrol-nework-preferences:  Fixed dialog used to set timeout settings.
<li>kcontrol-webbrowsing-cookies: Cosmetic fixes.
</ul>

<h3>kdebindings</h3><ul>
</ul>

<h3>kdegames</h3><ul>
<li>kshisen: Fix for the often reported end game bug associated with
the gravity option.</li>
</ul>

<h3>kdegraphics</h3><ul>
kghostview: allow paging with the wheelmouse in the PS/PDF viewer component.</li>
kghostview: fixed a memory leak in the PS/PDF viewer component.</li>
</ul>

<h3>kdemultimedia</h3><ul>
kmix: Mix applet now also works with a vertical panel.
</ul>

<h3>kdenetwork</h3><ul>
<li>kmail: fixed two bugs, that cause crashing on certain mails.</li>
<li>kmail: a failing pop3 server does no longer cause all accounts to fail.</li>
<li>lanbrowsing: several (usability) bugfixes, should be easier to setup/use now.</li>
</ul>

<h3>KDEPIM</h3><ul>
</ul>

<h3>KDESDK</h3><ul>
</ul>

<h3>KDEToys</h3><ul>
</ul>

<h3>KDEUtils</h3><ul>
<li>kfind: several bugfixes, including "don't crash the system anymore".</li>
<li>kfind: search for multiple file name patterns at once, e.g. "*C *cpp *cc"  
(separated by whitespace).</li>
<li>kcalc: minor bugfixes.</li>
<li>kcharselect: no longer offsets unicode fonts. highlights glyphs not in 
font.</li>
<li>klprfax/efax: security problem with efax corrected, efax is no longer installed suid.</li>
</ul>

<h3>KDdevelop</h3><ul>
<li>KDevelop-2.0.2 source code compilable/startable with KDE3 now.</li>
<li>important bugfixes for non-latin languages.</li>
<li>internal debugger can debug multithreaded applications now.</li>
<li>bugfixes for internal debugger: display of Qt3 strings with static
members; disassemble off when invisible.</li>
<li>project templates uses KDE's current admin.tar.gz, fixes for
autoconf/automake problems in most templates.</li>
<li>accelerators added for accessibility of all tool-views via keyboard.</li>
<li>TabPage MDI mode got accelerators Alt+0...9 for view switching.</li>
<li>bugfixes concerning the disappearing editor cursor.</li> 
<li>bugfixes for executing list items in the tree tool-views.</li>
<li>arguments for start of external tool applications fixed.</li>
<li>generating of the API documentation with kdoc fixed.</li>
<li>syntax highlighting for *.inl and *.tlh files.</li>
<li>importing of foreign projects now recognizes *.inl and *.tlh files as
template-based sources.</li>
<li>avoid 'all' argument when calling make (some Makefiles don't have such
section).</li>
<li>several bugfixes in QextMDI library.</li>
<li>classparser fixed for the showing of namespace prefixes in class
declarations.</li>
</ul>

<h3>KDoc</h3>No changes.