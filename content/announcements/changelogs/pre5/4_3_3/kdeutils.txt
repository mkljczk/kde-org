------------------------------------------------------------------------
r1030866 | dakon | 2009-10-03 11:30:13 +0000 (Sat, 03 Oct 2009) | 4 lines

set some sane defaults for column width

BUG:192375

------------------------------------------------------------------------
r1031304 | dakon | 2009-10-04 16:54:57 +0000 (Sun, 04 Oct 2009) | 1 line

Add a bunch of Q_ASSERT()'s. This will make nothing worse but if one of these is hit it would be easier to debug
------------------------------------------------------------------------
r1031305 | dakon | 2009-10-04 16:58:13 +0000 (Sun, 04 Oct 2009) | 1 line

clean up editor creation
------------------------------------------------------------------------
r1031308 | dakon | 2009-10-04 17:13:27 +0000 (Sun, 04 Oct 2009) | 1 line

use the defined debug area for KGpg and nothing else
------------------------------------------------------------------------
r1032267 | dakon | 2009-10-07 12:41:34 +0000 (Wed, 07 Oct 2009) | 1 line

remove a Q_ASSERT() that would cause trouble when the function is called from a constructor
------------------------------------------------------------------------
r1032534 | scripty | 2009-10-08 03:10:39 +0000 (Thu, 08 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1034470 | markuss | 2009-10-12 20:05:48 +0000 (Mon, 12 Oct 2009) | 1 line

Fix a missing icon
------------------------------------------------------------------------
r1034605 | nlecureuil | 2009-10-13 09:13:32 +0000 (Tue, 13 Oct 2009) | 3 lines

Allow to handle fstab when it contains UUID
CCBUG:118123

------------------------------------------------------------------------
r1036158 | kossebau | 2009-10-16 18:09:51 +0000 (Fri, 16 Oct 2009) | 2 lines

backport of 1036157: fixed: do not crash on empty clipboard (like there is if starting a live cd, presenting Okteta and accidently hitting the MMB :P )

------------------------------------------------------------------------
r1036267 | kossebau | 2009-10-16 21:32:59 +0000 (Fri, 16 Oct 2009) | 1 line

backport of 1036211: fixed: enable Paste action only if clipboard has content (and not rely on the clients who might claim to read everything)
------------------------------------------------------------------------
r1037427 | scripty | 2009-10-19 03:22:50 +0000 (Mon, 19 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1040111 | dakon | 2009-10-25 16:33:42 +0000 (Sun, 25 Oct 2009) | 1 line

remove unused member from KgpgEditor class
------------------------------------------------------------------------
r1040113 | dakon | 2009-10-25 16:38:23 +0000 (Sun, 25 Oct 2009) | 1 line

remove some debries
------------------------------------------------------------------------
r1040119 | dakon | 2009-10-25 17:08:03 +0000 (Sun, 25 Oct 2009) | 4 lines

add missing stupid KDE4 port

backport of r1040118

------------------------------------------------------------------------
r1040127 | dakon | 2009-10-25 17:19:19 +0000 (Sun, 25 Oct 2009) | 4 lines

workaround for NULL-deref

BUG:203497

------------------------------------------------------------------------
r1040133 | rkcosta | 2009-10-25 18:03:00 +0000 (Sun, 25 Oct 2009) | 6 lines

Backport r1003879.

Do not set limits on infopanel's size.

CCBUG: 169368

------------------------------------------------------------------------
r1040136 | rkcosta | 2009-10-25 18:07:41 +0000 (Sun, 25 Oct 2009) | 6 lines

Backport r1003880.

Make the icon in infopanel 64x64 instead of 128x128

CCBUG: 169368

------------------------------------------------------------------------
r1040137 | rkcosta | 2009-10-25 18:09:27 +0000 (Sun, 25 Oct 2009) | 6 lines

Backport r1003907.

Set the default icon when selecting multiple files

CCBUG: 169368

------------------------------------------------------------------------
r1040138 | rkcosta | 2009-10-25 18:12:08 +0000 (Sun, 25 Oct 2009) | 7 lines

Backport r1003908.

When loading a remote file, show the remote name, not the name of the
temporary file that was downloaded.

CCBUG: 169368

------------------------------------------------------------------------
r1040141 | rkcosta | 2009-10-25 18:15:20 +0000 (Sun, 25 Oct 2009) | 11 lines

Backport r1039962.

Use a KSqueezedTextLabel for the file name in infopanel.

This way the panel won't be resized to fit the file name, which can be
quite large.

Thanks to Ivo Anjo for the hint.

CCBUG: 169368

------------------------------------------------------------------------
r1040201 | dakon | 2009-10-25 20:19:03 +0000 (Sun, 25 Oct 2009) | 10 lines

properly encode what is sent do gpg processes

-don't destroy UTF8 encoding
-use QByteArray::number() instead of QString::number().toAscii()
-remove bogus Q_UNUSED()

BUG:211399

backport of r1040200

------------------------------------------------------------------------
r1040241 | rkcosta | 2009-10-25 21:37:55 +0000 (Sun, 25 Oct 2009) | 6 lines

Backport r1040225.

Correctly sort the "Compressed Size" column.

CCBUG: 181746

------------------------------------------------------------------------
r1040270 | rkcosta | 2009-10-25 22:36:45 +0000 (Sun, 25 Oct 2009) | 13 lines

Backport r1040267.

Fix sorting in the GUI.

All columns now work fine as well as flat archives (in which there
is no directory structure).

Thanks to Michal Sciubidlo <michal.sciubidlo@gmail.com> for the
patch!

BUG: 181746
CCMAIL: michal.sciubidlo@gmail.com

------------------------------------------------------------------------
r1040803 | rkcosta | 2009-10-26 21:55:39 +0000 (Mon, 26 Oct 2009) | 7 lines

Backport r1040802.

Namespace our Part class.

Part is too common a class name, we'd better namespece it to
avoid nasty naming conflicts.

------------------------------------------------------------------------
r1042074 | scripty | 2009-10-29 04:20:37 +0000 (Thu, 29 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1042242 | dakon | 2009-10-29 10:59:57 +0000 (Thu, 29 Oct 2009) | 1 line

bump KGpg version number to 2.2.2 for KDE 4.3.3
------------------------------------------------------------------------
