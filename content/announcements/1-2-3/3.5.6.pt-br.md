---
aliases:
- ../announce-3.5.6
custom_about: true
custom_contact: true
date: '2007-01-25'
title: An&uacute;ncio de Lan&ccedil;amento do KDE 3.5.6
---

<h3 align="center">
   O Projeto KDE Lan&ccedil;a a Sexta Vers&atilde;o de
   Tradu&ccedil;&otilde;es e Servi&ccedil;os do Ambiente de
   Trabalho Livre
</h3>

<p align="justify">
  O KDE 3.5.6 disponibiliza tradu&ccedil;&otilde;es em 65
  l&iacute;nguas, melhorias no motor de
  visualiza&ccedil;&atilde;o de HTML (o KHTML) e&nbsp;outras
  aplica&ccedil;&otilde;es.
</p>

<p align="justify">
   O Projeto KDE anunciou hoje a
   disponibilidade imediata do KDE 3.5.6, uma vers&atilde;o de
   manuten&ccedil;&atilde;o da &uacute;ltima
   gera&ccedil;&atilde;o do ambiente de trabalho poderoso,
   avan&ccedil;ado e livre para o GNU/Linux e outros UNIXes. O KDE
   suporta agora 65 idiomas, tornando-se acess&iacute;vel para ainda
   mais pessoas que a maioria do software propriet&aacute;rio, podendo
   ser estendido facilmente pelas comunidades que desejem contribuir para
   o projeto c&oacute;digo-aberto.
</p>

<p align="justify">
Esta vers&atilde;o inclui
in&uacute;meras corre&ccedil;&otilde;es de defeitos do
KHTML, do <a href="http://kate-editor.org">Kate</a>,
do kicker, do ksysguard e de outras
aplica&ccedil;&otilde;es. Entre as novas funcionalidades
significativas destacam-se o suporte adicional para o compiz como
gerenciador de janelas com o kicker, gerenciamento de sess&atilde;o
das abas de navega&ccedil;&atilde;o do&nbsp;<a
 href="http://akregator.kde.org">Akregator</a>, modelos
para mensagens do KMail e novos menus de sum&aacute;rio para
o&nbsp;<a href="http://kontact.kde.org">Kontact</a>,
tornando mais f&aacute;cil de trabalhar com tarefas e listas
de pend&ecirc;ncias. As tradu&ccedil;&otilde;es tamb&eacute;m
continuam, com as tradu&ccedil;&otilde;es para o&nbsp;<a
 href="http://l10n.kde.org/team-infos.php?teamcode=gl">Galego</a>
quase dobrarem para 78%.
</p>

<p align="justify">
Para uma lista mais detalhada das melhorias desde a vers&atilde;o
KDE 3.5.5, de 11 de outubro de 2006, veja por favor a&nbsp;<a
 href="/announcements/changelogs/changelog3_5_5to3_5_6">lista de altera&ccedil;&otilde;es
do KDE 3.5.6</a>.
</p>

<p align="justify">
  O KDE 3.5.6 inclui um ambiente de trabalho b&aacute;sico e quinze
  outros pacotes (Gestor de Informa&ccedil;&atilde;o Pessoal,
  administra&ccedil;&atilde;o, redes, educa&ccedil;&atilde;o,
  utilit&aacute;rios, multim&iacute;dia, jogos, trabalhos
  gr&aacute;ficos, desenvolvimento web, entre outras coisas). As
  ferramentas e aplica&ccedil;&otilde;es mais importantes est&atilde;o
  dispon&iacute;veis em <strong>65 idiomas</strong>.
</p>

<h4>
  Distribui&ccedil;&otilde;es que Disponibilizam o KDE
</h4>
<p align="justify">
  A maioria das distribui&ccedil;&otilde;es de Linux e dos sistemas
  operacionais UNIX n&atilde;o incorpora imediatamente as
  vers&otilde;es novas do KDE, mas ir&atilde;o integrar os pacotes do
  KDE 3.5.6 nas suas pr&oacute;ximas vers&otilde;es. Veja
  <a href="/distributions">esta lista</a>
  para ver as distribui&ccedil;&otilde;es que oferecem o KDE.
</p>

<h4>
  Instala&ccedil;&atilde;o dos Pacotes Bin&aacute;rios do KDE 3.5.6
</h4>
<p align="justify">
  <em>Criadores de Pacotes</em>.
   Alguns vendedores de sistemas operacionais fornecem gentilmente pacotes
   bin&aacute;rios do KDE 3.5.6 para algumas vers&otilde;es das suas distribui&ccedil;&otilde;es e, 
   em outros casos, existem comunidades volunt&aacute;rias que tamb&eacute;m o fazem.
   Alguns desses pacotes bin&aacute;rios est&atilde;o dispon&iacute;veis gratuitamente para
   <em>download</em> no servidor do KDE em 
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.6/">download.kde.org</a>.
  Os pacotes bin&aacute;rios adicionais, assim como as atualiza&ccedil;&otilde;es aos pacotes
  dispon&iacute;veis neste momento, dever&atilde;o estar dispon&iacute;veis nas pr&oacute;ximas
  semanas.
</p>

<p align="justify">
  <a id="package_locations"><em>Localiza&ccedil;&otilde;es dos Pacotes</em></a>.
  Para uma lista atualizada com os pacotes bin&aacute;rios dispon&iacute;veis, dos
  quais o Projeto KDE tenha sido informado, veja por favor a
  <a href="/info/1-2-3/3.5.6">P&aacute;gina de Informa&ccedil;&otilde;es do KDE 3.5.6</a>.
</p>

<h4>
  Compila&ccedil;&atilde;o do KDE 3.5.6
</h4>
<p align="justify">
  <a id="source_code"></a><em>C&oacute;digo-Fonte</em>.  O
  c&oacute;digo-fonte completo do KDE 3.5.6 pode
  ser <a href="http://download.kde.org/stable/3.5.6/src/">baixado
  livremente</a>. As instru&ccedil;&otilde;es de
  compila&ccedil;&atilde;o e instala&ccedil;&atilde;o do KDE 3.5.6
  est&atilde;o dispon&iacute;veis na
  <a href="/info/1-2-3/3.5.6">P&aacute;gina de Informa&ccedil;&atilde;o do KDE 3.5.6</a>.
</p>

<h4>
  Suporte ao KDE
</h4>
<p align="justify">
O KDE &eacute; um projeto de <a href="http://www.gnu.org/philosophy/free-sw.html">'Software' Livre</a> que existe e cresce somente com a ajuda dos v&aacute;rios volunt&aacute;rios
que doam o seu tempo e esfor&ccedil;o. O KDE est&aacute; sempre &agrave; procura de novos
volunt&aacute;rios e colaboradores que possam ajudar programando, corrigindo ou
relatando erros, escrevendo alguma documenta&ccedil;&atilde;o, traduzindo, promovendo,
doando dinheiro, etc. Todas as contribui&ccedil;&otilde;es s&atilde;o bem-vindas e ansiosamente
aceitas. Para mais informa&ccedil;&otilde;es, leia por favor a <a href="/community/donations/">p&aacute;gina de Suporte do KDE</a>. </p>

<p align="justify">
Ficamos &agrave; espera de not&iacute;cias suas em breve!
</p>

<h4>
  Sobre o KDE
</h4>
<p align="justify">
  O KDE &eacute; um projeto <a href="/community/awards/">premiado</a>, independente e com
  <a href="/people/">centenas</a> de programadores, tradutores,
  artistas e outros profissionais que colaboram atrav&eacute;s da
  Internet para criar e distribuir gratuitamente um ambiente de
  trabalho e de escrit&oacute;rio sofisticado, personalizado e
  est&aacute;vel. Este utiliza uma arquitetura flex&iacute;vel,
  baseada em componentes, transpar&ecirc;ncia a redes e oferece uma
  &oacute;tima plataforma de desenvolvimento.</p>

<p align="justify">
  O KDE oferece um ambiente de trabalho est&aacute;vel e maduro, que inclui um 
  navegador Web magn&iacute;fico 
  (o <a href="http://konqueror.kde.org/">Konqueror</a>), um pacote de gest&atilde;o
  da informa&ccedil;&atilde;o pessoal (o <a href="http://kontact.org/">Kontact</a>),
  um pacote de escrit&oacute;rio completo (o 
  <a href="http://www.koffice.org/">KOffice</a>), um grande conjunto de
  aplica&ccedil;&otilde;es de rede e utilit&aacute;rios, assim como um ambiente de desenvolvimento
  eficiente e intuitivo, com o excelente IDE 
  <a href="http://www.kdevelop.org/">KDevelop</a>.</p>

<p align="justify">
  O KDE &eacute; a prova real que o modelo de desenvolvimento de <em>software de c&oacute;digo-aberto
  </em> de estilo "bazar" pode criar tecnologias, com um rendimento
  superior &agrave;s complexas aplica&ccedil;&otilde;es propriet&aacute;rias existentes.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Avisos de Marcas Registadas.</em>
  KDE<sup>&#174;</sup> e o log&oacute;tipo do K Desktop Environment<sup>&#174;</sup>
  s&atilde;o marcas registadas da KDE e.V.

  Linux &eacute; uma marca registada de Linus Torvalds.

  UNIX &eacute; uma marca registada do The Open Group, nos Estados
  Unidos e em outros pa&iacute;ses.

  Todas as outras marcas registadas e direitos autorais referenciados neste
  an&uacute;ncio s&atilde;o propriedades dos seus respectivos donos.
  </font>
</p>

<hr  />

<h4>Contatos de Imprensa</h4>
<table cellpadding="10" align="center"><tr valign="top">
<td>

<b>&Aacute;frica</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Telefone: +264 - 61 - 24 92 49<br />
<a href="&#109;a&#105;l&#116;o:&#105;&#110;fo-&#0097;&#0102;r&#105;&#99;a&#x40;k&#100;e.&#111;&#x72;g">info-africa kde.org</a><br />
</td>

<td>
<b>&Aacute;sia e &Iacute;ndia</b><br />
     Pradeepto Bhattacharya<br/>
     A-4 Sonal Coop. Hsg. Society<br/>
     Plot-4, Sector-3,<br/>
     New Panvel,<br/>
     Maharashtra.<br/>
     India 410206<br/>
     Telefone : +91-9821033168<br/>
<a href="ma&#0105;&#108;to&#00058;inf&#00111;-&#97;&#115;&#x69;a&#x40;kde.or&#x67;">info-asia kde.org</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europa</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Telefone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#x61;il&#0116;o&#x3a;i&#x6e;fo-&#00101;&#00117;rope&#64;k&#x64;&#x65;&#00046;o&#x72;&#00103;">info-europe kde.org</a>
</td>

<td>
<b>Am&eacute;rica do Norte</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Telefone: (416)-925-4030 <br />
<a href="&#109;ai&#x6c;&#x74;&#x6f;&#0058;i&#x6e;&#0102;o&#0045;no&#0114;t&#104;&#0097;m&#x65;&#x72;i&#x63;&#x61;&#x40;k&#x64;e&#46;&#0111;&#x72;&#x67;">info-northamerica kde.org</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Telefone: (+61)402 346684<br />
<a href="&#109;&#x61;&#x69;&#x6c;&#x74;o:&#105;&#x6e;fo&#45;&#x6f;c&#101;&#x61;&#110;ia&#064;kde&#00046;org">info-oceania kde.org</a><br />
</td>

<td>
<b>Am&eacute;rica do Sul</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Telefone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="ma&#105;&#x6c;&#116;&#x6f;&#x3a;&#0105;&#110;&#102;&#x6f;-&#00115;&#111;ut&#104;&#97;&#x6d;e&#0114;&#x69;ca&#0064;&#107;d&#x65;.&#111;r&#x67;">info-southamerica kde.org</a><br />
</td>
