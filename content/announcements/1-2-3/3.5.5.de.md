---
aliases:
- ../announce-3.5.5
- ../announce-3.5.5.de
custom_about: true
custom_contact: true
date: '2006-10-11'
title: KDE 3.5.5-Pressemitteilung
---

<h3 align="center">
   KDE-Projekt gibt das fünfte Übersetzungs- und Wartungsrelease der führenden, freien Arbeitsumgebung frei.
</h3>

<p align="justify">
  KDE 3.5.5 bringt Übersetzungen und Lokalisierungen in 65 verschiedene Sprachen, Verbesserungen der Instant-Messaging-Anwendung und der HTML-Rendering-Engine (KHTML) mit sich.
</p>

<p align="justify">
  Das <a href="/">KDE-  Projekt</a> gab heute die ab sofortige Verfügbarkeit von KDE Version 3.5.5, einem Wartungsrelease für die letzte Generation der am fortgeschrittensten und umfangreichsten, <em>freien</em>, Arbeitsumgebung für GNU/Linux und andere unixoide Betriebssysteme bekannt. KDE unterstützt nun 65 verschiedene Sprachen, was es für viel mehr Menschen zugänglich macht, als es die meiste kommerzielle Software ist. Außerdem kann KDE jederzeit erweitert werden um noch weitere Sprachen zu unterstützen, falls sich Freiwillige finden die mit der Übersetzung in eine noch nicht verfübare Sprache zum Projekt beitragen möchten.
</p>

<p align="justify">
  Bemerkenswerte Neuerungen und Änderungen sind:
</p>

<ul>
  <li>
    <a href="http://kopete.kde.org/">Kopete</a> Version 0.12.3 ersetzt Version 0.11.3 in KDE 3.5.5, sie bietet unter anderem Unterstützung für <a href="http://www.adiumx.com/">Adium</a>-Designs, Geschwindigkeitsverbesserungen und bessere Unterstützung des <a href="http://messenger.yahoo.com/">Yahoo!</a>- und <a href="http://www.jabber.org/">Jabber</a>-Protokolls.
  </li>
  <li>
    kdesu unterstützt jetzt "sudo".
  </li>
  <li>
    Unterstützung von XShape1.1 in KWin (KDE Fenstermanager).
  </li>
  <li>
    Zahlreiche Geschwindigkeitsverbesserungen und behobene Fehler in der HTML-Engine von <a href="http://www.konqueror.org">Konqueror</a> (KHTML).
  </li>
  <li>
    <a href="http://printing.kde.org/">KDEPrint</a> unterstützt nun <a href="http://www.cups.org/">CUPS</a> 1.2.
  </li>
  <li>
    Große Sprünge in der Vollständigkeit gab es bei der
    <a href="http://l10n.kde.org/stats/gui/stable/zh_TW/index.php">Chinesischen (Traditionell)</a>,
    <a href="http://l10n.kde.org/stats/gui/stable/fa/index.php">Farsi</a>,
    <a href="http://l10n.kde.org/stats/gui/stable/km/index.php">Khmer</a>,
    <a href="http://l10n.kde.org/stats/gui/stable/nds/index.php">Plattdeutschen</a>
    und <a href="http://l10n.kde.org/stats/gui/stable/sk/index.php">Slovakischen</a>
    Übersetzung.
  </li>
</ul>

<p align="justify">
  Eine detailierte Liste der Änderungen seit
  <a href="/announcements/announce-3.5.4">KDE Version 3.5.4</a>, veröffentlicht am
  2. August 2006, findet sich in der
  <a href="/announcements/changelogs/changelog3_5_4to3_5_5">Liste der Änderungen in KDE 3.5.5</a>.
</p>

<p align="justify">
  KDE 3.5.5 kommt standardmäßig mit einer einfachen Arbeitsumgebung, sowie fünfzehn weiteren Softwarepaketen (PIM, Administration, Netzwerken, Lernprogramme, Dienstprogramme, Multimedia, Spielen, Grafik, Web-Entwicklung und viele weitere). Die bereits mehrfach ausgezeichneten KDE-Werkzeuge und Programme sind in <strong>65 Sprachen</strong> verfügbar.
</p>

<h4>
  Distributionen die KDE enthalten, bzw. für Ihre Benutzer zur Verfügung stellen
</h4>
<p align="justify">
  Der Großteil der Linux-Distributionen und unixoiden Betriebssysteme werden die KDE-3.5.5 Pakete nicht sofort in ihre Distribution aufnehmen, sondern erst mit deren nächsten Veröffentlichung ausliefern. Werfen Sie einen Blick auf <a href="/distributions">diese Liste</a>, um zu sehen, welche Distributionen KDE enthalten, bzw. für Ihre Benutzer zur Verfügung stellen.
</p>

<h4>
  Binärpakete von KDE 3.5.5 installieren
</h4>
<p align="justify">
  <em>Paketersteller</em>.
  Einige Betriebssystemhersteller waren so freundlich, Binärpakete von KDE 3.5.5 für einige ihrer Distributionen bereitzustellen. Wo dies
nicht der Fall war, wurden die Pakete von freiwilligen Helfern
erstellt. Einige dieser Pakete können gratis vom KDE-FTP-Server, erreichbar unter der Adresse <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.5/">download.kde.org</a> heruntergeladen werden. Zusätzliche Binärpakete, sowie Paket-Aktualisierungen werden im Laufe der nächsten Wochen verfügbar sein.
</p>

<p align="justify">
  <a name="package_locations"><em>Verfügbare Pakete</em></a>.
  Für eine List der verfügbaren Binärpakete über die das KDE Projekt informiert wurde, siehe bitte die <a href="/info/1-2-3/3.5.5">Informationsseite zu KDE 3.5.5</a>.
</p>

<h4>
  KDE 3.5.5 kompilieren
</h4>
<p align="justify">
  <em>Quelltext</em>.
  Der vollständige Quelltext von KDE 3.5.5 kann <a href="http://download.kde.org/stable/3.5.5/src/">frei heruntergeladen</a> werden. Hinweise zum Kompilieren und Installieren von KDE 3.5.5 finden sich auf der <a href="/info/1-2-3/3.5.5">KDE
  3.5.5 Informationsseite</a>.
</p>

<h4>
  KDE Unterstützen
</h4>
<p align="justify">
  KDE ist ein <a href="http://www.gnu.org/philosophy/free-sw.html">freies Softwareprojekt</a> das nur aufgrund der Hilfe zahlreicher Freiwilliger, die ihre Zeit und Arbeit beisteuern, existiert und wächst. KDE ist immer auf der Suche nach neuen Freiwilligen und Helfern, ganz egal ob Sie Code schreiben, Fehler beheben oder melden, Dokumentationen schreiben, Übersetzungen anfertigen, Öffentlichkeitsarbeit leisten, Geld spenden, etc. Jede Hilfe ist herzlich willkommen, und wird gerne angenommen. Bitte lesen Sie die Seite <a href="/community/donations/">"KDE unterstützen"</a> für nähere Informationen.
</p>

<p align="justify">
Wir freuen uns bald von Ihnen zu hören!
</p>

<h4>
  Über KDE
</h4>
<p align="justify">
  KDE ist ein mehrfach <a href="/community/awards/">ausgezeichnetes</a>, unabhängiges Projekt <a href="/people/">hunderter</a> Entwickler, Übersetzer, Künstler und anderen Mitarbeitern auf der ganzen Welt, die über das Internet zusammenarbeiten um eine frei verfügbare, stabile, an die persönlichen Bedürfnisse anpassbare, flexible, komponentenbasierte, netzwerktransparente Arbeits- und Büroumgebung, sowie einen herausragende Plattform für Entwickler zu erstellen und zu verbreiten.
</p>

<p align="justify">
  KDE bietet eine stabile, durchdachte Arbeitsumgebung die unter anderem einen  modernen Webbrowser (<a href="http://konqueror.kde.org/">Konqueror</a>), eine PIM-Suite (<a href="http://kontact.org/">Kontact</a>), eine vollständige Office-Suite <a href="http://www.koffice.org/">KOffice</a>), eine große Auswahl an Netzwerkanwendungen und Werkzeugen, sowie eine effiziente und intuitive Entwicklungsumgebung (<a href="http://www.kdevelop.org/">KDevelop</a>) enthält.
</p>

<p align="justify">
  KDE ist "der lebende Beweis", dass das "Basar-artige" Entwicklungsmodell freier Software erstklassige Technologien hervorbringen kann, welche gleichwertig, wenn nicht sogar überlegen gegenüber der komplexesten, kommerziellen Software sind.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Markenrechtshinweise.</em>
  KDE<sup>&#174;</sup> und das K Desktop Environment<sup>&#174;</sup> sind eingetragene Warenzeichen des KDE e.V.

Linux ist ein eingetragenes Warenzeichen von Linus Torvalds.

UNIX ist ein eingetragenes Warenzeichnen von The Open Group in den Vereinigten Staaten und anderen Ländern.

Alle anderen in dieser Pressemitteilungen Warenzeichen sind Eigentum der jeweiligen Besitzer.
</font>

</p>

<hr />

<h4>Pressekontaktdaten</h4>
<table cellpadding="10" align="center"><tr valign="top">
<td>

Afrika<br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="&#109;a&#105;l&#116;o:&#105;&#110;fo-&#0097;&#0102;r&#105;&#99;a&#x40;k&#100;e.&#111;&#x72;g">info-africa kde.org</a><br />

</td>

<td>
Asien und Indien<br />
  Pradeepto Bhattacharya<br/>
  A-4 Sonal Coop. Hsg. Society<br/>
  Plot-4, Sector-3,<br/>
  New Panvel,<br/>
  Maharashtra.<br/>
  India 410206<br/>
  Phone : +91-9821033168<br/>
<a href="ma&#0105;&#108;to&#00058;inf&#00111;-&#97;&#115;&#x69;a&#x40;kde.or&#x67;">info-asia kde.org</a>
</td>

</tr>
<tr valign="top">

<td>
Europa<br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#x61;il&#0116;o&#x3a;i&#x6e;fo-&#00101;&#00117;rope&#64;k&#x64;&#x65;&#00046;o&#x72;&#00103;">info-europe kde.org</a>
</td>

<td>
Nordamerika<br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="&#109;ai&#x6c;&#x74;&#x6f;&#0058;i&#x6e;&#0102;o&#0045;no&#0114;t&#104;&#0097;m&#x65;&#x72;i&#x63;&#x61;&#x40;k&#x64;e&#46;&#0111;&#x72;&#x67;">info-northamerica kde.org</a><br />
</td>

</tr>

<tr>
<td>
Ozeanien<br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="&#109;&#x61;&#x69;&#x6c;&#x74;o:&#105;&#x6e;fo&#45;&#x6f;c&#101;&#x61;&#110;ia&#064;kde&#00046;org">info-oceania kde.org</a><br />
</td>

<td>
Südamerika<br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="ma&#105;&#x6c;&#116;&#x6f;&#x3a;&#0105;&#110;&#102;&#x6f;-&#00115;&#111;ut&#104;&#97;&#x6d;e&#0114;&#x69;ca&#0064;&#107;d&#x65;.&#111;r&#x67;">info-southamerica kde.org</a><br />
</td>

</tr></table>
