---
aliases:
- ../announce-3.5.4
custom_about: true
custom_contact: true
date: '2006-08-02'
title: KDE 3.5.4 tillkännagivande
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE projektet släpper fjärde översättnings- och serviceversionen av den ledande fria skrivbordsmiljön.
</h3>

<p align="justify">
  <strong>
    KDE 3.5.4 är översatt till 65 språk, förbättrad stöd för pluggbara enheter och förbättringar i renderingsmotorn för HTML (KHTML).
  </strong>
</p>

<p align="justify">
  
  <a href="/">KDE
  projektet</a> illkännager idag KDE 3.5.4, en serviceversion för senaste generationen
  av den mest avancerade och kraftfulla skrivbordsmiljön för GNU/Linux och andra UNIX.
  KDE innehåller nu översättningar till 65 språk, vilket gör den tillgänglig för fler människor
  än de flesta proprietära mjukvaror och kan enkelt utökas till fler språk av de som vill bidra
  till öppen källkodsprojektet.
</p>

<p align="justify">
  De viktigaste förändringarna är:
</p>

<ul>
  <li>Förbättrat stöd för pluggbara enheter under Linux (användare kan nu montera alla enheter som stöds av
    <a href="http://www.freedesktop.org/wiki/Software/hal">FreeDesktops HAL</a> och kontrollera hur det görs.
  </li>
  <li>Optimeringar i <a href="http://konsole.kde.org">Konsole</a> och <a href="http://kate-editor.org">Kate</a></li>
  <li>Möjlighet för flera helgdagar på samma datum i <a href="http://korganizer.kde.org">KOrganizer</a></li>
  <li>Ett stort antal buggrättningar i <a href="http://www.konqueror.org">Konqueror</a>s HTML motor, KHTML</li>
  <li>Dialogen för att skicka klient-SSL-certifikat har blivit mer användbar.</li>
  <li><a href="http://knetworkconf.sourceforge.net">KNetworkConf</a> stöder nu Fedora Core 5 och hanterar WEP-nycklar bättre.</li>
</ul>

<p align="justify">
  Mer än 10 nya funktioner har lagts till och över 100 buggar har blivit rättade.
  För en mer detaljerad lista över förbättringarna sedan KDE 3.5.3 från mars 2006 se
  <a href="/announcements/changelogs/changelog3_5_3to3_5_4">KDE 3.5.4 Changelog</a>.
</p>

<p align="justify">
  KDE 3.5.4 levereras med en grundläggande skrivbordsmiljö och femton övriga paket
  (PIM, administration,  network, edutainment, utilities, multimedia, games,
  artwork, web development and more). KDE's prisbelönta verktyg och 
  applikationer är tillgängliga på <strong>65 språk</strong>.
</p>

<h4>
  OM KDE-PROJEKTET
</h4>
<p align="justify">
  KDE-projektet består av hundratals utvecklare, översättare, grafiker och andra bidragsgivare över hela världen som samarbetar via Internet. Gemenskapen skapar och distribuerar fritt en stabil, integrerad och fri skrivbords- och kontorsmiljö. KDE tillhandahåller en flexibel, komponentbaserad, nätverkstransparent arkitektur och kraftfulla utvecklingsverktyg, som erbjuder en enastående utvecklingsplattform.</p>

<p align="justify">
  KDE, som är baserat på Qt-teknologi från Trolltech, är ett levande bevis på att utvecklingsmodellen av programvara med öppen källkod enligt "basarstilen" kan resultera i förstklassig teknologi likvärdig eller överlägsen även den mest komplexa kommersiella programvara.</p>


<hr  />

<h4>PRESSKONTAKTER FÖR YTTERLIGARE INFORMATION</h4>
<table cellpadding="10" align="center"><tr valign="top">
<td>

<b>Afrika</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="&#109;a&#105;l&#116;o:&#105;&#110;fo-&#0097;&#0102;r&#105;&#99;a&#x40;k&#100;e.&#111;&#x72;g">info-africa kde.org</a><br />
</td>

<td>
<b>Asien</b><br />
     Pradeepto Bhattacharya<br/>
     A-4 Sonal Coop. Hsg. Society<br/>
     Plot-4, Sector-3,<br/>
     New Panvel,<br/>
     Maharashtra.<br/>
     India 410206<br/>
     Phone : +91-9821033168<br/>
<a href="ma&#0105;&#108;to&#00058;inf&#00111;-&#97;&#115;&#x69;a&#x40;kde.or&#x67;">info-asia kde.org</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europa</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#x61;il&#0116;o&#x3a;i&#x6e;fo-&#00101;&#00117;rope&#64;k&#x64;&#x65;&#00046;o&#x72;&#00103;">info-europe kde.org</a>
</td>

<td>
<b>Nordamerika</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="&#109;ai&#x6c;&#x74;&#x6f;&#0058;i&#x6e;&#0102;o&#0045;no&#0114;t&#104;&#0097;m&#x65;&#x72;i&#x63;&#x61;&#x40;k&#x64;e&#46;&#0111;&#x72;&#x67;">info-northamerica kde.org</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceanien</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="&#109;&#x61;&#x69;&#x6c;&#x74;o:&#105;&#x6e;fo&#45;&#x6f;c&#101;&#x61;&#110;ia&#064;kde&#00046;org">info-oceania kde.org</a><br />
</td>

<td>
<b>Sydamerika</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="ma&#105;&#x6c;&#116;&#x6f;&#x3a;&#0105;&#110;&#102;&#x6f;-&#00115;&#111;ut&#104;&#97;&#x6d;e&#0114;&#x69;ca&#0064;&#107;d&#x65;.&#111;r&#x67;">info-southamerica kde.org</a><br />
</td>

</tr></table>
