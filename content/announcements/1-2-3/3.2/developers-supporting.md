---
custom_about: true
custom_contact: true
hidden: true
title: 'The Developer''s Perspective: Supporting KDE'
---

Being a software project the KDE project naturally has a steady need of developers. If you are a programmer, preferably with knowledge of at least C++ and Qt, there are many ways to support KDE. In addition to <a href="../users-supporting">all the things that any user can do</a> you may be able to contribute in one or more of the following ways:

### <a name="Patches">Patches</a>

KDE's Bug Tracking System is a good starting point for any programmer looking for relatively small works. Especially check out the "most hated bugs" and "most wanted features" links there in the "Bug Hunting Tools" section. Small bugs and feature requests give the opportunity to write small patches and posting them in the respective reports. There are also several web-based systems giving easy access to the necessary source code.

[ <a href="http://bugs.kde.org/">Visit KDE's Bug Tracking System</a> | <a href="http://lxr.kde.org/">Visit KDE Source Cross Reference</a> | <a href="http://webcvs.kde.org/">Visit KDE CVS Repository</a> ]

### <a name="Code">Code</a>

When you want to do some serious programming in KDE you can rely on a huge library of information. KDE Developer's Corner is the traditional place for KDE development related information.

[ <a href="http://developer.kde.org/documentation/index.html">Visit KDE Developer's Corner Documentation</a> ]

### <a name="Maintenance">Maintenance</a>

There is a list of KDE applications open for maintainance at the Open Job page for those who are interested and have the necessary skills.

[ <a href="http://www.kde.org/jobs/open.php">Visit Open Jobs</a> ]

<table style="border: solid 1px;" align="center" cellpadding="6" cellspacing="0">
<tr>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">Users</th>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">Developers</th>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">About KDE</th>
</tr>
<tr>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap">
<a href="../users-whykde">Why KDE?</a><br/>
<a href="../users-deploying">Deploying KDE</a><br/>
<a href="../users-supporting">Supporting KDE</a>
</td>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap">
<a href="../developers-whykde">Why KDE?</a><br/>
<a href="../developers-developing">Developing With KDE</a><br/>
<a href="../developers-supporting">Supporting KDE</a>
</td>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap"><a href="/">The KDE Project</a><br/>
<a href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a><br/>

</td>
</tr>
</table>