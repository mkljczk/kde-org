---
aliases:
- ../../kde-frameworks-5.37.0
date: 2017-08-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---

### New framework: kirigami, a set of QtQuick plugins to build user interfaces based on the KDE UX guidelines

### Breeze Icons

- update .h and .h++ colors (bug 376680)
- remove ktorrent small monochrome icon (bug 381370)
- bookmarks is an action icon not a folder icon (bug 381383)
- update utilities-system-monitor (bug 381420)

### Extra CMake Modules

- Add --gradle to androiddeployqt
- Fix install apk target
- Fix usage of query_qmake: differ between calls expecting qmake or not
- Add API dox for KDEInstallDirs' KDE_INSTALL_USE_QT_SYS_PATHS
- Add a metainfo.yaml to make ECM a proper framework
- Android: scan for qml files in the source dir, not in the install dir

### KActivities

- emit runningActivityListChanged on activity creation

### KDE Doxygen Tools

- Escape HTML from search query

### KArchive

- Add Conan files, as a first experiment to Conan support

### KConfig

- Allow to build KConfig without Qt5Gui
- Standard shortcuts: use Ctrl+PageUp/PageDown for prev/next tab

### KCoreAddons

- Remove unused init() declaration from K_PLUGIN_FACTORY_DECLARATION_WITH_BASEFACTORY_SKEL
- New spdx API on KAboutLicense to get SPDX license expressions
- kdirwatch: Avoid potential crash if d-ptr destroyed before KDirWatch (bug 381583)
- Fix display of formatDuration with rounding (bug 382069)

### KDeclarative

- fix plasmashell unsetting QSG_RENDER_LOOP

### KDELibs 4 Support

- Fix 'Deprecated hint for KUrl::path() is wrong on Windows' (bug 382242)
- Update kdelibs4support to use the target based support provided by kdewin
- Mark constructors as deprecated too
- Sync KDE4Defaults.cmake from kdelibs

### KDesignerPlugin

- Add support for the new widget kpasswordlineedit

### KHTML

- Support SVG too (bug 355872)

### KI18n

- Allow loading i18n catalogs from arbitrary locations
- Make sure that the tsfiles target is generated

### KIdleTime

- Only require Qt5X11Extras when we actually need it

### KInit

- Use proper feature flag to include kill(2)

### KIO

- Add new method urlSelectionRequested to KUrlNavigator
- KUrlNavigator: expose the KUrlNavigatorButton that received a drop event
- Stash without asking the user with a Copy/Cancel popup
- Ensure KDirLister updates items whose target URL has changed (bug 382341)
- Make advanced options of "open with" dialog collabsible and hidden by default (bug 359233)

### KNewStuff

- Give a parent to KMoreToolsMenuFactory menus
- When requesting from the cache, report all entries at bulk

### KPackage Framework

- kpackagetool now can output appstream data to a file
- adopt new KAboutLicense::spdx

### KParts

- Reset url in closeUrl()
- Add template for a simple kpart-based application
- Drop usage of KDE_DEFAULT_WINDOWFLAGS

### KTextEditor

- Handle fine-grained wheel event in zooming
- Add template for a ktexteditor plugin
- copy permissions from original file on save copy (bug 377373)
- perhaps avoid stringbuild crash (bug 339627)
- fix problem with * adding for lines outside of comments (bug 360456)
- fix save as copy, it missed to allow overwriting the destination file (bug 368145)
- Command 'set-highlight': Join args with space
- fix crash on view destruction because of non-deterministic cleanup of objects
- Emit signals from icon border when no mark was clicked
- Fix crash in vi input mode (sequence: "o" "Esc" "O" "Esc" ".") (bug 377852)
- Use mutually exclusive group in Default Mark Type

### KUnitConversion

- Mark MPa and PSI as common units

### KWallet Framework

- Use CMAKE_INSTALL_BINDIR for dbus service generation

### KWayland

- Destroy all kwayland objects created by registry when it is destroyed
- Emit connectionDied if the QPA is destroyed
- [client] Track all created ConnectionThreads and add API to access them
- [server] Send text input leave if focused surface gets unbound
- [server] Send pointer leave if focused surface gets unbound
- [client] Properly track enteredSurface in Keyboard
- [server] Send keyboard leave when client destroys the focused surface (bug 382280)
- check Buffer validity (bug 381953)

### KWidgetsAddons

- Extract lineedit password widget =&gt; new class KPasswordLineEdit
- Fixed a crash when searching with accessibility support enabled (bug 374933)
- [KPageListViewDelegate] Pass widget to drawPrimitive in drawFocus

### KWindowSystem

- Remove header reliance on QWidget

### KXMLGUI

- Drop usage of KDE_DEFAULT_WINDOWFLAGS

### NetworkManagerQt

- Adding support to ipv*.route-metric
- Fix undefined NM_SETTING_WIRELESS_POWERSAVE_FOO enums (bug 382051)

### Plasma Framework

- [Containment Interface] always emit contextualActionsAboutToShow for containment
- Treat Button/ToolButton labels as plaintext
- Don't perform wayland specific fixes when on X (bug 381130)
- Add KF5WindowSystem to link interface
- Declare AppManager.js as pragma library
- [PlasmaComponents] Remove Config.js
- default to plain text for labels
- Load translations from KPackage files if bundled (bug 374825)
- [PlasmaComponents Menu] Don't crash on null action
- [Plasma Dialog] Fix flag conditions
- update akregator system tray icon (bug 379861)
- [Containment Interface] Keep containment in RequiresAttentionStatus while context menu is open (bug 351823)
- Fix tab bar layout key handling in RTL (bug 379894)

### Sonnet

- Allow to build Sonnet without Qt5Widgets
- cmake: rewrite FindHUNSPELL.cmake to use pkg-config

### Syntax Highlighting

- Allow to build KSyntaxHighlighter without Qt5Gui
- Add cross-compilation support for the highlighting indexer
- Themes: Remove all unused metadata (license, author, read-only)
- Theme: Remove license and author fields
- Theme: Derive read-only flag from file on disk
- Add syntax highlighting for YANG data modeling language
- PHP: Add PHP 7 keywords (bug 356383)
- PHP: Clean up PHP 5 information
- fix gnuplot, make leading/trailing spaces fatal
- fix 'else if' detection, we need to switch context, add extra rule
- indexer checks for leading/trailing whitespaces in XML highlighting
- Doxygen: Add Doxyfile highlighting
- add missing standard types to C highlighting and update to C11 (bug 367798)
- Q_PI D =&gt; Q_PID
- PHP: Improve highlighting of variables in curly braces in double quotes (bug 382527)
- Add PowerShell highlighting
- Haskell: Add file extension .hs-boot (bootstrap module) (bug 354629)
- Fix replaceCaptures() to work with more than 9 captures
- Ruby: Use WordDetect instead of StringDetect for full word matching
- Fix incorrect highlighting for BEGIN and END in words such as "EXTENDED" (bug 350709)
- PHP: Remove mime_content_type() from list of deprecated functions (bug 371973)
- XML: Add XBEL extension/mimetype to xml highlighting (bug 374573)
- Bash: Fix incorrect highlighting for command options (bug 375245)
- Perl: Fix heredoc highlighting with leading spaces in the delimiter (bug 379298)
- Update SQL (Oracle) syntax file (bug 368755)
- C++: Fix '-' is not a part of UDL String (bug 380408)
- C++: printf format specifies: add 'n' and 'p', remove 'P' (bug 380409)
- C++: Fix char value have the color of the strings (bug 380489)
- VHDL: Fix highlighting error when using brackets and attributes (bug 368897)
- zsh highlighting: Fix math expression in a substring expression (bug 380229)
- JavaScript Highlighting: Add support for E4X xml extension (bug 373713)
- Remove "*.conf" extension rule
- Pug/Jade syntax

### ThreadWeaver

- Add missing export to QueueSignals

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.
