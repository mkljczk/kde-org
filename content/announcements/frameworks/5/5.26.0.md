---
aliases:
- ../../kde-frameworks-5.26.0
date: 2016-09-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---

### Attica

- Add Qt5Network as a public dependency

### BluezQt

- Fix include dir in pri file

### Breeze Icons

- Add missing namespace prefix definitions
- Check SVG icons for wellformedness
- Fix all edit-clear-location-ltr icons (bug 366519)
- add kwin effect icon support
- rename caps-on in input-caps-on
- add caps icons for text input
- add some gnome specific icons from Sadi58
- add app icons from gnastyle
- Dolphin, Konsole and Umbrello icons optimized for 16px, 22px, 32px
- Updated VLC icon for 22px, 32px and 48px
- Added app icon for Subtitle Composer
- Fix Kleopatra new icon
- Added app icon for Kleopatra
- Added icons for Wine and Wine-qt
- fix presentation word bug thanks Sadi58 (bug 358495)
- add system-log-out icon in 32px
- add 32px system- icons, remove colored system- icons
- add pidgin, banshee icon support
- remove vlc app icon due to license issue, add new VLC icon (bug 366490)
- Add gthumb icon support
- use HighlightedText for folder icons
- places folder icons are now use stylesheet (highleight color)

### Extra CMake Modules

- ecm_process_po_files_as_qm: Skip fuzzy translations
- The default level for logging categories should be Info rather than Warning
- Document ARGS variable in the create-apk-* targets
- Create a test that validates projects' appstream information

### KDE Doxygen Tools

- Add condition if group's platforms are not defined
- Template: Sort platforms alphabetically

### KCodecs

- Bring from kdelibs the file used to generate kentities.c

### KConfig

- Add Donate entry to KStandardShortcut

### KConfigWidgets

- Add Donate standard action

### KDeclarative

- [kpackagelauncherqml] Assume desktop file name is same as pluginId
- Load QtQuick rendering settings from a config file and set default
- icondialog.cpp - proper compile fix that doesn't shadow m_dialog
- Fix crash when no QApplication is available
- expose translation domain

### KDELibs 4 Support

- Fix Windows compilation error in kstyle.h

### KDocTools

- add paths for config, cache + data to general.entities
- Made up-to-date with the English version
- Add Space and Meta key entities to src/customization/en/user.entities

### KFileMetaData

- Only require Xattr if the operating system is Linux
- Restore Windows build

### KIdleTime

- [xsync] XFlush in simulateUserActivity

### KIO

- KPropertiesDialog: remove warning note from docu, the bug is gone
- [test program] resolve relative paths using QUrl::fromUserInput
- KUrlRequester: fix error box when selecting a file and reopening the file dialog
- Provide a fallback if slaves don't list the . entry (bug 366795)
- Fix creating symlink over "desktop" protocol
- KNewFileMenu: when creating symlinks use KIO::linkAs instead of KIO::link
- KFileWidget: fix double '/' in path
- KUrlRequester: use static connect() syntax, was inconsistent
- KUrlRequester: pass window() as parent for the QFileDialog
- avoid calling connect(null, .....) from KUrlComboRequester

### KNewStuff

- uncompress archives in subfolders
- No longer allow installing to generic data folder because of potential security hole

### KNotification

- Get StatusNotifierWatcher property ProtocolVersion in async way

### Package Framework

- silence contentHash deprecation warnings

### Kross

- Revert "Remove unused KF5 dependencies"

### KTextEditor

- remove accel clash (bug 363738)
- fix email address highlighting in doxygen (bug 363186)
- detect some more json files, like our own projects ;)
- improve mime-type detection (bug 357902)
- Bug 363280 - highlighting: c++: #if 1 #endif #if defined (A) aaa #elif defined (B) bbb #endif (bug 363280)
- Bug 363280 - highlighting: c++: #if 1 #endif #if defined (A) aaa #elif defined (B) bbb #endif
- Bug 351496 - Python folding is not working during initial typing (bug 351496)
- Bug 365171 - Python syntax highlighting: not working correctly for escape sequences (bug 365171)
- Bug 344276 - php nowdoc not folded correctly (bug 344276)
- Bug 359613 - Some CSS3 properties are not supported in syntax highlight (bug 359613)
- Bug 367821 - wineHQ syntax: The section in a reg file isn't highlighted correctly (bug 367821)
- Improve swap file handling if swap directory specified
- Fix crash when reloading documents with auto-wrapped line due to line length limit (bug 366493)
- Fix constant crashes related to the vi command bar (bug 367786)
- Fix: Line numbers in printed documents now starts at 1 (bug 366579)
- Backup Remote Files: Treat mounted files also as remote files
- cleanup logic for searchbar creation
- add highlighting for Magma
- Allows only one level of recursion
- Fix broken swap-file on windows
- Patch: add bitbake support for syntax highlighting engine
- autobrace: look at spellcheck attribute where the character was entered (bug 367539)
- Highlight QMAKE_CFLAGS
- Don't pop out of the main context
- Add some executable names that are commonly used

### KUnitConversion

- Add British "stone" unit of mass

### KWallet Framework

- Move kwallet-query docbook to correct subdir
- Fix wording an -&gt; one

### KWayland

- Make linux/input.h compile time optional

### KWidgetsAddons

- Fix background of non-BMP characters
- Add C octal escaped UTF-8 search
- Make the default KMessageBoxDontAskAgainMemoryStorage save to QSettings

### KXMLGUI

- Port to Donate standard action
- Port away from deprecated authorizeKAction

### Plasma Framework

- fix device icon 22px icon didn't work in the old file
- WindowThumbnail: Do GL calls in the correct thread (bug 368066)
- Make plasma_install_package work with KDE_INSTALL_DIRS_NO_DEPRECATED
- add margin and padding to the start.svgz icon
- fix stylesheet stuff in computer icon
- add computer and laptop icon for kicker (bug 367816)
- Fix cannot assign undefined to double warning in DayDelegate
- fix stylesheed svgz files are not in love with me
- rename the 22px icons to 22-22-x and the 32px icons to x for kicker
- [PlasmaComponents TextField] Don't bother loading icons for unused buttons
- Extra guard in Containment::corona in the special system tray case
- When marking a containment as deleted, also mark all sub-applets as deleted - fixes system tray container configs not being deleted
- Fix Device Notifier icon
- add system-search to system in 32 and 22px size
- add monochrome icons for kicker
- Set colour scheme on system-search icon
- Move system-search into system.svgz
- Fix wrong or missing "X-KDE-ParentApp" in desktop file definitions
- Fix API dox of Plasma::PluginLoader: mixup of applets/dataengine/services/..
- add system-search icon for the sddm theme
- add nepomuk 32px icon
- update touchpad icon for the system tray
- Remove code that can never be executed
- [ContainmentView] Show panels when UI becomes ready
- Don't redeclare property implicitHeight
- use QQuickViewSharedEngine::setTranslationDomain (bug 361513)
- add 22px and 32px plasma breeze icon support
- remove colored system icons and add 32px monochrome ones
- Add an optional reveal password button to TextField
- The standard tooltips are now mirrored when in a right-to-left language
- Performance when changing months in the calendar has been greatly improved

### Sonnet

- Don't lowercase the language names in trigram parsing
- Fix immediate crash on startup due to null plugin pointer
- Handle dictionaries without correct names
- Replace hand-curated list of script-language mappings, use proper names for languages
- Add tool to generate trigrams
- Unbreak language detection a bit
- Use selected language as suggestion for detection
- Use cached spellers in language detection, improve performance a bit
- Improve language detection
- Filter list of suggestions against available dictionaries, remove dupes
- Remember to add the last trigram match
- Check if any of the trigrams actually matched
- Handle multiple languages with same score in trigram matcher
- Don't check for minimum size twice
- Prune list of languages against available languages
- Use same minimum length everywhere in langdet
- Sanity check that the loaded model has the correct amount of trigrams for each language

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.
