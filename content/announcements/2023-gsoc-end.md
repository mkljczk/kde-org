---
description: KDE and Google Summer of Code 2023
authors:
  - SPDX-FileCopyrightText: 2023 Johnny Jazeix <jazeix@gmail.com>
SPDX-License-Identifier: CC-BY-4.0
date: 2023-11-14
hero_image: GSoCPlusKDE.png
images:
  - /announcements/2023-gsoc-end/GSoCPlusKDE.png
scssFiles:
- /scss/2023-gsoc-end.scss
title: "KDE and Google Summer of Code 2023"
draft: false
---
![](GSoCPlusKDE.png)

This year KDE successfully mentored seven projects in the [Google Summer of Code](https://summerofcode.withgoogle.com/) (GSoC). GSoC is a program in which contributors new to Free and Open Source software spend between 175 and 350 hours during from 10  to 22 weeks working on an Open Source project. This post will introduce the projects and contributors for 2023 and their achievements.

### Projects

#### [Merkuro](https://apps.kde.org/merkuro.calendar/)

* [Improving Mail Integration in Merkuro](https://community.kde.org/GSoc/2023/StatusReports/AakarshMJ): [**Aakarsh MJ**](https://invent.kde.org/aakarshmj) worked on the mail integration in Merkuro. Now, we can compose and send mail via Merkuro!

![Mail composition window in Merkuro](aakarsh_merkuro.png)

* [Implement calendar availability](https://community.kde.org/GSoc/2023/StatusReports/AnantVerma): [**InfiniteVerma**](https://invent.kde.org/infiniteverma) worked on calendar availability in Merkuro. This will allow you to specify the hours when you are available and can be invited to meetings and events. This work is still in progress, and hopefully it will be polished and merged soon.
![Current work in progress of calendar availability feature of Merkuro](infiniteverma_merkuro.png)

#### [digiKam](https://www.digikam.org/)

* [Add Automatic Tags Assignment Tools and Improve Face Recognition Engine for digiKam](https://community.kde.org/GSoc/2023/StatusReports/QuocHungTran): [**TRAN Quoc Hung**](https://invent.kde.org/quochungtran) developed a deep learning model that can recognize various categories of objects, scenes, and events in digital photos. The model generates corresponding keywords that can be stored in digiKam’s database and assigned to photos automatically. The MR will be merged after the release of DigiKam 8.2.0.

![Screenshot of digiKam automatic Tags Assignment Tools](gsoc23_TRAN.png)

#### [Krita](http://krita.org/)

* [Improving the Bundle Creator](https://community.kde.org/GSoc/2023/StatusReports/SrirupaDatta): [**Srirupa Datta**](https://invent.kde.org/srirupa) worked on improving and expanding the bundle creator in Krita. Bundles are packages of resources, like brushes or gradients that Krita users can exchange. The work has already been merged and will be part of the next release.

![Bundle editing workflow](srirupa_krita.gif)

{{< section class="itinerary py-5" >}}

#### [KDE Eco](https://eco.kde.org/)

{{< announcements/feature-container-no-fdroid src="KDE-eco.png" >}}

[Measuring Energy Consumption using Remote Lab](https://community.kde.org/GSoc/2023/StatusReports/KaranjotSingh): [**Karanjot Singh**](https://invent.kde.org/drquark) implemented remote access to the KDE Eco energy measurement lab. Although the lab is physically located at the KDAB offices in Berlin, with [KEcoLab](https://invent.kde.org/teams/eco/remote-eco-lab) it is now accessible to KDE and other Free Software developers from anywhere in the world. This was achieved by setting up backend CI/CD integration and automating the energy measurement process, including providing a summary of the results.

{{< /announcements/feature-container-no-fdroid >}}

{{< /section >}}

#### [Tokodon](https://apps.kde.org/tokodon/)

* [Adding moderation tool in Tokodon project under KDE](https://community.kde.org/GSoC/2023/StatusReports/RishiKumar): [**Rishi Kumar**](https://invent.kde.org/keys) worked on implementing the admin APIs in Tokodon. Rishi added various moderation tools that all have been merged and will be available in the next Tokodon release.

![Moderation tool added](rishi_moderation_tool_incognito.png)

#### [Okular](https://okular.kde.org/)

* [Improve Okular For Android](https://community.kde.org/GSoC/2023/StatusReports/ShivoditGill): [**Shivodit**](https://invent.kde.org/shivodayt) worked on improving Okular for Android, bringing in the much-needed font rendering improvement when fonts are not embedded in the PDF file - text was not rendered (the left image), and now it is (the right image). Other improvements have also been carried out during the period, such as improving the "About" page and finding the root cause of a freeze on Android. All of Shivodit's work has been merged in the various repositories.

![Screenshot of Okular application on Android before and after GSoC"](shivodit_okular_android.png)

### Next Steps

The GSoC period is over, but it does not mean the contributions should stop there. Contributors had a fun summer honing their skills within KDE with the support of the community. Hopefully, they felt welcome and will continue to contribute for many years in the future.

