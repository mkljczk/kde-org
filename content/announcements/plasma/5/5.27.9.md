---
date: 2023-10-24
changelog: 5.27.8-5.27.9
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Discover: Fix network cache size for first run. [Commit.](http://commits.kde.org/discover/b98e9209b84473fcaa158d6a2d0710a51e3e43c3) See bug [#464517](https://bugs.kde.org/464517)
+ GTK Config: make it compile with GLib < 2.74. [Commit.](http://commits.kde.org/kde-gtk-config/1b116a53d2566041c58e2ec9b4d8dc8b7004dcdc)
+ Powerdevil: Provide a default action in Suspend session config. [Commit.](http://commits.kde.org/powerdevil/d176188ff9b573a784905cada8824897af86b34b) Fixes bug [#475866](https://bugs.kde.org/475866)
