---
aliases:
- ../../plasma-5.5.2
changelog: 5.5.1-5.5.2
date: 2015-12-22
layout: plasma
figure:
  src: /announcements/plasma/5/5.5.0/plasma-5.5.png
---

{{% i18n_date %}}

Today KDE releases a bugfix update to Plasma 5, versioned 5.5.2. <a href='https://www.kde.org/announcements/plasma-5.5.0.php'>Plasma 5.5</a> was released a couple of weeks ago with many feature refinements and new modules to complete the desktop experience. We are experimenting with a new release schedule with bugfix releases started out frequent and becoming less frequent. The first two bugfix releases come out in the weeks following the initial release and future ones at larger increments.

{{< i18n "annc-plasma-bugfix-worth-1" >}}

{{< i18n "annc-plasma-bugfix-last" >}}

- Task Manager: behave properly in popups again. <a href="http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8e4431a2c05ba12ffb8c771c1ea0c842ec18453b">Commit.</a>
- KWin: fix build with Qt 5.6. <a href="http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=05c542ad602a114751f34ac9d03597f11e95470f">Commit.</a> Code review <a href="https://git.reviewboard.kde.org/r/126234">#126234</a>
- Make initial default panel thickness scale with DPI. <a href="http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=eed2f0206184a5066038a6dea7402f24634fb72e">Commit.</a> Code review <a href="https://git.reviewboard.kde.org/r/126363">#126363</a>
