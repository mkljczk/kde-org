---
version: "5.7.4"
title: "KDE Plasma 5.7.4, Bugfix Release"
errata:
    link: https://community.kde.org/Plasma/5.7_Errata
    name: 5.7 Errata
type: info/plasma5
signer: David Edmundson
signing_fingerprint: F5675605C74E02CF
---

This is a Bugfix release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer.  Details in the
[Plasma 5.7.4 announcement](/announcements/plasma-5.7.4).

