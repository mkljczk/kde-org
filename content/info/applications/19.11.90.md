---
aliases:
- /info/applications-19.11.90
title: "KDE Applications 19.11.90 Info Page"
announcement: /announcements/releases/19.12-rc
layout: applications
signer: Christoph Feck
signing_fingerprint: F23275E4BF10AFC1DF6914A6DBD2CE893E2D1C87
---
