<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">SHA1&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdeaccessibility-4.2.3.tar.bz2">kdeaccessibility-4.2.3</a></td><td align="right">6,4MB</td><td><tt>1f0675b666e91fe18bd6307693523f15479e3892</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdeadmin-4.2.3.tar.bz2">kdeadmin-4.2.3</a></td><td align="right">1,9MB</td><td><tt>7a344a8def92a7d2801afd78c8bff06187b1bd98</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdeartwork-4.2.3.tar.bz2">kdeartwork-4.2.3</a></td><td align="right">22MB</td><td><tt>f438060107caeb5bddd1e23a1417edf4e8476158</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdebase-4.2.3.tar.bz2">kdebase-4.2.3</a></td><td align="right">4,1MB</td><td><tt>bc05bf836ff2aea64c0806be161b8ec8b9a5a42b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdebase-runtime-4.2.3.tar.bz2">kdebase-runtime-4.2.3</a></td><td align="right">67MB</td><td><tt>f3adc26e6b313a14af1e4208bc539017c8dcccd7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdebase-workspace-4.2.3.tar.bz2">kdebase-workspace-4.2.3</a></td><td align="right">49MB</td><td><tt>0c92579c651c5a08ff6440762eb5c2ad9d5bc0ad</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdebindings-4.2.3.tar.bz2">kdebindings-4.2.3</a></td><td align="right">4,6MB</td><td><tt>d8e5ddf5e993124e0250c3e9a9de52264ca5ca7c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdeedu-4.2.3.tar.bz2">kdeedu-4.2.3</a></td><td align="right">57MB</td><td><tt>32a3ddef04f3e0d7d110f616caf98c4537ee8bb8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdegames-4.2.3.tar.bz2">kdegames-4.2.3</a></td><td align="right">37MB</td><td><tt>74e0a41cdce34bead787a7c4586d3dae7aa06cc9</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdegraphics-4.2.3.tar.bz2">kdegraphics-4.2.3</a></td><td align="right">3,5MB</td><td><tt>1304fe7562e41fad30841dfda4b42197e3d95b3d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdelibs-4.2.3.tar.bz2">kdelibs-4.2.3</a></td><td align="right">9,6MB</td><td><tt>c4cde3ea347d89d79ffdead203c85b1c2d1f8757</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdemultimedia-4.2.3.tar.bz2">kdemultimedia-4.2.3</a></td><td align="right">1,5MB</td><td><tt>ebf2d0f04dd8ab2c10a3505a2c7a3468173e369e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdenetwork-4.2.3.tar.bz2">kdenetwork-4.2.3</a></td><td align="right">6,8MB</td><td><tt>633432d049794f50143ed60197f6f0b5ac9011a7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdepim-4.2.3.tar.bz2">kdepim-4.2.3</a></td><td align="right">12MB</td><td><tt>9d46fe2ce1bf183cce82d0d60a9a29ec3c53832f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdepimlibs-4.2.3.tar.bz2">kdepimlibs-4.2.3</a></td><td align="right">1,6MB</td><td><tt>3b12d5974bfbc384e3c986328a7bb5b1b6b50361</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdeplasma-addons-4.2.3.tar.bz2">kdeplasma-addons-4.2.3</a></td><td align="right">4,1MB</td><td><tt>9c825fe7b93fcccd6de44f168438f67cf0066f22</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdesdk-4.2.3.tar.bz2">kdesdk-4.2.3</a></td><td align="right">5,3MB</td><td><tt>cf24ae63e6ee4ed875f580a7dfd8aa6d822d9b47</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdetoys-4.2.3.tar.bz2">kdetoys-4.2.3</a></td><td align="right">1,3MB</td><td><tt>cb84d7b8da85ed82972a4c99065644532cf12d6d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdeutils-4.2.3.tar.bz2">kdeutils-4.2.3</a></td><td align="right">2,3MB</td><td><tt>3c312d12b75f1064085ee4ea200a5f7278bce0e0</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.2.3/src/kdewebdev-4.2.3.tar.bz2">kdewebdev-4.2.3</a></td><td align="right">2,5MB</td><td><tt>737f6876d17da45e8dc855d47973ab8aa91827e3</tt></td></tr>
</table>
