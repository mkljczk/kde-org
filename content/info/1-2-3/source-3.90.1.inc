<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.90.1/src/kdeaccessibility-3.90.1.tar.bz2">kdeaccessibility-3.90.1</a></td><td align="right">7.5MB</td><td><tt>005c590747dd8bd3781d093b842a9269</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.90.1/src/kdeaddons-3.90.1.tar.bz2">kdeaddons-3.90.1</a></td><td align="right">769KB</td><td><tt>8e299b5e06063d85ba972f27e602e493</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.90.1/src/kdeadmin-3.90.1.tar.bz2">kdeadmin-3.90.1</a></td><td align="right">1.4MB</td><td><tt>707d04cff9d08c7cefcb6dbc8d948d4e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.90.1/src/kdeartwork-3.90.1.tar.bz2">kdeartwork-3.90.1</a></td><td align="right">44MB</td><td><tt>acdfc80caae764477f817b32bceca3db</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.90.1/src/kdebase-3.90.1.tar.bz2">kdebase-3.90.1</a></td><td align="right">19MB</td><td><tt>4fdda6481954b44608e5ce40920f61f8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.90.1/src/kdeedu-3.90.1.tar.bz2">kdeedu-3.90.1</a></td><td align="right">27MB</td><td><tt>e6fb48cd3cef0d1c6c602a2d61268559</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.90.1/src/kdegames-3.90.1.tar.bz2">kdegames-3.90.1</a></td><td align="right">21MB</td><td><tt>8dd188dd082b06562959ea286a2314af</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.90.1/src/kdegraphics-3.90.1.tar.bz2">kdegraphics-3.90.1</a></td><td align="right">5.6MB</td><td><tt>b08e25aad74e7dd55b1c22a16795991d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.90.1/src/kdelibs-3.90.1.tar.bz2">kdelibs-3.90.1</a></td><td align="right">45MB</td><td><tt>950b6f4dd6f0e17ed2551e8e81ff8bd6</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.90.1/src/kdemultimedia-3.90.1.tar.bz2">kdemultimedia-3.90.1</a></td><td align="right">4.0MB</td><td><tt>2ed05fba3577f5c8f055ca078b9cdafa</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.90.1/src/kdenetwork-3.90.1.tar.bz2">kdenetwork-3.90.1</a></td><td align="right">6.9MB</td><td><tt>1c4d1bc5aaba182b54e4ea04591298f7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.90.1/src/kdepim-3.90.1.tar.bz2">kdepim-3.90.1</a></td><td align="right">9.3MB</td><td><tt>c7ad1aa3c369351500fa92aceb94ba11</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.90.1/src/kdepimlibs-3.90.1.tar.bz2">kdepimlibs-3.90.1</a></td><td align="right">1.5MB</td><td><tt>cfa36650c09598ea72e236713700839c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.90.1/src/kdesdk-3.90.1.tar.bz2">kdesdk-3.90.1</a></td><td align="right">5.1MB</td><td><tt>2d6bbd7fad57c6040b98703bb104c076</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.90.1/src/kdetoys-3.90.1.tar.bz2">kdetoys-3.90.1</a></td><td align="right">2.2MB</td><td><tt>b73493bb651236b43beac78a43d54ad3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.90.1/src/kdeutils-3.90.1.tar.bz2">kdeutils-3.90.1</a></td><td align="right">2.1MB</td><td><tt>410925929e9442a46b83ab9d714c730c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.90.1/src/kdevelop-3.90.1.tar.bz2">kdevelop-3.90.1</a></td><td align="right">2.5MB</td><td><tt>c46e7e22dfe57f651832490ec22b1847</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.90.1/src/kdewebdev-3.90.1.tar.bz2">kdewebdev-3.90.1</a></td><td align="right">4.4MB</td><td><tt>6465e65542968c587cb7c35d6cca0a60</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.90.1/src/qt-copy-20070423.tar.bz2">qt-copy-20070423</a></td><td align="right">36MB</td><td><tt>3131de308d293d5cb7c19c7bb2b1d3ab</tt></td></tr>
</table>
